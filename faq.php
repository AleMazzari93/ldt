<!DOCTYPE html>
<?php include "sections/header.php";?>

<!-- PAGE TITLE SMALL -->
<div class="page-title-cont page-title-small grey-light-bg">
  <div class="relative container align-left">
    <div class="row">

      <div class="col-md-8">
        <h1 class="page-title">FAQ</h1>
      </div>

      <div class="col-md-4">
        <div class="breadcrumbs">
          <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current">FAQ</span>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="p-140-cont">

  <!-- CONTAINER -->
  <div class="container">
<!-- MELTING -->
    <div class="col-md-12 faq-div">
      <h3 class="lang" key="casting"></h3>
      <!-- Toggle with BG-->
      <ul class="toggle-view-custom faq-ul" style="list-style-type: none;">
        <li>
          <h3 class="ui-accordion-header lang" key="first-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="first-sub-casting"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="second-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="second-sub-casting"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="third-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="third-sub-casting"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="fourth-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="fourth-sub-casting"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="fifth-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="fifth-sub-casting"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="sixth-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="sixth-sub-casting"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="seventh-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="seventh-sub-casting"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="eighth-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="eighth-sub-casting"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="ninth-casting"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="ninth-sub-casting"></p>
          </div>
        </li>
      </ul><!-- End Toggles -->
    </div>
<!-- WELDING -->
    <div class="col-md-12 faq-div">
      <h3 class="lang" key="welding"></h3>
      <!-- Toggle with BG-->
      <ul class="toggle-view-custom faq-ul" style="list-style-type: none;">
        <li>
          <h3 class="ui-accordion-header lang" key="first-welding"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="first-sub-welding"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="second-welding"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="second-sub-welding"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="third-welding"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="third-sub-welding"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="fourth-welding"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="fourth-sub-welding"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="fifth-welding"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="fifth-sub-welding"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="sixth-welding"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="sixth-sub-welding"></p>
          </div>
        </li>
        <li>
          <h3 class="ui-accordion-header lang" key="seventh-welding"><span class="link"></span></h3>
          <div class="panel">
            <p class="lang" key="seventh-sub-welding"></p>
          </div>
        </li>
      </ul><!-- End Toggles -->
    </div>


  </div>
</div>

<?php include "sections/footer.php";?>
