<!DOCTYPE html>
        <?php include "sections/header.php";?>

          <!-- PAGE TITLE -->
          <div class="page-title-cont page-title-small grey-light-bg">
            <div class="relative container align-left">
              <div class="row">

                <div class="col-md-8">
                  <h1 class="page-title">TABELLA SPESE SPEDIZIONE</h1>
                </div>

                <div class="col-md-4">
                  <div class="breadcrumbs">
                    <a href="reservedarea-homepage.php">Home</a><span class="slash-divider">/</span><span class="bread-current">TABELLA COSTO SPEDIZIONE</span>
                  </div>
                </div>

              </div>
            </div>
          </div>


          <div class="page-section">
            <div class="container fes4-cont">
              <div class="col-md-12 main-div-sc">
                <h3>CORRIERE E TIPO DI SPEDIZIONE</h3>
                <div class="col-md-12">
                  <select class="form-control fercam-option" id="shipping-type" onchange="changeCourrier.call(this, event)">
                    <option>Seleziona un opzione</option>
                    <option>SDA NAZIONALE</option>
                    <option>SDA - EUROPA</option>
                    <option>FERCAM INTERNAZIONALE</option>
                  </select>
                  <br>
                </div>
              </div>


              <!-- SDA NAZIONALE -->
              <div class="row sda-national no-display">
                <div class="col-md-12 main-div-sc">
                  <h3>DIMENSIONI PACCO</h3>
                  <div class="col-md-6 input-package">
                    <input id="packWidth" type="number" name="" value="" placeholder="Larghezza pacco (cm)"></input>
                  </div>
                  <div class="col-md-6 input-package">
                    <input id="packDeep" type="number" name="" value="" placeholder="Profondità pacco (cm)"></input>
                  </div>
                  <div class="col-md-6 input-package">
                    <input id="packHeight" type="number" name="" value="" placeholder="Altezza pacco (cm)"></input>
                  </div>
                </div>
                <div class="col-md-12 main-div-sc">
                  <h3>PESO PACCO</h3>
                  <div class="col-md-6 input-package">
                    <input id="weight" type="number" name="" value="" placeholder="Peso pacco (KG)"></input>
                  </div>
                </div>
                <div class="col-md-12 main-div-sc">
                  <h3>OPZIONI AGGIUNTIVE</h3>
                  <div class="col-md-6 input-package">
                    <input type="checkbox" value="false" id="isInItaly">Destinazione: Sicilia, Calabria, Sardegna</input>
                  </div>
                  <div class="col-md-6 input-package">
                    <input id="countersign" type="checkbox" value="">Contrassegno</input>
                  </div>
                  <div class="col-md-6 input-package">
                    <input id="goodsValue" type="number" name="" value="" placeholder="Valore merce €">*Se contrassegno, immettere il valore totale della merce</input>
                  </div>
                </div>
                <div class="col-md-12 main-div-sc">
                  <h3>PREZZO IMBALLO DA ADDEBITARE AI CLIENTI</h3>
                  <div class="col-md-6 input-package">
                    <select class="form-control" id="sel1">
                      <option>Seleziona un opzione</option>
                      <option>SCATOLA CARTONE DA 20X20X15 A 60X40X40</option>
                      <option>SCATOLA CARTONE DA 60X50X40 A 80X80X60</option>
                      <option>SCATOLA CARTONE PESANTE DA 80X60X60</option>
                      <option>SCATOLA CARTONE PESANTE DA 120X80X77</option>
                      <option>BANCALE 120X80</option>
                      <option>BANCALE 80X60</option>
                    </select>
                    <br>
                    <input id="isUsed" type="checkbox" value="">L'imballo è usato?</input>
                  </div>
                </div>
                <div class="col-md-12 main-div-sc">
                  <button type="button" name="button" data-toggle="modal" data-target="#exampleModalCenter" onclick="formShippingSdaNational()" class="button medium gray mt-40">CALCOLA</button>
                </div>
              </div>

              <!-- SDA EUROPA -->
              <div class="row sda-europe no-display">
                <div class="col-md-12 main-div-sc">
                  <h3>DIMENSIONI PACCO</h3>
                  <div class="col-md-6 input-package">
                    <input id="packWidthEu" type="number" name="" value="" placeholder="Larghezza pacco (cm)"></input>
                  </div>
                  <div class="col-md-6 input-package">
                    <input id="packDeepEu" type="number" name="" value="" placeholder="Profondità pacco (cm)"></input>
                  </div>
                  <div class="col-md-6 input-package">
                    <input id="packHeightEu" type="number" name="" value="" placeholder="Altezza pacco (cm)"></input>
                  </div>
                </div>
                <div class="col-md-12 main-div-sc">
                  <h3>PESO PACCO</h3>
                  <div class="col-md-6 input-package">
                    <input id="weightEu" type="number" name="" value="" placeholder="Peso pacco (KG)"></input>
                  </div>
                </div>
                <div class="col-md-12 main-div-sc">
                  <h3>PREZZO IMBALLO DA ADDEBITARE AI CLIENTI</h3>
                  <div class="col-md-6 input-package">
                    <select class="form-control" id="sel1Eu">
                      <option>Seleziona un opzione</option>
                      <option>SCATOLA CARTONE DA 20X20X15 A 60X40X40</option>
                      <option>SCATOLA CARTONE DA 60X50X40 A 80X80X60</option>
                      <option>SCATOLA CARTONE PESANTE DA 80X60X60</option>
                      <option>SCATOLA CARTONE PESANTE DA 120X80X77</option>
                      <option>BANCALE 120X80</option>
                      <option>BANCALE 80X60</option>
                    </select>
                    <br>
                    <input id="isUsedEu" type="checkbox" value="">L'imballo è usato?</input>
                  </div>
                </div>
                <div class="col-md-12 main-div-sc">
                  <h3>DESTINAZIONE</h3>
                  <div class="col-md-6 input-package">
                    <select class="form-control" id="destination">
                      <option>Seleziona una destinazione</option>
                      <option>GERMANIA</option>
                      <option>AUSTRIA</option>
                      <option>OLANDA</option>
                      <option>BELGIO</option>
                      <option>LUSSEMBURGO</option>
                      <option>BOSNIA AND HERZ.</option>
                      <option>UNGHERIA</option>
                      <option>MONTENEGRO</option>
                      <option>SERBIA</option>
                      <option>SLOVACCHIA</option>
                      <option>SLOVENIA</option>
                      <option>GRAN BRETAGNA - ISOLE CHANNEL</option>
                      <option>SPAGNA</option>
                      <option>FRANCIA (CON P. DI MONACO)</option>
                      <option>PORTOGALLO - ISOLE BALEARI</option>
                      <option>IRLANDA. REP.</option>
                      <option>IRLANDA DEL NORD</option>
                      <option>SVIZZERA - LIECHTENSTEIN</option>
                      <option>POLONIA</option>
                      <option>ESTONIA</option>
                      <option>DANIMARCA</option>
                      <option>LETTONIA - LITUANIA</option>
                      <option>SVEZIA</option>
                      <option>FINLANDIA</option>
                      <option>NORVEGIA</option>
                      <option>REP. CECA</option>
                      <option>CORSICA</option>
                      <option>AZZORRE - MADEIRA</option>
                      <option>ANDORRA - ISOLE CANARIE - CEUTA - MELILLA</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-12 main-div-sc">
                  <h3>NOTE</h3>
                  <div class="col-md-6 input-package">
                    <textarea id="notes"></textarea>
                  </div>
                </div>
                <div class="col-md-12 main-div-sc">
                  <button type="button" name="button" data-toggle="modal" data-target="#exampleModalCenter" onclick="formShippingSdaEurope()" class="button medium gray mt-40">CALCOLA</button>
                </div>
              </div>



        <!-- FERCAM INTERNAZIONALE -->

        <div class="row fercam-international no-display">
          <div class="col-md-12 main-div-sc">
            <h3>DIMENSIONI PACCO</h3>
            <div class="col-md-6 input-package">
              <input id="packWidthFe" type="number" name="" value="" placeholder="Larghezza pacco (cm)"></input>
            </div>
            <div class="col-md-6 input-package">
              <input id="packDeepFe" type="number" name="" value="" placeholder="Profondità pacco (cm)"></input>
            </div>
            <div class="col-md-6 input-package">
              <input id="packHeightFe" type="number" name="" value="" placeholder="Altezza pacco (cm)"></input>
            </div>
          </div>
          <div class="col-md-12 main-div-sc">
            <h3>PESO COLLO</h3>
            <div class="col-md-6 input-package">
              <input id="weightFe" type="number" name="" value="" placeholder="Peso pacco (KG)"></input>
            </div>
          </div>

          <div class="col-md-12 main-div-sc">
            <h3>DESTINAZIONE</h3>
            <div class="col-md-6 input-package">
              <select onchange="removeCapDiv()" class="form-control" id="fercam-destination">
                <option disabled selected>Selezionare una destinazione</option>
              </select>
            </div>
          </div>

          <div class="col-md-12 main-div-sc cap-div">
            <h3>CAP DELLA DESTINAZIONE</h3>
            <div class="col-md-6 input-package">
              <input type="text" id="zipCode" placeholder="Inserire il CAP" required></input>
            </div>
          </div>

          <div class="col-md-12 main-div-sc">
            <h3>OPZIONI AGGIUNTIVE</h3>
            <div class="col-md-6 input-package">
              <input type="checkbox" id="dem">DEM</input>
            </div>
            <div class="col-md-6 input-package">
              <input type="checkbox" id="insurance">Assicurazione "All Risks"</input>
            </div>
            <div class="col-md-6 input-package">
              <input type="checkbox" id="border-operation">Operazioni doganali</input>
            </div>
            <div class="col-md-6 input-package">
              <input type="checkbox" id="ex1">EX1/ATR/EUR1</input>
            </div>
            <div class="col-md-6 input-package">
              <input type="checkbox" id="hydraulic">Utilizzo sponda idraulica</input>
            </div>
          </div>
          <div class="col-md-12 main-div-sc">
            <h3>VALORE DELLA MERCE</h3>
            <div class="col-md-6 goods-value-div">
              <input id="goods-value" type="number" placeholder="Inserire il valore della merce" style="width : 250px;"></input>
            </div>
          </div>

          <div class="col-md-12 main-div-sc">
            <h3>NOTE</h3>
            <div class="col-md-6 input-package">
              <textarea id="notesFe"></textarea>
            </div>
          </div>
          <div class="col-md-12 main-div-sc">
            <button type="button" name="button" data-toggle="modal" data-target="#exampleModalCenter" onclick="formShippingFercamInternational()" class="button medium gray mt-40">CALCOLA</button>
          </div>
        </div>

      </div>
    </div>

    <?php include "sections/modal-weight.php";?>
    <?php include "sections/footer.php";?>
