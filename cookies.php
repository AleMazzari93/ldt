<!DOCTYPE html>
        <?php include "sections/header.php";?>

        <!-- PAGE TITLE -->
        <div class="page-title-cont page-title-small grey-light-bg">
          <div class="relative container align-left">
            <div class="row">

              <div class="col-md-8">
                <h1 class="page-title">COOKIES</h1>
              </div>

              <div class="col-md-4">
                <div class="breadcrumbs">
                  <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current">COOKIES</span>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div style="text-align: left; margin: 80px 40px 40px 40px">
        <p><strong>Che cosa sono i Cookies</strong></p>
        <p>Come è prassi comune di quasi tutti i siti web anche LuigiDalTrozzo.it utilizza i cookie. I cookie sono piccoli file che vengono scaricati sul computer e utilizzati per migliorare l&#8217;esperienza di navigazione su i siti web.<br />
          Questa pagina descrive quali informazioni vengono raccolte, la modalità di utilizzo delle informazioni e il motivo per il quale a volte è necessario utilizzare i cookie.<br />
          In seguito, verrà descritto anche come è possibile impedire la memorizzazione dei cookie e, tuttavia, questo potrebbe impedire il corretto funzionamento del sito web.</p>
          <p><strong>Come utilizziamo i Cookies</strong></p>
          <p>I cookie vengono utilizzati per una serie di funzionalità illustrate qui di seguito.<br />
            Purtroppo, nella maggior parte dei casi, non ci sono opzioni standard per la disattivazione di una parte di cookie senza disattivare completamente la funzionalità e le caratteristiche del sito web. Si consiglia, pertanto, di lasciare attivi i cookie relativi ai servizi forniti da LuigiDalTrozzo.it e disabilitare solo quelli per cui si ha una sicura certezza che non compromettano il regolare funzionamento di LuigiDalTrozzo.it.</p>
            <p><strong>Disabilitare i Cookies</strong></p>
            <p>È possibile impedire la memorizzazione dei cookie modificando le impostazioni del browser web (vedere la Guida del proprio browser web su come procedere). Bisogna tenere bene in mente che la disattivazione dei cookie influenzerà sia il corretto funzionamento di LuigiDalTrozzo.it che quello di molti altri siti web.<br />
              La disattivazione dei cookie limiterà le funzionalità e le caratteristiche del sito web.</p>
              <p>Di seguito riportiamo i link alle pagine che descrivono la disattivazione dei cookie per i principali browser web:<br />
                Google Chrome <a href="https://support.google.com/accounts/answer/61416?hl=it" target="_blank">https://support.google.com/accounts/answer/61416?hl=it</a><br />
                Mozilla Firefox <a href="https://support.mozilla.org/it/kb/Disattivare%20i%20cookie%20di%20terze%20parti" target="_blank">https://support.mozilla.org/it/kb/Disattivare%20i%20cookie%20di%20terze%20parti</a><br />
                Internet Explorer <a href="http://windows.microsoft.com/it-it/windows7/block-enable-or-allow-cookies" target="_blank">http://windows.microsoft.com/it-it/windows7/block-enable-or-allow-cookies</a><br />
                Apple Safari <a href="https://support.apple.com/it-it/HT201265" target="_blank">https://support.apple.com/it-it/HT201265</a><br />
                Opera <a href="http://help.opera.com/Windows/10.00/it/cookies.html" target="_blank">http://help.opera.com/Windows/10.00/it/cookies.html</a></p>
                <p><strong>I Cookies che abilitiamo</strong></p>
                <p>Quando si crea un account con LuigiDalTrozzo.it, contestualmente vengono creati dei cookie tecnici che verranno utilizzati per il processo di registrazione e di amministrazione. Questi cookie, in genere, vengono eliminati automaticamente dopo un breve periodo di tempo all&#8217;uscita del sito web e alla chiusura del browser web.</p>
                <p>LuigiDalTrozzo.it utilizza i cookie tecnici per gestire le sessioni utente, al fine di permettere l&#8217;accesso alle funzioni riservate di account. Questi cookie evitano di dover inserire il proprio utente e la propria password ogni volta che si visita una pagina della propria area riservata. Questi cookie sono tipicamente rimossi all&#8217;uscita del sito web e alla chiusura del browser web.<br />
                  Dal momento che LuigiDalTrozzo.it è un sito web per il commercio elettronico i cookie tecnici sono utilizzati anche per la gestione dell&#8217;ordine e il perfezionamento dell&#8217;acquisto dei prodotti. La disattivazione dei cookie tecnici compromette il regolare acquisto dei prodotti desiderati!</p>
                  <p><strong>Cookies di terze parti</strong></p>
                  <p>LuigiDalTrozzo.it può utilizzare cookie forniti da terzi parti. Di seguiti elenchiamo i cookie di terze parti che LuigiDalTrozzo.it utilizza.</p>
                  <p>LuigiDalTrozzo.it utilizza le mappe di Google per visualizzare la propria posizione. Per maggiore informazioni vi invitiamo a visitare la seguente pagina web: <a href="http://www.google.com/policies/technologies/cookies/" target="_blank">http://www.google.com/policies/technologies/cookies/</a></p>
                  <p>La tipologia di pagamento a mezzo carta di credito sul sito LuigiDalTrozzo.it è gestita dal sistema interbancario di PayPal. PayPal è un sito web esterno a LuigiDalTrozzo.it e anch&#8217;esso potrebbe utilizzare i cookie. I dati forniti da PayPal a LuigiDalTrozzo.it sono utilizzati esclusivamente per la gestione e l&#8217;evasione degli ordini di acquisto.<br />
                    Per ulteriori informazioni su PayPal vi invitiamo a visitare la pagina ufficiale:<br />
                    <a href="https://www.paypal.com/it/webapps/mpp/ua/cookie-full" target="_blank">https://www.paypal.com/it/webapps/mpp/ua/cookie-full</a></p>
                    <p><strong>Cookies di profilazione</strong></p>
                    <p>Sono quei cookie necessari a creare profili utenti al fine di inviare messaggi pubblicitari in linea con le preferenze manifestate dall&#8217;utente all&#8217;interno delle pagine del sito web.<br />
                      <em><span style="text-decoration: underline;">LuigiDalTrozzo.it <strong>NON</strong> utilizza cookie di profilazione.</span></em></p>
                      <p><strong>Ulteriori informazioni</strong></p>
                      <p>Vi invitiamo a lasciare abilitati i cookie in modo da poter usufruire di tutte le funzionalità offerte dal LuigiDalTrozzo.it. Se desiderate ulteriori informazioni potete inviarci la vostra richiesta tramite la nostra <a href="http://www.luigidaltrozzo.it/contatti/">pagina di contatto</a>.</p>
                      <div class="clearfix"></div>
                    </div>

        <?php include "sections/footer.php";?>
