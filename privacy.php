<!DOCTYPE html>
<?php include "sections/header.php";?>

<!-- PAGE TITLE -->
<div class="page-title-cont page-title-small grey-light-bg">
  <div class="relative container align-left">
    <div class="row">

      <div class="col-md-8">
        <h1 class="page-title">PRIVACY</h1>
      </div>

      <div class="col-md-4">
        <div class="breadcrumbs">
          <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current">Privacy</span>
        </div>
      </div>

    </div>
  </div>
</div>

<div style="text-align: left; margin: 80px 40px 40px 40px">
  <h2>Privacy</h2>
  <p><strong>Informativa all’Interessato ai sensi dell’art. 13 del D. Lgs. 196/2003 e dell’art. 13 del Regolamento (UE) 2016/679 </strong></p>
  <p>LUIGI DAL TROZZODI C.A. MAZZARI & C. S.A.S. nella qualità di Titolare del trattamento dei Suoi dati personali, aisensi e per gli effetti del Reg.to UE
    2016/679 diseguito 'GDPR', con la presente La informa che la citata normativa prevede la tutela degli interessatirispetto al trattamento dei dati
    personali e che tale trattamento sarà improntato ai principi dicorrettezza, liceità, trasparenza e di tutela della Sua riservatezza e dei Suoi diritti.
    I Suoi dati personali verranno trattati in accordo alle disposizioni legislative della normativa sopra richiamata e degli obblighi diriservatezza ivi previsti.
    Finalità e base giuridica del trattamento: in particolare i Suoi datisaranno utilizzati per le seguenti finalità relative all’esecuzione dimisure connesse ad
    obblighicontrattuali o pre-contrattuali:
    <ul>
      <li>finalità Statistica e di Analisi della navigazione e degli utenti.</li>
    </ul>
  </p>
  <p>Modalità del trattamento. I suoi dati personali potranno essere trattati nei seguenti modi<br>
    <ul>
      <li>a mezzo calcolatori elettronicicon utilizzo disistemisoftware gestiti o programmati direttamente.</li>
    </ul>
    Ogni trattamento avviene nelrispetto delle modalità dicui agli artt. 6, 32 del GDPR e mediante l'adozione delle adeguate misure disicurezza previste.
    Comunicazione : isuoi datisaranno comunicati esclusivamente a soggetticompetenti e debitamente nominati per l'espletamento deiservizi necessari ad
    una corretta gestione delrapporto, con garanzia di tutela dei diritti dell'interessato.
    I suoi datisaranno trattati unicamente da personale espressamente autorizzato dal Titolare ed, in particolare, dalle seguenticategorie di addetti:
    <ul>
      <li>programmatori e Analisti;</li>
      <li>ufficio Marketing.</li>
    </ul>
  </p>
  <p>Diffusione: I suoi dati personali non verranno diffusi in alcunmodo.
    I suoi dati personali potranno inoltre essere trasferiti, limitatamente alle finalità sopra riportate, neiseguentistati:
    <br />
    <ul>
      <li>paesi UE.</li>
    </ul>
    Periodo di Conservazione. Le segnaliamo che, nelrispetto dei principi di liceità, limitazione delle finalità e minimizzazione dei dati, aisensi dell’art. 5 del
    GDPR, il periodo diconservazione dei Suoi dati personali è:</p>
    <ul>
      <li>stabilito per un arco di tempo non superiore alconseguimento delle finalità per le qualisono raccolti e trattati per l'esecuzione e l'espletamento
        delle finalità contrattuali;</li>
        <li>stabilito per un arco di tempo non superiore all'espletamento deiservizi erogati;</li>
        <li>stabilito per un arco di tempo non superiore alconseguimento delle finalità per le qualisono raccolti e trattati e nelrispetto dei tempi obbligatori
          prescritti dalla legge.</li>
        </ul>
        <p>Gestione deicookie: nelcaso in cui Lei abbia dubbi o preoccupazioni inmerito all'utilizzo deicookie Le è sempre possibile intervenire per impedirne l'impostazione e la lettura, ad esempiomodificando le impostazionisulla privacy all'interno del Suo browser al fine di bloccarne determinati tipi.
          Poiché ciascun browser - e spesso diverse versioni dello stesso browser - differiscono anche sensibilmente le une dalle altre se preferisce agire
          autonomamente mediante le preferenze del Suo browser può trovare informazioni dettagliate sulla procedura necessaria nella guida del Suo browser. Per
          una panoramica delle modalità di azione per i browser più comuni, può visitare l'indirizzo www.cookiepedia.co.uk.
          Le società pubblicitarie consentono inoltre dirinunciare alla ricezione di annuncimirati, se lo si desidera. Ciò non impedisce l'impostazione deicookie, ma
          interrompe l'utilizzo e la raccolta di alcuni dati da parte di talisocietà.
          Permaggiori informazioni e possibilità dirinuncia, visitare l'indirizzo www.youronlinechoices.eu/.
          Titolare: il Titolare del trattamento dei dati, aisensi della Legge, è LUIGI DAL TROZZODI C.A. MAZZARI & C. S.A.S. (Via Claudio Treves 26 , 20090
          Vimodrone (MI); P.Iva: 08015150157) nella persona delsuo legale rappresentante pro tempore.
          Lei ha diritto di ottenere dal titolare la cancellazione (diritto all'oblio), la limitazione, l'aggiornamento, la rettificazione, la portabilità, l'opposizione al
          trattamento dei dati personaliche La riguardano, nonché in generale può esercitare tutti i diritti previsti dagli artt. 15, 16, 17, 18, 19, 20, 21, 22 del
          GDPR.
          Potrà inoltre visionare in ognimomento la versione aggiornata della presente informativa collegandosi all'indirizzo internet
          <a href="https://www.privacylab.it/informativa.php?11611355435">qui</a>
          <br />
          <p><strong>1. L'interessato ha diritto di ottenere la conferma dell'esistenza omeno di dati personaliche lo riguardano, anche se non ancora registrati, e la loro
            comunicazione in forma intelligibile.</strong>
            <br /></p>
            <p><strong>2. L'interessato ha diritto di ottenere l'indicazione:</strong></p>
            <ul>
              <li>a. dell'origine dei dati personali;</li>
              <li>b. delle finalità e modalità del trattamento;</li>
              <li>c. della logica applicata in caso di trattamento effettuato con l'ausilio distrumenti elettronici;</li>
              <li>d. degli estremi identificativi del titolare, deiresponsabili e delrappresentante designato aisensi dell'articolo 5, comma 2;</li>
              <li>e. deisoggetti o delle categorie disoggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di
                rappresentante designato nel territorio dello Stato, diresponsabili o incaricati.</li>
              </ul>
              <p><strong>3. L'interessato ha diritto di ottenere:</strong></p>
              <ul>
                <li>a. l'aggiornamento, la rettificazione ovvero, quando vi ha interesse, l'integrazione dei dati;</li>
                <li>b. la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli dicui non è necessaria la
                  conservazione in relazione agliscopi per i quali i datisono statiraccolti o successivamente trattati;</li>
                  <li>c. l'attestazione che le operazioni dicui alle lettere a) e b) sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, dicoloro ai
                    quali i datisono staticomunicati o diffusi, eccettuato ilcaso in cui tale adempimento sirivela impossibile o comporta un impiego dimezzi
                    manifestamente sproporzionato rispetto al diritto tutelato;</li>
                    <li>d. la portabilità dei dati.</li>
                  </ul>
                  <p><strong>4. L'interessato ha diritto di opporsi, in tutto o in parte:</strong></p>
                  <ul>
                    <li>a. permotivi legittimi al trattamento dei dati personaliche lo riguardano, ancorché pertinenti allo scopo della raccolta;</li>
                    <li>b. al trattamento di dati personaliche lo riguardano a fini di invio dimateriale pubblicitario o di vendita diretta o per ilcompimento diricerche di
                      mercato o dicomunicazione commerciale.</li>
                    </ul>
                    <br>
                  <p>In fede<br>Luigi Dal Trozzo & C. s.a.s</p>
                  </div>
                  <div class="clearfix"></div>
                </div>


                <?php include "sections/footer.php";?>
