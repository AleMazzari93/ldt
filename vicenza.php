<!DOCTYPE html>
        <?php include "sections/header.php";?>

        <!-- PAGE TITLE -->
        <div class="page-title-cont page-title-small grey-light-bg">
          <div class="relative container align-left">
            <div class="row">

              <div class="col-md-8">
                <h1 class="page-title">VICENZA</h1>
              </div>

              <div class="col-md-4">
                <div class="breadcrumbs">
                  <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current">VICENZA</span>
                </div>
              </div>

            </div>
          </div>
        </div>

          <!-- GOOGLE MAP -->
          <div class="page-section">
            <div class="container-fluid">
              <div class="row">
                <div data-address="Via Zamenhof 719, Vicenza 36100" id="google-map"></div>
              </div>
            </div>
          </div>

          <!-- CONTACT INFO SECTION 1 -->
          <div id="contact-link" class="page-section p-80-cont grey-light-bg">
            <div class="container">
              <div class="row">

                <div class="col-md-3 col-sm-6">
                  <div class="cis-cont">
                    <div class="cis-icon">
                      <div class="icon icon-basic-clock"></div>
                    </div>
                    <div class="cis-text">
                      <h3><span class="bold lang" key="openingHours"></span></h3>
                      <p class="lang" key="hoursVicenza"></p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="cis-cont">
                    <div class="cis-icon">
                      <div class="icon icon-basic-map"></div>
                    </div>
                    <div class="cis-text">
                      <h3><span class="bold lang" key="address"></span></h3>
                      <p>VIA LUDOVICO LAZZARO ZAMENHOF 719, VICENZA 36100</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="cis-cont">
                    <div class="cis-icon">
                      <div class="icon icon-basic-mail"></div>
                    </div>
                    <div class="cis-text">
                      <h3><span class="bold">EMAIL</span></h3>
                      <p><a href="mailto:vicenza@luigidaltrozzo.it">VICENZA@LUIGIDALTROZZO.IT</a></p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="cis-cont">
                    <div class="cis-icon">
                      <div class="icon icon-basic-smartphone"></div>
                    </div>
                    <div class="cis-text">
                      <h3><span class="bold lang" key="callUs"></span></h3>
                      <p>+39 0444 91 40 46</p>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <?php include "sections/contact-form.php";?>
          <?php include "sections/footer.php";?>
