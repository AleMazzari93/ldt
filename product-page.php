<!DOCTYPE html>
<?php include "sections/header.php";?>
<div class="page-title-cont page-title-small grey-light-bg">
  <div class="relative container align-left">
    <div class="row">
      <div class="col-md-8 title title-name">
        </h1>
      </div>
      <div class="col-md-4">
        <div class="breadcrumbs">
          <a href="index.php">Home</a><span class="slash-divider">/</span><a href="second-hand.php" class="lang" key="sh"></a><span class="slash-divider">/</span><span class="bread-current articleName"></span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-section p-140-cont">
  <div class="container">
    <a href="/second-hand.php">
      <div class="col icon-back icon icon-arrows-left-double-32"></div>
    </a>
    <div class="row">
      <div class="col-md-4 col-sm-12 mb-50" >
        <div class="post-prev-img popup-gallery">
        </div>
        <div class="sale-label-cont sale-dot">
        </div>
        <div class="row">
        </div>
      </div>
      <div class="col-md-7 col-sm-12 col-md-offset-1 mb-50">
        <div class="articleNameBanner"></div>
        <hr class="mt-0 mb-30">
        <div class="row">
          <div class="col-xs-6  mt-0 mb-30 priceRow">
            <strong class="item-price">€ </strong>
            <h4 class="lang" key="vat"></h4>
          </div>
        </div>
        <hr class="mt-0 mb-30">
        <div class="row mb-30">
          <form method="post" action="#" class="form">
            <div class="col-xs-8 col-sm-10 col-md-12">
              <div class="post-prev-more-cont clearfix">
                <div class="shop-add-btn-cont">
                </div>
              </div>
            </div>
          </form>
        </div>
        <hr class="mt-0 mb-30">
        <div class="lang ourCode" key="ourCode"></div>
        <div class="lang storedIn" key="storedIn"></div>
      </div>
    </div>
  </div>
  <div class="container mb-100">
    <div>
      <div class="tabs-3">
        <ul class="nav nav-tabs xs-tabs-transform bootstrap-tabs">
          <li class="active">
            <a class="lang" href="#one" data-toggle="tab" key="description"></a>
          </li>
          <li id="spec-div">
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade" id="two">
            <table class="table table-striped table-bordered mt-30">
              <tbody class="productSpec">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <hr class="mt-0 mb-80">
    <div class="container relatedContainer">
      <div class="row">
        <div class="col-md-12">
          <h4 class="blog-page-title mt-0 mb-40 lang" key="related-product"></h4>
        </div>
        <?php include "sections/related.php";?>
        <?php include "sections/modal.php";?>
      </div>
    </div>
  </div>
<?php include "sections/footer.php";?>


<!-- <div class="port-btn-cont">
  <a href="${product.imageBig}" class="lightbox mr-20"><div aria-hidden="true" class="icon_search"></div></a>
  <a href="product-page.php" class="mr-20"><div aria-hidden="true" class="shopCart icon_cart"></div></a>
</div> -->
