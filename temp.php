<!-- SLIDE 3 -->
<li data-transition="zoomout" data-slotamount="1" data-masterspeed="1500" data-thumb="#"  data-saveperformance="on"  data-title="WELCOM">

  <!-- MAIN IMAGE -->
  <img src="images/revo-slider/dummy.png"  alt="citybg" data-lazyload="images/revo-slider/slide_1_3.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

  <!-- LAYERS -->

  <!-- LAYER NR. 0 BG CAPTIONS -->
  <div class="tp-caption rs-parallaxlevel-4 zoomout"
  data-x="0"
  data-y="center"
  data-speed="1300"
  data-start="200"
  data-easing="Power3.easeInOut"
  style="z-index: 0;">
  <div class=""></div>
</div>

<!--PARALLAX & OPACITY container -->
<div class="rs-parallaxlevel-4  opacity-scroll2">
  <!-- LAYER NR. 1 -->
  <div class="tp-caption dark-light-60 center-0-478 sfb tp-resizeme lang"
  data-x="0"
  data-y="218"
  data-speed="500"
  data-start="850"
  data-easing="Power1.easeInOut"
  data-splitin="none"
  data-splitout="none"
  data-elementdelay="0.1"
  data-endelementdelay="0.1"
  key="thirdSlide1"
  style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap; color: white;">
</div>

<!-- LAYER NR. 2 -->
<div class="tp-caption dark-black-60 center-0-478 sfb tp-resizeme lang"
data-x=""
data-y="320"
data-speed="500"
data-start="1050"
data-easing="Power1.easeInOut"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.1"
data-endelementdelay="0.1"
key="thirdSlide2"
style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap; color: white;">
</div>

<!-- LAYER NR. 3 -->
<div class="tp-caption center-0-478 sfb"
data-x="0"
data-y="465"
data-speed="900"
data-start="1350"
data-easing="Power3.easeInOut"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.1"
data-endelementdelay="0.1"
style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><a class="button medium gray btn-5 btn-5aa" href="https://infinity.luigidaltrozzo.it"><span aria-hidden="true" class="button-icon-anim icon-basic-info"></span><span class="button-text-anim lang" key="infoButton"></span></a>
</div>

</li>
