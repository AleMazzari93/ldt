<!DOCTYPE html>
        <?php include "sections/header.php";?>

        <!-- PAGE TITLE -->
        <div class="page-title-cont page-title-small grey-light-bg">
          <div class="relative container align-left">
            <div class="row">

              <div class="col-md-8">
                <h1 class="page-title lang" key="contact"></h1>
              </div>

              <div class="col-md-4">
                <div class="breadcrumbs">
                  <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current lang" key="contact"></span>
                </div>
              </div>

            </div>
          </div>
        </div>

          <!-- CONTACT INFO SECTION 1 -->
          <div id="contact-link" class="page-section p-80-cont">
            <div class="container">
              <div class="row">

                <div class="col-md-12 branchesDiv">
                      <h3><a href="hq.php" class="bold lang" key="hq"></span></a></h3>
                  </div>

                <div class="col-md-12 branchesDiv">
                      <h3><a href="arezzo.php" class="bold"></span>AREZZO</a></h3>
                  </div>

                <div class="col-md-12 branchesDiv">
                      <h3><a href="valenza.php" class="bold"></span>VALENZA</a></h3>
                  </div>

                <div class="col-md-12 branchesDiv">
                      <h3><a href="vicenza.php" class="bold"></span>VICENZA</a></h3>
                  </div>

                <div class="col-md-12 branchesDiv">
                      <h3><a href="milano.php" class="bold"></span>MILANO</a></h3>
                  </div>

                <div class="col-md-12 branchesDiv">
                      <h3><a href="roma.php" class="bold"></span>ROMA</a></h3>
                  </div>

              </div>
            </div>
          </div>

          <hr>

        <?php include "sections/contact-form.php";?>
        <?php include "sections/footer.php";?>
