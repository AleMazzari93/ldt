<?php

if ( $_POST["enter"] ) {

 if ( !$_POST["g-recaptcha-response"] ) {
		exit("Captcha g-recaptcha-response doesn't exist");
 };
 //var_dump($_POST);

  $url = "https://www.google.com/recaptcha/api/siteverify";
	$key = "6Lev4U4UAAAAAC4ormq8u1oBBCkza2zV3uiqpAY_";
	$query = $url."?secret=".$key."&response=".$_POST["g-recaptcha-response"]."&remoteip=".$_SERVER["REMOTE_ADDR"];

  $data = json_decode(file_get_contents($query));

  if ( $data->success == false) {
		$arrResult = array ('response'=>'error');
    echo json_encode($arrResult);
  } else {
    if (array_key_exists('email', $_POST)) {
    date_default_timezone_set('Etc/UTC');

    require 'php-mailer/PHPMailerAutoload.php';

    //Create a new PHPMailer instance
    $mail = new PHPMailer;

    $mail->setFrom('alessandro.mazzari@luigidaltrozzo.it', 'www.luigidaltrozzo.it'); //**Write here sender email. For example, emails will be sent to you from your website, so write email of your website (if you don't have it, write any email, which you want) and the name of your website. Example ('email@your-website.com', 'your-website.com')) Send from a fixed, valid address in your own domain, perhaps one that allows you to easily identify that it originated on your contact form**

    //Send the message to yourself, or whoever should receive contact for submissions

    $mail->addAddress('alessandro.mazzari@luigidaltrozzo.it'); //**WRITE HERE RECIPIENT EMAIL ADDRESS (AT THIS ADDRESS EMAILS WILL BE COME)**

    $image = $_POST['image'];
    $data = substr($image, strpos($image, ","));

    $filename="Firma.png";
    $encoding = "base64";
    $type = "image/png";

    $image = $_POST['image'];
    $content = '.....';
    $mail->AddStringAttachment(base64_decode($data), $filename, $encoding, $type);

    if ($mail->addReplyTo($_POST['email'], $_POST['name'], $_POST['surname'], $_POST['company'], $_POST['promotion'], $_POST['agency'], $_POST['intermediary'] )) {
        $mail->Subject = 'Trattamento dati: '.$_POST['surname'];
        //Keep it simple - don't use HTML
        $mail->isHTML(false);
        $mail->CharSet = 'UTF-8';
        //Build a simple message body
        $mail->Body = <<<EOT
Email: {$_POST['email']}
Name: {$_POST['name']}
Surname: {$_POST['surname']}
Company: {$_POST['company']}
Promotion: {$_POST['promotion']}
Agency: {$_POST['agency']}
Intermediary: {$_POST['intermediary']}
EOT;
        //Send the message, check for errors
        if(!$mail->Send()) {
           $arrResult = array ('response'=>'error');
        }

          $arrResult = array ('response'=>'success');

          echo json_encode($arrResult);

        } else {

          $arrResult = array ('response'=>'error');
          echo json_encode($arrResult);

        }
    }
  };
}

?>
