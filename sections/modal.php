<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="">
          <form id="contact-form" action="php/contact-form-recaptcha.php" method="POST">

            <div class="row">

              <div class="col-md-6">
                <div class="row multiLangForm">
                </div>
              </div>

              <div class="col-md-6">
                <!-- <label>Message *</label> -->
                <textarea maxlength="5000" data-msg-required="Please enter your message" rows="4" class="controled " name="message" id="message" placeholder="MESSAGE" required></textarea>



              </div>

            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="checkboxes-right">
                  <h1 id="privacyAgreementTextUsed" class="lang" key="privacyMiniText"></h1>
                  <input type="radio" value="Yes" data-msg-required="Please choose an option to continue" class="controled-privacy uncheker2" name="agree" required><h5 class="lang" key="agree"></h5></input>
                  <input type="radio" value="No" data-msg-required="Please choose an option to continue" class="controled-privacy uncheker2" name="agree" required><h5 class="lang" key="noAgree"></h5></input>
                </div>
                <div class="right mt-50">

                  <!-- GOOGLE RECAPTHA -->
                  <div class="g-recaptcha" data-sitekey="6Lev4U4UAAAAAGACpOgExFjrts1nvMPARdcCCQvy"></div>
                </div>

              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="text-right text-center-xxs sendButtonContainer">
                </div>
              </div>
            </div>

          </form>
          <div class="alert alert-success hidden animated fadeIn lang" key="successForm" id="contactSuccess">
          </div>

          <div class="alert alert-danger hidden animated shake lang" key="errorForm" id="contactError">
          </div>
        </div>
      </div>
      <div class="modal-footer closeButtonContainer">
        <button type="button" class="button medium gray mt-40 lang" data-dismiss="modal" key="close"></button>
      </div>
    </div>

  </div>
</div>
