<html>
 	<head>
		<title>Luigi Dal Trozzo</title>
		<meta charset=utf-8>
		<meta name="robots" content="index, follow" >
		<meta name="keywords" content="HTML5" >
		<meta name="description" content="Attrezzature e macchinari per orafi, argentieri ed orologiai dal 1918 - Equipment and machinery for goldsmiths, silversmith and watchmakers since 1918" >
		<meta name="author" content="Alessandro Mazzari">

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- FAVICONS -->
    <link rel="shortcut icon" href="images/favicon/favicon.png">

<!-- CSS -->

    <!-- REVOSLIDER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.min.css" media="screen" />

    <!--  BOOTSTRAP -->
		<link rel="stylesheet" href="css/bootstrap.min.css">

    <!--  GOOGLE FONT -->
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700%7COpen+Sans:400,300,700' rel='stylesheet' type='text/css'>

    <!-- ICONS ELEGANT FONT & FONT AWESOME & LINEA ICONS  -->
		<link rel="stylesheet" href="css/icons-fonts.css" >

    <!--  CSS THEME -->
		<link rel="stylesheet" href="css/style.css">

    <!-- ANIMATE -->
		<link rel='stylesheet' href="css/animate.min.css">

    <link rel="stylesheet" type="text/css" href="css/jquery-eu-cookie-law-popup.css"/>
    <script src='https://www.google.com/recaptcha/api.js'></script>

	</head>
	<body>
		<!-- LOADER -->
		<div id="loader-overflow">
      <div id="loader3">Please enable JS</div>
    </div>

		<div id="wrap" class="boxed ">
			<div class="grey-bg"> <!-- Grey BG  -->

<!-- HEADER -->
<header id="nav" class="header header-1">
  <div class="header-wrapper">
  <div class="container-m-30 clearfix">
    <div class="logo-row">

    <!-- LOGO -->
    <div class="logo-container-2">
        <div class="logo-2">
          <a href="index.php" class="clearfix">
            <img id="logo-header" src="" class="logo-img" alt="Logo">
          </a>
        </div>
      </div>
    <!-- BUTTON -->
    <div class="menu-btn-respons-container">
      <button type="button" class="navbar-toggle btn-navbar collapsed" data-toggle="collapse" data-target="#main-menu .navbar-collapse">
        <span id="hambIcon" aria-hidden="true" class="icon_menu hamb-mob-icon"></span>
      </button>
    </div>
   </div>
  </div>

  <!-- MAIN MENU CONTAINER -->
  <div class="main-menu-container">

      <div class="container-m-30 clearfix">

        <!-- MAIN MENU -->
        <div id="main-menu">
          <div class="navbar navbar-default" role="navigation">

          <!-- MAIN MENU LIST -->
          <nav id="menuHamb" class="collapse collapsing navbar-collapse right-1024">
            <ul class="nav navbar-nav">

              <!-- MENU ITEM -->
              <li id="home" class="">
                <a href="index.php"><div class="main-menu-title change-color">HOME</div></a>
              </li>

              <!-- MENU ITEM -->
              <li id="news" class="">
                <a href="news.php"><div class="main-menu-title change-color">NEWS</div></a>
              </li>

              <!-- MENU ITEM -->
              <li id="secondhand" class="">
                <a href="second-hand.php"><div class="main-menu-title change-color lang" key="sh"></div></a>
              </li>

              <!-- MENU ITEM -->
              <li id="download" class="">
                <a href="download.php"><div class="main-menu-title change-color">DOWNLOAD</div></a>
              </li>

              <!-- MENU ITEM -->
              <li id="download" class="">
                <a href="faq.php"><div class="main-menu-title change-color">FAQ</div></a>
              </li>

              <!-- MENU ITEM -->
              <li class="">
                <a href="https://infinity.luigidaltrozzo.it"><div class="main-menu-title change-color">ESHOP</div></a>
              </li>

              <!-- MENU ITEM -->
              <li id="contact" class="parent megamenu">
                <a href="contacts.php"><div class="main-menu-title change-color lang" key="contact"></div></a>
                <ul class="sub">
                  <li class="clearfix" >
                    <div class="menu-sub-container">

                      <div class="box col-md-3 menu-demo-info closed">
                        <h5 class="title lang" key="contactPages"></h5>
                        <ul>
                        <li><a href="milano.php">Milano</a></li>
                        <li><a href="valenza.php">Valenza</a></li>
                        <li><a href="vicenza.php">Vicenza</a></li>
                        <li><a href="arezzo.php">Arezzo</a></li>
                        <li><a href="roma.php">Roma</a></li>
                        </ul>
                      </div>

                      <div class="col-md-3 menu-contact-info">
                        <ul class="contact-list">
                          <li class="contact-loc clearfix">
                            <div class="loc-icon-container">
                              <span aria-hidden="true" class="icon_pin_alt main-menu-contact-icon"></span>
                            </div>
                            <div class="menu-contact-text-container">Via Claudio Treves 26, Vimodrone 20090</div>
                          </li>
                          <li class="contact-phone clearfix">
                            <div class="loc-icon-container">
                              <span aria-hidden="true" class="icon_phone main-menu-contact-icon"></span>
                            </div>
                            <div class="menu-contact-text-container">+39 02 2885871</div>
                          </li>
                          <li class="contact-mail clearfix" >
                            <div class="loc-icon-container">
                              <span aria-hidden="true" class="icon_mail_alt main-menu-contact-icon"></span>
                            </div>
                            <div class="menu-contact-text-container">
                              <a class="a-mail" href="mailto:info@luigidaltrozzo.it">info@luigidaltrozzo.it</a>
                            </div>
                          </li>
                        </ul>
                      </div>

                      <div class="col-md-6 menu-map-container hide-max-960 ">
                        <!-- Google Maps -->
                          <div class="fb-page fb-box"
                               data-href="https://www.facebook.com/luigidaltrozzo1918/"
                               data-tabs="messages"
                               data-width="750"
                               data-height="250"
                               data-small-header="true">
                            <div class="fb-xfbml-parse-ignore">
                              <blockquote></blockquote>
                            </div>
                        </div>
                        <!-- Google Maps / End -->
                      </div>
                    </div>
                  </li>
                </ul>
              </li>

              <!-- MENU ITEM -->
              <!-- <li class="parent">
                <a href="#"><div class="main-menu-title change-color lang" key="language"></div></a>
                <ul class="sub">
                <button class="button medium gray translate lang" key="langIta" id="it"></button>
                <button class="button medium gray translate lang" key="langEng" id="en"></button>
                </ul>
              </li> -->

          </nav>

          </div>
        </div>
        <!-- END main-menu -->

      </div>
      <!-- END container-m-30 -->

  </div>
  <!-- END main-menu-container -->

  </div>
  <!-- END header-wrapper -->

</header>
