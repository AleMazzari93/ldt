<!-- REVO SLIDER FULLSCREEN 1 -->
<div class="relative">
  <div class="rs-fullscr-container">

    <div id="rs-fullscr" class="tp-banner" >
      <ul>
        <!-- SLIDE 1 -->
        <li data-transition="zoomout" data-slotamount="1" data-masterspeed="1500" data-thumb="#"  data-saveperformance="on"  data-title="BEGIN">
          <!-- MAIN IMAGE -->

          <img src="images/revo-slider/dummy.png"  alt="slidebg1" data-lazyload="images/revo-slider/slide_1_1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">


          <!-- LAYERS -->

          <!-- LAYER NR. 0 BG CAPTIONS -->
          <div class="tp-caption rs-parallaxlevel-4 zoomout"
          data-x="left"
          data-y="center"

          data-speed="1300"
          data-start="200"

          data-easing="Power3.easeInOut"
          style="z-index: 0;">
          <div class=""></div>
        </div>

        <!--PARALLAX & OPACITY container -->
        <div class="rs-parallaxlevel-4 opacity-scroll2">

          <!-- LAYER NR. 1 -->
          <div class="tp-caption dark-black-63 sfb tp-resizeme"
          data-x="center"
          data-hoffset="0"
          data-y="center"
          data-voffset="0"
          data-speed="500"
          data-start="1000"
          data-easing="Power1.easeInOut"
          data-splitin="none"
          data-splitout="none"
          data-elementdelay="0.1"
          data-endelementdelay="0.1"
          style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap; color: white; font-family: marketDeco; font-size: 7em;" <span class="bold">LUIGI DAL TROZZO</span>
        </div>

        <!-- LAYER NR. 4 -->
        <div class="tp-caption dark-light-63 sfb tp-resizeme lang"
        data-x="center"
        data-y="450"
        data-speed="750"
        data-start="900"
        data-easing="Power1.easeInOut"
        data-splitin="none"
        data-splitout="none"
        data-elementdelay="0.1"
        data-endelementdelay="0.1"
        key="subMain"
        style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap; color: white; font-size : 1.5em;">
      </div>

      <!-- LAYER NR. 3 -->
      <div class="tp-caption center-0-478 sfb"
      data-x="center"
      data-y="550"
      data-speed="900"
      data-start="1350"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><a class="button medium gray btn-5 btn-5aa" href="https://infinity.luigidaltrozzo.it/InfinityLDT/catalog/articolo/415-saldatura/grdplnrnwh-laser-saldatura-dado---aaa.html"><span aria-hidden="true" class="button-icon-anim icon-ecommerce-bag"></span><span class="button-text-anim lang" key="buyButton"></span></a>
      </div>

    </li>

    <!-- SLIDE 2 -->
    <li data-transition="zoomout" data-slotamount="1" data-masterspeed="1500" data-thumb="#"  data-saveperformance="on"  data-title="IMAGINATION">

      <!-- MAIN IMAGE -->
      <img src="images/revo-slider/dummy.png"  alt="slidebg2" data-lazyload="images/revo-slider/slide_1_2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

      <!-- LAYERS -->

      <!-- LAYER NR. 0 BG CAPTIONS -->
      <div class="tp-caption rs-parallaxlevel-4 zoomout"
      data-x="left"
      data-y="center"
      data-speed="1300"
      data-start="200"
      data-easing="Power3.easeInOut"
      style="z-index: 0;">
      <div class=""></div>
    </div>

    <!--PARALLAX & OPACITY container -->
    <div class="rs-parallaxlevel-4 opacity-scroll2">

      <!-- LAYER NR. 1 -->
      <div class="tp-caption center-0-478 dark-light-61 sfb tp-resizeme lang"
      data-x="650"
      data-y="255"
      data-speed="500"
      data-start="850"
      data-easing="Power1.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      key="secondSlide1"
      style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap; color: white;">
    </div>

    <!-- LAYER NR. 2 -->
    <div class="tp-caption center-0-478 dark-light-54 sfb tp-resizeme lang"
    data-x="650"
    data-y="330"
    data-speed="500"
    data-start="1050"
    data-easing="Power1.easeInOut"
    data-splitin="none"
    data-splitout="none"
    data-elementdelay="0.1"
    data-endelementdelay="0.1"
    key="secondSlide2"
    style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap; color: white;">
  </div>

  <!-- LAYER NR. 3 -->
  <div class="tp-caption center-0-478 dark-black-63 sfb tp-resizeme lang"
  data-x="650"
  data-y="407"
  data-speed="900"
  data-start="1500"
  data-easing="Power3.easeInOut"
  data-splitin="none"
  data-splitout="none"
  data-elementdelay="0.1"
  data-endelementdelay="0.1"
  key="secondSlide3"
  style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; color: white;">
</div>

<!-- LAYER NR. 3 -->
<div class="tp-caption center-0-478 sfb"
data-x="650"
data-y="465"
data-speed="900"
data-start="1350"
data-easing="Power3.easeInOut"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.1"
data-endelementdelay="0.1"
style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><a class="button medium gray btn-5 btn-5aa" href="https://infinity.luigidaltrozzo.it/InfinityLDT/catalog/articolo/401-prototipazione-e-modellazione/gmxorhdhsa-resina-universale-fondibile-bc-x10-lcddlp-qualit.html"><span aria-hidden="true" class="button-icon-anim icon-ecommerce-bag"></span><span class="button-text-anim lang" key="buyButton"></span></a>
</div>

</li>

</ul>

</div>

</div>

<!-- SCROLL ICON -->
<div class="local-scroll-cont">
  <a href="#shop-dep" class="scroll-down smooth-scroll">
    <div class="icon icon-arrows-down"></div>
  </a>
</div>
</div>
</div>
</div>
