<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">RISULTATO</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 id="resultWeight"></h1>
        <h1 id="resultTime"></h1>
        <ul class="toggle-view-custom" style="list-style-type: none;">
          <li>
            <h3 class="ui-accordion-header"><span class="link"></span>DETTAGLI</h3>
            <div class="panel">
              <p id="details"></p>
            </div>
          </li>
        </ul>
      </div>
      <div class="modal-footer">
        <button id="savePdf" type="button" class="button medium gray mt-40">SALVA PDF</button>
        <button type="button" class="button medium gray mt-40" data-dismiss="modal">CHIUDI</button>
      </div>
    </div>
  </div>
</div>
