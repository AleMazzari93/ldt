
<div class="page-title-cont page-title-small grey-light-bg">
  <div class="relative container align-left">
    <div class="row">

      <div class="col-md-8">
        <h1 class="page-title lang" key="">MODULO PRIVACY</h1>
      </div>

      <div class="col-md-4">
        <div class="breadcrumbs">
          <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current lang" key="">MODULO PRIVACY</span>
        </div>
      </div>

    </div>
  </div>
</div>

<div style="text-align: left; margin: 80px 40px 40px 40px">
  <h2>Privacy</h2>
  <p style="color:#a7192f"><strong>Informativa sul trattamento dei dati personali ex artt. 13-14 Reg.to UE 2016/679</strong></p>
  <p><strong>Soggetti Interessati:clienti.<strong></p>

  <p>LUIGI DAL TROZZODI C.A. MAZZARI & C. S.A.S. nella qualità di Titolare del trattamento dei Suoi dati personali, aisensi e per gli effetti del Reg.to UE
2016/679 diseguito 'GDPR', con la presente La informa che la citata normativa prevede la tutela degli interessatirispetto al trattamento dei dati
personali e che tale trattamento sarà improntato ai principi dicorrettezza, liceità, trasparenza e di tutela della Sua riservatezza e dei Suoi diritti.
I Suoi dati personali verranno trattati in accordo alle disposizioni legislative della normativa sopra richiamata e degli obblighi diriservatezza ivi previsti.
Finalità e base giuridica del trattamento: in particolare i Suoi dati verranno trattati per le seguenti finalità connesse all'attuazione di adempimentirelativi
ad obblighi legislativi:
    <ul class="ul-privacy">
      <li>adempimenti obbligatori per legge in campo fiscale e contabile;</li>
      <li>gestione delcontenzioso;</li>
      <li>di obblighi previsti dalle leggi vigenti.</li>
    </ul>
  </p>
  <p>I Suoi datisaranno inoltre utilizzati per le seguenti finalità relative all’esecuzione dimisure connesse ad obblighicontrattuali o pre-contrattuali:</p>
    <ul class="ul-privacy">
      <li>a mezzo calcolatori elettronici con utilizzo disistemisoftware gestiti o programmati direttamente.</li>
      <li>assistenza post-vendita;</li>
      <li>gestione della clientela;</li>
      <li>programmazione delle attività;</li>
      <li>storico fatturazione clienti.</li>

    </ul>
    <p>I Suoi dati personali potranno inoltre, previo suo consenso, essere utilizzati per le seguenti finalità:</p>

    <ul class="ul-privacy">
      <li>per rendere informazioni su future iniziative commerciali e su annunci di nuovi prodotti, servizi e offerte, sia da parte nostra sia da parte di
società affiliate e/o controllate e partners commerciali.</li>
    </ul>
  <p>Il conferimento dei dati è per Lei facoltativo riguardo alle sopraindicate finalità, ed un suo eventuale rifiuto al trattamento non compromette la
prosecuzione delrapporto o la congruità del trattamento stesso.
Modalità del trattamento. I suoi dati personali potranno essere trattati nei seguenti modi:</p>

    <ul class="ul-privacy">
      <li>paesi UE.</li>
      <li>affidamento a terzi di operazioni di elaborazione;</li>
      <li>creazione di profilirelativi a clienti, fornitori o consumatori;</li>
      <li>elaborazione di datiraccolti da terzi;</li>
      <li>raccolta di dati per via informatica o telematica.;</li>
      <li>raccolta di dati presso registri, elenchi, atti o documenti pubblici.;</li>
      <li>trattamento a mezzo dicalcolatori elettronici;</li>
      <li>trattamentomanuale a mezzo di archivicartacei.</li>
    </ul>
    <p>Ogni trattamento avviene nelrispetto delle modalità dicui agli artt. 6, 32 del GDPR e mediante l'adozione delle adeguate misure disicurezza previste.
I suoi datisaranno trattati unicamente da personale espressamente autorizzato dal Titolare ed, in particolare, dalle seguenticategorie di addetti:
    </p>
    <ul class="ul-privacy">
    <li>distributori;</li>
    <li>spedizionieri;</li>
    <li>ufficio Acquisti;</li>
    <li>ufficio Amministrazione;</li>
    <li>ufficio Marketing.</li>
  </ul>
    <p>Comunicazione: I suoi dati potranno essere comunicati a soggetti esterni per una corretta gestione delrapporto ed in particolare alle seguenticategorie di
Destinatari tra cui tutti i Responsabili del Trattamento debitamente nominati:
I suoi dati potranno essere comunicati a terzi debitamente nominati Responsabili al trattamento, ilcui elenco sarà a disposizione su semplice richiesta
dell'Interessato, in particolare a:
</p>
  <ul class="ul-privacy">
    <li>agenzie di intermediazione;</li>
    <li>banche e istituti dicredito;</li>
    <li>consulenti e liberi professionisti, anche in forma associata;</li>
    <li>nell'ambito disoggetti pubblici e/o privati per i quali la comunicazione dei dati è obbligatoria o necessaria in adempimento ad obblighi di legge o
    sia comunque funzionale all'amministrazione delrapporto;</li>
    <li>intermediari;</li>
    <li>spedizionieri, Trasportatori, Padroncini, Poste, Aziende per la Logistica.</li>
    <p>Diffusione: I suoi dati personali non verranno diffusi in alcunmodo.
    Periodo di Conservazione. Le segnaliamo che, nelrispetto dei principi di liceità, limitazione delle finalità e minimizzazione dei dati, aisensi dell’art. 5 del
    GDPR, il periodo diconservazione dei Suoi dati personali è:
    </p>
    <li>stabilito per un arco di tempo non superiore alconseguimento delle finalità per le qualisono raccolti e trattati per l'esecuzione e l'espletamento
    delle finalità contrattuali;</li>
    <li>stabilito per un arco di tempo non superiore all'espletamento deiservizi erogati;</li>
    <li>stabilito per un arco di tempo non superiore alconseguimento delle finalità per le qualisono raccolti e trattati e nelrispetto dei tempi obbligatori
    prescritti dalla legge.</li>
  </ul>
  <p>Gestione dei cookie: nelcaso in cui Lei abbia dubbi o preoccupazioni inmerito all'utilizzo deicookie Le è sempre possibile intervenire per impedirne
l'impostazione e la lettura, ad esempiomodificando le impostazionisulla privacy all'interno del Suo browser al fine di bloccarne determinati tipi.
Poiché ciascun browser - e spesso diverse versioni dello stesso browser - differiscono anche sensibilmente le une dalle altre se preferisce agire
autonomamente mediante le preferenze del Suo browser può trovare informazioni dettagliate sulla procedura necessaria nella guida del Suo browser. Per
una panoramica delle modalità di azione per i browser più comuni, può visitare l'indirizzo www.cookiepedia.co.uk.
Le società pubblicitarie consentono inoltre dirinunciare alla ricezione di annuncimirati, se lo si desidera. Ciò non impedisce l'impostazione deicookie, ma
interrompe l'utilizzo e la raccolta di alcuni dati da parte di talisocietà.
Permaggiori informazioni e possibilità dirinuncia, visitare l'indirizzo www.youronlinechoices.eu/.
Titolare: il Titolare del trattamento dei dati, aisensi della Legge, è LUIGI DAL TROZZODI C.A. MAZZARI & C. S.A.S. (Via Claudio Treves 26 , 20090
Vimodrone (MI); P.Iva: 08015150157) nella persona delsuo legale rappresentante pro tempore.
Lei ha diritto di ottenere dal titolare la cancellazione (diritto all'oblio), la limitazione, l'aggiornamento, la rettificazione, la portabilità, l'opposizione al
trattamento dei dati personaliche La riguardano, nonché in generale può esercitare tutti i diritti previsti dagli artt. 15, 16, 17, 18, 19, 20, 21, 22 del
GDPR.
</p>
  <p style="color:#a7192f"><strong>Reg.to UE 2016/679: Artt. 15, 16, 17, 18, 19, 20, 21, 22 - Diritti dell'Interessato</strong></p>
  <p>1. L'interessato ha diritto di ottenere la conferma dell'esistenza omeno di dati personaliche lo riguardano, anche se non ancora registrati, la loro
comunicazione in forma intelligibile e la possibilità di effettuare reclamo presso l’Autorità dicontrollo.<br>
2. L'interessato ha diritto di ottenere l'indicazione:
</p>
<ul class="ul-privacy">
  <li>a. dell'origine dei dati personali;</li>
  <li>b. delle finalità e modalità del trattamento;</li>
  <li>c. della logica applicata in caso di trattamento effettuato con l'ausilio distrumenti elettronici;</li>
  <li>d. degli estremi identificativi del titolare, deiresponsabili e delrappresentante designato aisensi dell'articolo 5, comma 2;</li>
  <li>e. deisoggetti o delle categorie disoggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di
  rappresentante designato nel territorio dello Stato, diresponsabili o incaricati.</li>
</ul>
<p>3. L'interessato ha diritto di ottenere:</p>
<ul class="ul-privacy">
  <li>a. l'aggiornamento, la rettificazione ovvero, quando vi ha interesse, l'integrazione dei dati;</li>
  <li>b. la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli dicui non è necessaria la
  conservazione in relazione agliscopi per i quali i datisono statiraccolti o successivamente trattati;</li>
  <li>c. l'attestazione che le operazioni dicui alle lettere a) e b) sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, dicoloro ai
  quali i datisono staticomunicati o diffusi, eccettuato ilcaso in cui tale adempimento sirivela impossibile o comporta un impiego dimezzi
  manifestamente sproporzionato rispetto al diritto tutelato;</li>
  <li>d. la portabilità dei dati.</li>
</ul>

<p>4. L'interessato ha diritto di opporsi, in tutto o in parte:</p>
  <ul class="ul-privacy">
    <li>a. permotivi legittimi al trattamento dei dati personaliche lo riguardano, ancorché pertinenti allo scopo della raccolta;</li>
    <li>b. al trattamento di dati personaliche lo riguardano a fini di invio dimateriale pubblicitario o di vendita diretta o per ilcompimento diricerche di
mercato o dicomunicazione commerciale.</li>
  </ul>
  <br>

  <p style="font-size:1.6em;"><strong>Formula di acquisizione delconsenso dell'interessato:</strong></p>
  <br>
  <p>Ilsottoscritto interessato, acquisite le informazioni fornite dal titolare del trattamento aisensi dell'articolo 7 del Reg.to UE 2016/679, (documento di
informativa n. 11611.51.355434.1240547):
Presta il suo consenso al trattamento dei dati personali per le finalità facoltative sotto riportate?
</p>




<div class="page-section pt-110-b-80-cont">
  <div class="container">
    <div class="row">
      <div class="col-md-12">


          <div class="">
            <form id="privacy-form" action="php/privacy_form.php" method="POST" enctype="multipart/form-data">

              <div class="row">

                <div class="col-md-6">
                  <div class="row">

                    <div class="col-md-12 mb-23">
                      <input type="text" value="" data-msg-required="Si prega di immettere il nome" maxlength="100" class="controled-privacy form-privacy-width" name="name" id="name" placeholder="NOME" required>
                    </div>

                    <div class="col-md-12 mb-23">
                      <input type="text" value="" data-msg-required="Si prega di immettere il cognome" maxlength="100" class="controled-privacy form-privacy-width" name="surname" id="surname" placeholder="COGNOME" required>
                    </div>

                    <div class="col-md-12 mb-23">
                      <input type="text" value="" data-msg-required="Perfavore immettere il nome dell'azienda" maxlength="100" class="controled-privacy form-privacy-width" name="company" id="company" placeholder="AZIENDA">
                    </div>

                    <div class="col-md-12 mb-23">

                      <input type="email" value="" data-msg-required="Perfavore immettere l'indirizzo mail" data-msg-email="Please enter a valid email address" maxlength="100" class="controled-privacy form-privacy-width" name="email" id="email" placeholder="EMAIL" required>
                    </div>

                    </div>
                  </div>

                    <div class="col-md-12">
                      <ul class="ul-privacy">
                        <li> per rendere informazioni su future iniziative commerciali e su annunci di nuovi prodotti, servizi e offerte, sia da parte nostra sia
da parte disocietà affiliate e/o controllate e partners commerciali
</li>
                      </ul>
                      <div class="checkboxes-right">
                        <input type="radio" value="Yes" data-msg-required="Perfavore acconsentire per continuare" class="controled-privacy uncheker" name="promotion" required>Acconsento</input>
                        <input type="radio" value="No" data-msg-required="Perfavore acconsentire per continuare" class="controled-privacy uncheker" name="promotion" required>Non acconsento</input>
                      </div>

                      <p>Presta il suo consenso alla comunicazione dei propri dati personali?</p>
                      <ul class="ul-privacy">
                        <li>agenzie di intermediazione</li>
                      </ul>
                      <div class="checkboxes-right">
                        <input type="radio" value="Yes" data-msg-required="Perfavore acconsentire per continuare" class="controled-privacy uncheker1" name="agency" required>Acconsento</input>
                        <input type="radio" value="No" data-msg-required="Perfavore acconsentire per continuare" class="controled-privacy uncheker1" name="agency" required>Non acconsento</input>
                      </div>
                      <ul class="ul-privacy">
                        <li>intermediari</li>
                      </ul>
                      <div class="checkboxes-right">
                        <input type="radio" value="Yes" data-msg-required="Perfavore acconsentire per continuare" class="controled-privacy uncheker2" name="intermediary" required>Acconsento</input>
                        <input type="radio" value="No" data-msg-required="Perfavore acconsentire per continuare" class="controled-privacy uncheker2" name="intermediary" required>Non acconsento</input>
                      </div>
                    </div>

                    <div class= "col-md-12 canvasDiv">
                      <h1>FIRMA</h1>
                      <button class="button medium gray buttonCanvas" type="button" onclick="signaturePad.clear();" name="button">Riprova</button>
                      <canvas id="signatureCanvas" width="800px" height="600px"></canvas>
                      <input id="hidden_input" type="hidden" name="image" value="">
                    </div>


              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="right mt-50">


                    <div class="g-recaptcha" data-sitekey="6Lev4U4UAAAAAGACpOgExFjrts1nvMPARdcCCQvy"></div>
                    </div>

                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="text-right text-center-xxs">
                    <button type="submit" onclick="encodingSignature();" name="enter" value="SEND MESSAGE" class="button medium gray mt-40 lang" data-loading-text="Loading..." key="sendButton"></button>
                  </div>
                </div>
              </div>
                <button type="button" onclick="reloadPage()" value="" class="button medium gray mt-40 lang">NUOVA RICHIESTA</button>

            </form>
            <div class="alert alert-success hidden animated fadeIn lang" id="contactSuccess">Grazie! Il suo modulo è stato compilato ed inviato correttamente.
            </div>

            <div class="alert alert-danger hidden animated shake lang" id="contactError">Errore! Qualcosa è andato storto. Contattare l'amministratore del sistema.
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
