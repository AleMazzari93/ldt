<hr class="mt-0 mb-0">

<!-- FOOTER 2 -->
<footer id="footer2" class="page-section pt-80 pb-50">
  <div class="container">
    <div class="row">

      <div class="col-md-3 col-sm-3 widget">
        <div class="logo-footer-cont">
          <a href="index.php">
            <img class="logo-footer" src="images/logo.png" alt="logo">
          </a>
        </div>
        <div class="footer-2-text-cont">
          <address>
            Via Claudio Treves, 26<br>
            Vimodrone, MI 20090
          </address>
        </div>
        <div class="footer-2-text-cont">
          02 2885871
        </div>
        <div class="footer-2-text-cont">
          <a class="a-text" href="mailto:info@luigidaltrozzo.it">info@luigidaltrozzo.it</a>
        </div>
      </div>

      <div class="col-md-3 col-sm-3 widget">
        <h4 class="lang" key="navigate"></h4>
        <ul class="links-list bold a-text-cont">
          <li><a href="index.php">HOME</a></li>
          <li><a href="news.php">NEWS</a></li>
          <li><a class="lang" key="sh" href="second-hand.php"></a></li>
          <li><a href="https://infinity.luigidaltrozzo.it">ESHOP</a></li>
        </ul>
      </div>

      <div class="col-md-3 col-sm-3 widget">
        <h4 class="lang" key="about-us"></h4>
        <ul class="links-list a-text-cont" >
          <li><a class="lang" key="liveChat" href="https://m.me/luigidaltrozzo1918"></a></li>
          <li><a class="lang" key="terms" href="terms.php"></a></li>
          <li><a href="faq.php">FAQ</a></li>
          <li><a href="cookies.php">COOKIE</a></li>
          <li><a href="privacy.php">PRIVACY</a></li>
          <li><a href="contacts.php" class="lang" key="contact"></a></li>
          <?php include "sections/password.php";?>
        </ul>
      </div>
    </div>

    <div class="footer-2-copy-cont clearfix">
      <!-- Social Links -->
      <div class="footer-2-soc-a right">
        <a href="https://www.facebook.com/luigidaltrozzo1918/" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
      </div>

      <!-- Copyright -->
      <div class="left lang" key="pIva">
        <a class="footer-2-copy"></a>
      </div>
    </div>

  </div>

</footer>



<!-- BACK TO TOP -->
<!-- <p id="back-top">
  <a href="#top" title="Back to Top"><span class="icon icon-arrows-up"></span></a>
</p> -->

</div><!-- End BG -->
</div><!-- End wrap -->

<!-- JS begin -->

<!-- jQuery  -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<!-- MAGNIFIC POPUP -->
<script src='js/jquery.magnific-popup.min.js'></script>

<!-- PORTFOLIO SCRIPTS -->
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>

<!-- COUNTER -->
<script type="text/javascript" src="js/jquery.countTo.js"></script>

<!-- APPEAR -->
<script type="text/javascript" src="js/jquery.appear.js"></script>

<!-- OWL CAROUSEL -->
<script type="text/javascript" src="js/owl.carousel.min.js"></script>

<!-- MAIN SCRIPT -->
<script src="js/main.js"></script>
<script src="js/lang.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/my-scripts.js"></script>
<script src="js/contact-form-validation-recaptcha.js"></script>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="js/signaturePad.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>

<!-- GOOLE MAP -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCp0Pil0hMp3_xpsqod5eGRd9DOXrQuQYc"></script>
<script type="text/javascript" src="js/gmap3.min.js"></script>
<script src="js/jquery-eu-cookie-law-popup.js"></script>


<!-- REVOSLIDER SCRIPTS  -->

<!-- REVOSLIDER INIT -->
<script type="text/javascript" src="js/revo-slider-init.js"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution-parallax.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110088935-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110088935-1');
</script>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<!-- JS end -->

</body>
</html>
