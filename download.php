<!DOCTYPE html>
        <?php include "sections/header.php";?>

        <!-- PAGE TITLE -->
        <div class="page-title-cont page-title-small grey-light-bg">
          <div class="relative container align-left">
            <div class="row">

              <div class="col-md-8">
                <h1 class="page-title">DOWNLOAD</h1>
              </div>

              <div class="col-md-4">
                <div class="breadcrumbs">
                  <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current">DOWNLOAD</span>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="page-section " >
          <div class="container fes4-cont">
            <div class="row">
              <div class="col-md-12">
                <div class="row">

                  <div class="col-md-6">
                    <div class="fes4-box">
                      <div class="fes4-title-cont" >
                        <div class="fes4-box-icon">
                          <div class="icon icon-basic-book"></div>
                        </div>
                        <h3><span class="bold lang" key="catalog"></span></h3>
                      </div>
                      <a href="https://drive.google.com/file/d/1B62yqRNaaEB4Prss8B_5bIuR7HX-fOkD/view?usp=sharing" target="_blank"><div class="fa fa-eye top-icon-download download-icon"></div></a>
                      <a href="https://drive.google.com/uc?export=download&id=1B62yqRNaaEB4Prss8B_5bIuR7HX-fOkD"><div class="fa fa-download bottom-icon-download download-icon"></div></a>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="fes4-box">
                      <div class="fes4-title-cont" >
                        <div class="fes4-box-icon">
                          <div class="icon icon-basic-anticlockwise"></div>
                        </div>
                        <h3><span class="bold lang" key="historyCatalog"></span></h3>
                      </div>
                      <a href="https://drive.google.com/file/d/1slrmWyflTqHSDQ2yJ6IUtA7Xnk-iHa_4/view?usp=sharing" target="_blank"><div class="fa fa-eye top-icon-download download-icon"></div></a>
                      <a href="https://drive.google.com/uc?export=download&id=1slrmWyflTqHSDQ2yJ6IUtA7Xnk-iHa_4"><div class="fa fa-download bottom-icon-download download-icon"></div></a>
                    </div>
                  </div>

                </div>

                <div class="row">

                  <div class="col-md-6">
                    <div class="fes4-box">
                      <div class="fes4-title-cont" >
                        <div class="fes4-box-icon">
                          <div class="icon icon-basic-book"></div>
                        </div>
                        <h3><span class="bold lang" key="grs"></span></h3>
                      </div>
                      <a href="https://drive.google.com/file/d/1O9S-daY3izuA5yeyQ7RSPJVmo7OCjqjo/view?usp=sharing" target="_blank"><div class="fa fa-eye top-icon-download download-icon"></div></a>
                      <a href="https://drive.google.com/uc?export=download&id=1O9S-daY3izuA5yeyQ7RSPJVmo7OCjqjo"><div class="fa fa-download bottom-icon-download download-icon"></div></a>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="fes4-box">
                      <div class="fes4-title-cont" >
                        <div class="fes4-box-icon">
                          <div class="icon icon-basic-sheet-multiple"></div>
                        </div>
                        <h3><span class="bold lang" key="sds"></span></h3>
                      </div>
                      <a href="https://drive.google.com/drive/folders/0B06hhDbiZ4CHNUdsUnp3bHdCSkU?usp=sharing" target="_blank"><div class="fa fa-eye top-icon-download download-icon"></div></a>
                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>

        </div>


        <?php include "sections/footer.php";?>
