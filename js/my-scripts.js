  // document.getElementById("main-menu").addEventListener("click", changeCurrent);

$(document).ready(function() {

  if (window.location.pathname == '/index.php') {
    document.getElementById("news").classList.remove("current");
    document.getElementById("secondhand").classList.remove("current");
    document.getElementById("download").classList.remove("current");
    document.getElementById("contact").classList.remove("current");
    document.getElementById("home").classList.add("current");
  } else if (window.location.pathname == '/') {
    document.getElementById("news").classList.remove("current");
    document.getElementById("secondhand").classList.remove("current");
    document.getElementById("download").classList.remove("current");
    document.getElementById("contact").classList.remove("current");
    document.getElementById("home").classList.add("current");
  } else if (window.location.pathname == '/news.php') {
    document.getElementById("home").classList.remove("current");
    document.getElementById("secondhand").classList.remove("current");
    document.getElementById("download").classList.remove("current");
    document.getElementById("contact").classList.remove("current");
    document.getElementById("news").classList.add("current");
  } else if (window.location.pathname == '/second-hand.php') {
    document.getElementById("home").classList.remove("current");
    document.getElementById("news").classList.remove("current");
    document.getElementById("download").classList.remove("current");
    document.getElementById("contact").classList.remove("current");
    document.getElementById("secondhand").classList.add("current");
  } else if (window.location.pathname == '/download.php') {
    document.getElementById("home").classList.remove("current");
    document.getElementById("secondhand").classList.remove("current");
    document.getElementById("news").classList.remove("current");
    document.getElementById("contact").classList.remove("current");
    document.getElementById("download").classList.add("current");
  } else if (window.location.pathname == '/milano.php' || window.location.pathname == '/valenza.php' || window.location.pathname == '/vicenza.php' || window.location.pathname == '/arezzo.php' || window.location.pathname == '/hq.php' || window.location.pathname == '/contacts.php') {
    document.getElementById("home").classList.remove("current");
    document.getElementById("secondhand").classList.remove("current");
    document.getElementById("news").classList.remove("current");
    document.getElementById("download").classList.remove("current");
    document.getElementById("contact").classList.add("current");
  }
});

$(document).ready(function() {
  if (window.location.pathname == '/index.php' || window.location.pathname == '/') {
    $('#logo-header').attr('src', 'images/logo_white.png');
    $('.hamb-mob-icon').css('color', 'white');
  } else if (window.location.pathname !== '/index.php') {
    $('#logo-header').attr('src', 'images/logo.png');
    $('.change-color').css('color', '#4b4e53');
    $('.current').css('color', '#4b4e53');
  }
});

$(window).scroll(function () {
  if (window.location.pathname == '/index.php' || window.location.pathname == '/') {
    if ($(document).scrollTop() == 0) {
        $('#logo-header').attr('src', 'images/logo_white.png');
        $('.change-color').css('color', 'white');
        $('.current').css('color', 'white');
    } else if (window.location.pathname == '/index.php' && $(window).innerWidth() > 1025 || window.location.pathname == '/' && $(window).innerWidth() > 1025) {
        $('#logo-header').attr('src', 'images/logo.png');
        $('.change-color').css('color', '#4b4e53');
        $('.current').css('color', '#4b4e53');
    } else if ($(window).innerWidth() < 1025) {
        $('.change-color').css('color', 'white');
        $('.current').css('color', 'white');
    }
  }
});

$( "#hambIcon" ).click(function() {
  var getHamb = document.getElementById("menuHamb");
  var check = getHamb.classList.contains("in");

  if (check == false) {
    $("#hambIcon").removeClass("icon_menu")
    $("#hambIcon").addClass("icon_close")
  } else if (check == true) {
    $("#hambIcon").removeClass("icon_close")
    $("#hambIcon").addClass("icon_menu")
  }

  if (check == false) {
    $('.hamb-mob-icon').css('color', 'white');
  }

  if ((check == true && window.location.pathname !== '/index.php') && (check == true && window.location.pathname !== '/')) {
    $('.hamb-mob-icon').css('color', '');
  } else if (check == true && window.location.pathname == '/index.php') {
    $('.hamb-mob-icon').css('color', 'white');
  } else if (check == true && window.location.pathname == '/') {
    $('.hamb-mob-icon').css('color', 'white');
  };
});

$(document).ready(function() {
  if ($(window).innerWidth() < 1025) {
    $('.change-color').css('color', 'white');
    $('.current').css('color', 'white');
  }
});

$('#myModal').on('show.bs.modal', function (event) {
  var langGet = localStorage.getItem('langSet');
  var button = $(event.relatedTarget);
  var product = button.data('product');
  var modal = $(this);
  if (langGet == 'en') {
    modal.find('.modal-title').text('Info request for ' + product)
    modal.find('.modal-body textarea').val('Hello, i would like to know more about : ' + product)
  } else if (langGet == 'it') {
    modal.find('.modal-title').text('Richiesta informazioni per ' + product)
    modal.find('.modal-body textarea').val('Buongiorno, ho bisogno di informazioni riguardanti a : ' + product)
  }
});

//COUNTDOWN -----------------------------------------------------------------------------
$('#clock').countdown('2018/5/1').on('update.countdown', function(event) {
   var $this = $(this).html(event.strftime(''
   + '<div class="col-xs-12 col-sm-6 col-md-3"><div class="countdown-item-container"><span class="countdown-amount">%D</span><span class="countdown-period">day%!D</span></div></div>'
   + '<div class="col-xs-12 col-sm-6 col-md-3"><div class="countdown-item-container"><span class="countdown-amount">%H</span><span class="countdown-period">hour%!H</span></div></div>'
   + '<div class="col-xs-12 col-sm-6 col-md-3"><div class="countdown-item-container"><span class="countdown-amount">%M</span><span class="countdown-period">minute%!M</span></div></div>'
   + '<div class="col-xs-12 col-sm-6 col-md-3"><div class="countdown-item-container"><span class="countdown-amount">%S</span><span class="countdown-period">second%!S</span></div></div>'));
 });

//FORM-SHIPPING

function formShippingSdaNational() {

 var width = document.getElementById('packWidth').value;
 var depth = document.getElementById('packDeep').value;
 var height = document.getElementById('packHeight').value;
 var weight = document.getElementById('weight').value;
 var isInItaly = document.getElementById('isInItaly').checked;
 var countersign = document.getElementById('countersign').checked;
 var packagingType = document.getElementById('sel1').value;
 var isItUsed = document.getElementById('isUsed').checked;
 var goodsValue = document.getElementById('goodsValue').value;

 var weightDefaultOver = 50;
 var weightDifference = 0;
 var overWeightPrice = 0;
 var totalPriceWeightBased = 0;
 var weightMax = 20;
 var countersignDefaultPrice = 5;
 var totalWithCountersign = 0;
 var percentageOnGoods = (goodsValue * 0.02);
 var packagingPrice = 0;

 var volumetricFirst = (width * depth * height);
 var volumetricSecond = 5000;
 var volumetricWeight = (volumetricFirst / volumetricSecond);
 var grandTotal = 0;

 //PESO VOULMETRICO
 if (volumetricWeight > weight) {
   weight = volumetricWeight;
   alert("Il peso volumetrico supera il peso della merce.");
 };

 //CONTROLLO DEL PESO
 if(isInItaly == false) {
   if (weight > 0 && weight <= 3) {
     totalPriceWeightBased = 9;
   } else if (weight >= 3.1 && weight <= 10) {
     totalPriceWeightBased = 12;
   } else if (weight >= 10.1 && weight <= 20) {
     totalPriceWeightBased = 14;
   } else if (weight >= 20.1 && weight <= 30) {
     totalPriceWeightBased = 16;
   } else if (weight >= 30.1 && weight <= 50) {
     totalPriceWeightBased = 20;
   } else if (weight >= 50.1) {
     weightDifference = (weight - weightDefaultOver);
     overWeightPrice = (weightDifference * 0.45);
     totalPriceWeightBased = (weightMax + overWeightPrice);
   }
 };

 if (isInItaly == true) {
   if (weight > 0 && weight <= 3) {
     totalPriceWeightBased = 11;
   } else if (weight >= 3.1 && weight <= 10) {
     totalPriceWeightBased = 14.50;
   } else if (weight >= 10.1 && weight <= 20) {
     totalPriceWeightBased = 16.80;
   } else if (weight >= 20.1 && weight <= 30) {
     totalPriceWeightBased = 19;
   } else if (weight >= 30.1 && weight <= 50) {
     totalPriceWeightBased = 24;
   } else if (weight >= 50.1) {
     weightDifference = (weight - weightDefaultOver);
     overWeightPrice = (weightDifference * 0.85);
     totalPriceWeightBased = (weightMax + overWeightPrice);
   }
 };

 //SPESE CON CONTRASSEGNO
 if (countersign == true) {
   totalWithCountersign = (countersignDefaultPrice + totalPriceWeightBased + percentageOnGoods);
 }
 //SPESE DI IMBALLO
 if (isItUsed == false) {
   if (packagingType == "SCATOLA CARTONE DA 20X20X15 A 60X40X40") {
     packagingPrice = 9;
   } else if (packagingType == "SCATOLA CARTONE DA 60X50X40 A 80X80X60") {
     packagingPrice = 13;
   } else if (packagingType == "SCATOLA CARTONE PESANTE DA 80X60X60") {
     packagingPrice = 18;
   } else if (packagingType == "SCATOLA CARTONE PESANTE DA 120X80X77") {
     packagingPrice = 25;
   } else if (packagingType == "BANCALE 120X80") {
     packagingPrice = 25;
   } else if (packagingType == "BANCALE 80X60") {
     packagingPrice = 18;
   }
 } else if (isItUsed == true) {
   if (packagingType == "SCATOLA CARTONE DA 20X20X15 A 60X40X40") {
     packagingPrice = 4;
   } else if (packagingType == "SCATOLA CARTONE DA 60X50X40 A 80X80X60") {
     packagingPrice = 6;
   } else if (packagingType == "BANCALE 120X80") {
     packagingPrice = 15;
   } else if (packagingType == "BANCALE 80X60") {
     packagingPrice = 12;
   }
 };

 if (countersign == true) {
   grandTotal = totalWithCountersign + packagingPrice;
 } else if (countersign == false) {
   grandTotal = totalPriceWeightBased + packagingPrice;
 }

  document.getElementById('resultWeight').innerHTML = "Il totale della spedizione è di " + Math.round(grandTotal) + " €";
  document.getElementById('details').innerHTML = "Dimensioni pacco: " + width + " x" + depth + " x" + height + " cm <br> Peso: " + weight + " kg <br> Il pacco è per la Sardegna, Calabria o Sicilia: " + isInItaly + "<br> Contrassegno: " + countersign + "<br> Valore della merce: " + goodsValue + " €<br> Tipo di imballo: " + packagingType + "<br> Imballo usato: " + isItUsed;

  //GENERATORE DI PDF
  var doc = jsPDF("p","mm","a4")

  var lMargin=15; //left margin in mm
  var rMargin=15; //right margin in mm
  var pdfInMM=210;  // width of A4 in mm

  var paragraph = "Il totale della spedizione è di " + Math.round(grandTotal) + " € - Dimensioni pacco: " + width + " x" + depth + " x" + height + " cm - Peso: " + weight + " kg - Il pacco è per la Sardegna, Calabria o Sicilia: " + isInItaly + " - Contrassegno: " + countersign + "- Valore della merce: " + goodsValue + " € - Tipo di imballo: " + packagingType + " - Imballo usato: " + isItUsed;
  var lines =doc.splitTextToSize(paragraph, (pdfInMM-lMargin-rMargin));

  doc.text(lMargin,20,lines);

    $('#savePdf').click(function(){
      doc.save('Generated.pdf');
    });
};

//UNCHEKER
$('input.uncheker').on('change', function() {
    $('input.uncheker').not(this).prop('checked', false);
});
$('input.uncheker1').on('change', function() {
    $('input.uncheker1').not(this).prop('checked', false);
});
$('input.uncheker2').on('change', function() {
    $('input.uncheker2').not(this).prop('checked', false);
});

function formShippingSdaEurope() {

  var destination = document.getElementById('destination').value;
  var width = document.getElementById('packWidthEu').value;
  var depth = document.getElementById('packDeepEu').value;
  var height = document.getElementById('packHeightEu').value;
  var weight = document.getElementById('weightEu').value;
  var packagingType = document.getElementById('sel1Eu').value;
  var isItUsed = document.getElementById('isUsedEu').checked;
  var notes = document.getElementById('notes').value;

  var weightDefaultOver = 31.5;
  var weightDifference = 0;
  var overWeightPrice = 0;
  var totalPriceWeightBased = 0;
  var weightMax = 0;
  var packagingPrice = 0;
  var i;
  var arrayDimensions = [width, depth, height];
  var extraDimensionPrice = 0;

  var volumetricFirst = (width * depth * height);
  var volumetricSecond = 5000;
  var volumetricWeight = (volumetricFirst / volumetricSecond);
  var grandTotal = 0;

  //PESO VOULMETRICO
  if (volumetricWeight > weight) {
    weight = volumetricWeight;
    alert("Il prezzo della spedizione verrà calcolato in base al peso volumetrico.");
  };

  //CONTROLLO DIMENSIONI PACCO

  //Controllo del lato più lungo : corrisponde alla lunghezza
  var packLenght = Math.max(width, depth, height);

  //Rilevazione altezza e larghezza
  for (i = 0; i < arrayDimensions.length; i++) {
    if (arrayDimensions[i] == packLenght) {
      arrayDimensions.splice(i, 1);
    };
  };

  //Calcolo perimetro
  var dimensionOne = (arrayDimensions[0] * 2);
  var dimensionTwo = (arrayDimensions[1] * 2);
  var perimeter = (dimensionOne + dimensionTwo);

  //Calcolo dimensioni complessive del collo
  var totalDimension = (perimeter + packLenght);

  //Aggiunta della tariffa extra
  if (totalDimension > 330 && totalDimension < 449 || packLenght > 200 && packLenght < 279) {
    alert("Attenzione il pacco supera le dimensioni massime per collo consentite dallo spedizioniere. Verrà applicata una tariffa aggiuntiva");
    if (destination == "GERMANIA" || destination == "AUSTRIA" || destination == "OLANDA" || destination == "BELGIO" || destination == "LUSSEMBURGO" || destination == "BOSNIA AND HERZ." || destination == "UNGHERIA" || destination == "MONTENEGRO" || destination == "SERBIA" || destination == "SLOVACCHIA" || destination == "SLOVENIA" || destination == "GRAN BRETAGNA - ISOLE CHANNEL" || destination == "SPAGNA" || destination == "FRANCIA (CON P. DI MONACO)" || destination == "PORTOGALLO - ISOLE BALEARI" || destination == "SVIZZERA - LIECHTENSTEIN" || destination == "POLONIA" || destination == "REP. CECA") {
      extraDimensionPrice = 8;
    } else if (destination == "IRLANDA REP.") {
      extraDimensionPrice = 86.75;
    } else if (destination == "IRLANDA DEL NORD") {
      extraDimensionPrice = 86.75;
    } else if (destination == "DANIMARCA") {
      extraDimensionPrice = 85.70;
    } else if (destination == "ESTONIA") {
      extraDimensionPrice = 300.45;
    } else if (destination == "LETTONIA - LITUANIA") {
      extraDimensionPrice = 395.70;
    } else if (destination == "SVEZIA") {
      extraDimensionPrice = 121.75;
    } else if (destination == "FINLANDIA") {
      extraDimensionPrice = 189.50;
    } else if (destination == "NORVEGIA") {
      extraDimensionPrice = 189.55;
    } else if (destination == "CORSICA") {
      extraDimensionPrice = 177.40;
    } else if (destination == "AZZORRE - MADEIRA") {
      extraDimensionPrice = 122.95;
    } else if (destination == "ANDORRA - ISOLE CANARIE - CEUTA - MELILLA") {
      extraDimensionPrice = 122.95;
    }
  } else if (totalDimension > 450 || packLenght > 280) {
    alert("Attenzione il pacco supera le dimensioni massime per collo consentite dallo spedizioniere. Verrà applicata una tariffa aggiuntiva");
    if (destination == "GERMANIA") {
      extraDimensionPrice = 21.80;
    } else if (destination == "AUSTRIA") {
      extraDimensionPrice = 22.85;
    } else if (destination == "OLANDA") {
      extraDimensionPrice = 22.85;
    } else if (destination == "BELGIO") {
      extraDimensionPrice = 24.20;
    } else if (destination == "LUSSEMBURGO") {
      extraDimensionPrice = 24.60;
    } else if (destination == "BOSNIA AND HERZ.") {
      extraDimensionPrice = 27.35;
    } else if (destination == "UNGHERIA") {
      extraDimensionPrice = 27.35;
    } else if (destination == "MONTENEGRO") {
      extraDimensionPrice = 27.35;
    } else if (destination == "SERBIA") {
      extraDimensionPrice = 27.35;
    } else if (destination == "SLOVACCHIA") {
      extraDimensionPrice = 27.35;
    } else if (destination == "SLOVENIA") {
      extraDimensionPrice = 21.80;
    } else if (destination == "GRAN BRETAGNA - ISOLE CHANNEL") {
      extraDimensionPrice = 39.35;
    } else if (destination == "SPAGNA") {
      extraDimensionPrice = 40.30;
    } else if (destination == "FRANCIA (CON P. DI MONACO)") {
      extraDimensionPrice = 40.30;
    } else if (destination == "PORTOGALLO - ISOLE BALEARI") {
      extraDimensionPrice = 63.95;
    } else if (destination == "SVIZZERA - LIECHTENSTEIN") {
      extraDimensionPrice = 21.80;
    } else if (destination == "POLONIA") {
      extraDimensionPrice = 21.80;
    } else if (destination == "REP. CECA") {
      extraDimensionPrice = 21.80;
    } else if (destination == "IRLANDA REP.") {
      extraDimensionPrice = 86.75;
    } else if (destination == "IRLANDA DEL NORD") {
      extraDimensionPrice = 86.75;
    } else if (destination == "DANIMARCA") {
      extraDimensionPrice = 85.70;
    } else if (destination == "ESTONIA") {
      extraDimensionPrice = 300.45;
    } else if (destination == "LETTONIA - LITUANIA") {
      extraDimensionPrice = 395.70;
    } else if (destination == "SVEZIA") {
      extraDimensionPrice = 121.75;
    } else if (destination == "FINLANDIA") {
      extraDimensionPrice = 189.50;
    } else if (destination == "NORVEGIA") {
      extraDimensionPrice = 189.55;
    } else if (destination == "CORSICA") {
      extraDimensionPrice = 177.40;
    } else if (destination == "AZZORRE - MADEIRA") {
      extraDimensionPrice = 122.95;
    } else if (destination == "ANDORRA - ISOLE CANARIE - CEUTA - MELILLA") {
      extraDimensionPrice = 122.95;
    }
  };

  //SPESE DI IMBALLO
  if (isItUsed == false) {
    if (packagingType == "SCATOLA CARTONE DA 20X20X15 A 60X40X40") {
      packagingPrice = 9;
    } else if (packagingType == "SCATOLA CARTONE DA 60X50X40 A 80X80X60") {
      packagingPrice = 13;
    } else if (packagingType == "SCATOLA CARTONE PESANTE DA 80X60X60") {
      packagingPrice = 18;
    } else if (packagingType == "SCATOLA CARTONE PESANTE DA 120X80X77") {
      packagingPrice = 25;
    } else if (packagingType == "BANCALE 120X80") {
      packagingPrice = 25;
    } else if (packagingType == "BANCALE 80X60") {
      packagingPrice = 18;
    }
  } else if (isItUsed == true) {
    if (packagingType == "SCATOLA CARTONE DA 20X20X15 A 60X40X40") {
      packagingPrice = 4;
    } else if (packagingType == "SCATOLA CARTONE DA 60X50X40 A 80X80X60") {
      packagingPrice = 6;
    } else if (packagingType == "BANCALE 120X80") {
      packagingPrice = 15;
    } else if (packagingType == "BANCALE 80X60") {
      packagingPrice = 12;
    }
  };

  //GERMANIA
  if (destination == "GERMANIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 11.10;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 13.60;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 17.95;
    } else if (weight >= 31.6) {
      weightMax = 17.95;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.39);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //AUSTRIA
  if (destination == "AUSTRIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 11.10;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 14.90;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 17.90;
    } else if (weight >= 31.6) {
      weightMax = 17.90;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.43);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //OLANDA
  if (destination == "OLANDA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 12.20;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 17.45;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 21.10;
    } else if (weight >= 31.6) {
      weightMax = 21.10;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.48);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //BELGIO
  if (destination == "BELGIO") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 11.10;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 16.65;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 20;
    } else if (weight >= 31.6) {
      weightMax = 20;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.48);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //LUSSEMBURGO
  if (destination == "LUSSEMBURGO") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 11.10;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 16.65;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 20;
    } else if (weight >= 31.6) {
      weightMax = 20;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.48);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //BOSNIA AND HERZ.
  if (destination == "BOSNIA AND HERZ.") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 15.05;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 20.70;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 28.15;
    } else if (weight >= 31.6) {
      weightMax = 28.15;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.48);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //UNGHERIA
  if (destination == "UNGHERIA") {
    console.log("OK");
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 13.55;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 20.70;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 25.10;
    } else if (weight >= 31.6) {
      weightMax = 25.10;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.48);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //MONTENEGRO
  if (destination == "MONTENEGRO") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 15.35;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 21.50;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 29.75;
    } else if (weight >= 31.6) {
      weightMax = 29.75;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.52);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //SERBIA
  if (destination == "SERBIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 15.05;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 20.70;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 28.15;
    } else if (weight >= 31.6) {
      weightMax = 28.15;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.48);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //SLOVACCHIA
  if (destination == "SLOVACCHIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 15.05;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 20.05;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 23.20;
    } else if (weight >= 31.6) {
      weightMax = 23.20;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.48);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //SLOVENIA
  if (destination == "SLOVENIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 14.65;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 20.60;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 27.20;
    } else if (weight >= 31.6) {
      weightMax = 27.20;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.53);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //GRAN BRETAGNA - ISOLE CHANNEL
  if (destination == "GRAN BRETAGNA - ISOLE CHANNEL") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 12.20;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 17.80;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 21.10;
    } else if (weight >= 31.6) {
      weightMax = 21.10;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.53);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //SPAGNA
  if (destination == "SPAGNA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 12.20;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 17.80;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 21.10;
    } else if (weight >= 31.6) {
      weightMax = 21.10;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.43);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //FRANCIA (CON P. DI MONACO)
  if (destination == "FRANCIA (CON P. DI MONACO)") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 12.25;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 16.15;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 20.65;
    } else if (weight >= 31.6) {
      weightMax = 20.65;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.39);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //PORTOGALLO - ISOLE BALEARI
  if (destination == "PORTOGALLO - ISOLE BALEARI") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 20;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 25.75;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 30.35;
    } else if (weight >= 31.6) {
      weightMax = 30.35;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.54);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //IRLANDA. REP.
  if (destination == "IRLANDA. REP.") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 34.10;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 38.45;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 44.55;
    } else if (weight >= 31.6) {
      weightMax = 44.55;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.56);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //IRLANDA DEL NORD
  if (destination == "IRLANDA DEL NORD") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 34.10;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 38.45;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 44.55;
    } else if (weight >= 31.6) {
      weightMax = 44.55;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.70);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //SVIZZERA - LIECHTENSTEIN
  if (destination == "SVIZZERA - LIECHTENSTEIN") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 16.30;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 24.15;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 53.15;
    } else if (weight >= 31.6) {
      weightMax = 53.15;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 1.32);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //POLONIA
  if (destination == "POLONIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 14.70;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 20.65;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 30.25;
    } else if (weight >= 31.6) {
      weightMax = 30.25;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.64);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //DANIMARCA
  if (destination == "DANIMARCA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 19.45;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 23.20;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 28.60;
    } else if (weight >= 31.6) {
      weightMax = 28.60;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.38);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //ESTONIA
  if (destination == "ESTONIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 30.10;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 35;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 41.35;
    } else if (weight >= 31.6) {
      weightMax = 41.35;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.38);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //LETTONIA - LITUANIA
  if (destination == "LETTONIA - LITUANIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 30.10;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 35;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 41.35;
    } else if (weight >= 31.6) {
      weightMax = 41.35;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.38);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //SVEZIA
  if (destination == "SVEZIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 21.05;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 25;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 29.45;
    } else if (weight >= 31.6) {
      weightMax = 29.45;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.38);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //FINLANDIA
  if (destination == "FINLANDIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 22.80;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 28.55;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 33;
    } else if (weight >= 31.6) {
      weightMax = 33;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.38);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //NORVEGIA
  if (destination == "NORVEGIA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 44.95;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 49.85;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 56.05;
    } else if (weight >= 31.6) {
      weightMax = 56.05;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.38);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //REP. CECA
  if (destination == "REP. CECA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 12.30;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 20;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 23.55;
    } else if (weight >= 31.6) {
      weightMax = 23.55;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 0.59);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //CORSICA
  if (destination == "CORSICA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 53.35;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 57.10;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 61.75;
    } else if (weight >= 31.6) {
      weightMax = 61.75;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 1.03);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //AZZORRE - MADEIRA
  if (destination == "AZZORRE - MADEIRA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 44.45;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 51.70;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 56.35;
    } else if (weight >= 31.6) {
      weightMax = 56.35;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 2.46);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };

  //ANDORRA - ISOLE CANARIE - CEUTA - MELILLA
  if (destination == "ANDORRA - ISOLE CANARIE - CEUTA - MELILLA") {
    if (weight > 0 && weight <= 5) {
      totalPriceWeightBased = 72.45;
    } else if (weight >= 5.1 && weight <= 15) {
      totalPriceWeightBased = 77.45;
    } else if (weight >= 15.1 && weight <= 31.5) {
      totalPriceWeightBased = 82.65;
    } else if (weight >= 31.6) {
      weightMax = 82.65;
      weightDifference = (weight - weightDefaultOver);
      overWeightPrice = (weightDifference * 2.46);
      totalPriceWeightBased = (weightMax + overWeightPrice);
    }
  };


 var totalToUs = (totalPriceWeightBased + packagingPrice + extraDimensionPrice);
 var precentageToAdd = (totalToUs * 0.30);
 var totalToCostumer = (totalToUs + precentageToAdd);

document.getElementById('resultWeight').innerHTML = "Il totale della spedizione è di " + Math.round(totalToCostumer) + " €";
document.getElementById('details').innerHTML = "Dimensioni pacco: " + width + " x" + depth + " x" + height + " cm <br> Peso: " + weight + " kg <br> Il lato più lungo del pacco è: " + packLenght + " cm<br> Perimetro del pacco: " + perimeter + " cm<br> Destinazione: " + destination + "<br> Tipo di imballo: " + packagingType + "<br> Prezzo della spedizione: " + totalPriceWeightBased + " €<br> Prezzo dell'imballaggio: " + packagingPrice + " €<br> Sovraprezzo dato dalle dimensioni: " + extraDimensionPrice + "€<br> Note: "+ notes;

//GENERATORE DI PDF
var doc = jsPDF("p","mm","a4")

var lMargin=15; //left margin in mm
var rMargin=15; //right margin in mm
var pdfInMM=210;  // width of A4 in mm

var paragraph = "Il totale della spedizione è di " + Math.round(totalToCostumer) + "- € Dimensioni pacco: " + width + " x" + depth + " x" + height + " cm Peso: " + weight + " kg - Il lato più lungo del pacco è: " + packLenght + " cm - Perimetro del pacco: " + perimeter + " cm - Destinazione: " + destination + "- Tipo di imballo: " + packagingType + "- Prezzo della spedizione: " + totalPriceWeightBased + " € - Prezzo dell'imballaggio: " + packagingPrice + " € - Sovraprezzo dato dalle dimensioni: " + extraDimensionPrice + "€ - Note: " + notes;
var lines =doc.splitTextToSize(paragraph, (pdfInMM-lMargin-rMargin));

doc.text(lMargin,20,lines);

  $('#savePdf').click(function(){
    doc.save('Generated.pdf');
  });
};

if (window.location.pathname == '/shipping-cost.php') {
const countries = ["Austria", "Belgio", "Bosnia", "Bulgaria", "Croazia", "Danimarca", "Estonia", "Francia", "Germania", "Gran Bretagna", "Grecia", "Irlanda", "Lettonia", "Liechtenstein", "Lituania", "Lussemburgo", "Macedonia", "Norvegia", "Olanda", "Polonia", "Portogallo", "Repubblica Ceca", "Repubblica Serbia", "Romania", "Slovacchia", "Slovenia", "Spagna", "Svezia", "Svizzera", "Ungheria"];

  for (i=0; i < countries.length; i++) {
    $("#fercam-destination").append("<option>" + countries[i] + "</option>");
  };
};



function removeCapDiv() {
  var countrySelected = document.getElementById("fercam-destination").value;

  if (countrySelected == 'Irlanda' || countrySelected == 'Liechtenstein' || countrySelected == 'Lussemburgo' || countrySelected == 'Slovenia') {
    $(".cap-div").toggleClass("display");
  } else {
    $(".cap-div").removeClass("display");
  }
};

function formShippingFercamInternational() {
  // INFORMAZIONI VARIE DI SPEDIZIONE
  var countrySelected = document.getElementById("fercam-destination").value;
  var zipCodeRaw = document.getElementById("zipCode").value;
  var zipCodeSubString = zipCodeRaw.substring(0, 2);
  var zipCodeSubStringGreece = zipCodeRaw.substring(0, 3);
  var zipCodeString = zipCodeSubString.toUpperCase();
  var zipCodeNumber = parseInt(zipCodeSubString);
  var zipCodeNumberGreece = parseInt(zipCodeSubStringGreece);
  var weightRaw = document.getElementById("weightFe").value;
  var weight = parseInt(weightRaw);
  var goodsValue = document.getElementById("goods-value").value;

  var demCheckBoxValue = document.getElementById("dem").checked;
  var borderOperationCheckBoxValue = document.getElementById("border-operation").checked;
  var ex1CheckBoxValue = document.getElementById("ex1").checked;
  var hydraulicCheckBoxValue = document.getElementById("hydraulic").checked;
  var insuranceCheckBox = document.getElementById("insurance").checked;

  var shippingTime = 0;

  var pricePerQuintal;
  var packageQuintals;
  var totalPerQuintals;
  var ourCharge;
  var total;

  var demCost = 0;
  var borderOperationCost = 0;
  var ex1Cost = 0;
  var hydraulicCost = 0;
  var insuranceCost = 0;

  //DIMENSIONI PACCO
  var packageX = document.getElementById("packWidthFe").value;
  var packageY = document.getElementById("packHeightFe").value;
  var packageZ = document.getElementById("packDeepFe").value;

  //CALCOLO PESO VOLUMETRICO
  var volumetricFirst = (packageX * packageZ * packageY);
  var volumetricSecond = 5000;
  var volumetricWeight = (volumetricFirst / volumetricSecond);

  if (volumetricWeight > weight) {
    weight = volumetricWeight;
    alert("Il prezzo della spedizione verrà calcolato in base al peso volumetrico.");
  };

  if (weight > 1500) {
    alert("Il peso supera il limite consentito da FERCAM per singolo collo");
  };

  if (packageX >= 180 || packageY >= 220 || packageZ >= 240) {
    alert("Il collo supera le dimensioni consentite da FERCAM");
  };

  if (demCheckBoxValue == true) {
    demCost = 15;
  };

  if (borderOperationCheckBoxValue == true) {
    if (goodsValue <= 25000) {
      borderOperationCost = 55;
    } else if (goodsValue > 25000 && goodsValue <= 50000) {
      borderOperationCost = 70;
    } else if (goodsValue > 50000) {
      borderOperationCost = 100;
    };
  };

  if (insuranceCheckBox == true) {
    var goodsValueOne = (goodsValue * 110);
    var goodsValueTwo = (goodsValueOne / 100);
    var insurancePercentage = (goodsValueTwo * 0.005);

    if (insurancePercentage < 25) {
      insuranceCost == 25;
    } else {
      insuranceCost == insurancePercentage;
    };
  };

  if (ex1CheckBoxValue == true) {
    ex1Cost = 10;
  };

  if (hydraulicCheckBoxValue == true) {
    hydraulicCost = 30;
  };

  //AUSTRIA
  if (countrySelected == "Austria") {
    shippingTime = 72;
    if ((zipCodeNumber >= 00 && zipCodeNumber <= 19) || (zipCodeNumber >= 23 && zipCodeNumber <= 25) || (zipCodeNumber >= 50 && zipCodeNumber <= 61) || (zipCodeNumber >= 66 && zipCodeNumber <= 70)) {
      if (weight <= 100) {
        pricePerQuintal = 30.65;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 29.05;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 22.80;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 20.95;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 20.05;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 19.80;
      } else if (weight > 2000) {
        pricePerQuintal = 19.65;
      };
    } else if ((zipCodeNumber >= 20 && zipCodeNumber <= 22) || (zipCodeNumber >= 26 && zipCodeNumber <= 31) || zipCodeNumber == 34 || zipCodeNumber == 35 || zipCodeNumber == 37 || zipCodeNumber == 48 || (zipCodeNumber >= 62 && zipCodeNumber <= 65) || (zipCodeNumber >= 71 && zipCodeNumber <= 73)) {
      if (weight <= 100) {
        pricePerQuintal = 33.45;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 31.55;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 25.15;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 23.10;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 22.05;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 21.85;
      } else if (weight > 2000) {
        pricePerQuintal = 21.65;
      };
    } else if ((zipCodeNumber >= 32 && zipCodeNumber <= 33) || zipCode == 36 || (zipCodeNumber >= 39 && zipCodeNumber <= 47) || zipCode == 49 || (zipCodeNumber >= 74 && zipCodeNumber <= 85) || zipCode == 87 || (zipCodeNumber >= 89 && zipCodeNumber <= 93) || zipCodeNumber == 95) {
      if (weight <= 100) {
        pricePerQuintal = 40.80;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 38.30;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 29.90;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.55;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 26.35;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 26.10;
      } else if (weight > 2000) {
        pricePerQuintal = 25.90;
      };
    } else if (zipCodeNumber == 38 || zipCodeNumber == 86 || zipCodeNumber == 88 || zipCodeNumber == 94 || (zipCodeNumber >= 96 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 44.45;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 41.45;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 32.70;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 29.95;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 28.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 28.35;
      } else if (weight > 2000) {
        pricePerQuintal = 28.15;
      };
    };
  // BELGIO
  } else if (countrySelected == "Belgio") {
    shippingTime = 72;
    if ((zipCodeNumber >= 00 && zipCodeNumber <= 12) || (zipCodeNumber >= 18 && zipCodeNumber <= 33) || (zipCodeNumber >= 35 && zipCodeNumber <= 41) || (zipCodeNumber >= 43 && zipCodeNumber <= 49) || zipCodeNumber == 52 || (zipCodeNumber >= 66 && zipCodeNumber <= 69) || zipCodeNumber == 77 || zipCodeNumber == 85 || (zipCodeNumber >= 91 && zipCodeNumber <= 93)) {
      if (weight <= 100) {
        pricePerQuintal = 34.45;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 32.25;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 24.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 22.25;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 21.15;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 20.90;
      } else if (weight > 2000) {
        pricePerQuintal = 20.70;
      };
    } else if ((zipCodeNumber >= 13 && zipCodeNumber <= 17) || zipCodeNumber == 34 || zipCodeNumber == 42 || zipCodeNumber == 53 || (zipCodeNumber >= 60 && zipCodeNumber <= 62) || zipCodeNumber == 65  || (zipCodeNumber >= 70 && zipCodeNumber <= 71) || zipCodeNumber == 73 || (zipCodeNumber >= 75 && zipCodeNumber <= 76) || (zipCodeNumber >= 78 && zipCodeNumber <= 84) || (zipCodeNumber >= 86 && zipCodeNumber <= 90) || (zipCodeNumber >= 94 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 37.20;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 34.80;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 26.70;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 24.40;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 23.25;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 23.00;
      } else if (weight > 2000) {
        pricePerQuintal = 22.80;
      };
    } else if ((zipCodeNumber >= 50 && zipCodeNumber <= 51) || (zipCodeNumber >= 55 && zipCodeNumber <= 59) || (zipCodeNumber >= 63 && zipCodeNumber <= 64)) {
      if (weight <= 100) {
        pricePerQuintal = 42;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 38.65;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 30.05;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 25.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 25.35;
      } else if (weight > 2000) {
        pricePerQuintal = 25.15;
      };
    };
  } else if (countrySelected == "Bosnia") {
    if ((zipCodeNumber >= 70 && zipCodeNumber <= 80) || (zipCodeNumber >= 88 && zipCodeNumber <= 89)) {
      if (weight <= 100) {
        pricePerQuintal = 58.90;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 54.95;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 44.35;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 41.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 39.35;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 39.05;
      } else if (weight > 2000) {
        pricePerQuintal = 38.80;
      };
    };
  } else if (countrySelected == "Bulgaria") {
    shippingTime = 144;
    if ((zipCodeNumber >= 00 && zipCodeNumber <= 19) || (zipCodeNumber >= 22 && zipCodeNumber <= 27) || (zipCodeNumber >= 30 && zipCodeNumber <= 32) || (zipCodeNumber >= 34 && zipCodeNumber <= 35)) {
      if (weight <= 100) {
        pricePerQuintal = 31.10;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 30.85;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 27.00;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 25.85;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 25.25;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 25.15;
      } else if (weight > 2000) {
        pricePerQuintal = 25.00;
      };
    } else if (zipCodeNumber == 20 || (zipCodeNumber >= 28 && zipCodeNumber <= 29) || zipCodeNumber == 36 || (zipCodeNumber >= 39 && zipCodeNumber <= 40)) {
      if (weight <= 100) {
        pricePerQuintal = 35.20;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 34.25;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 29.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.90;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 27.10;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 26.95;
      } else if (weight > 2000) {
        pricePerQuintal = 26.85;
      };
    } else if (zipCodeNumber == 21 || zipCodeNumber == 33 || (zipCodeNumber >= 37 && zipCodeNumber <= 38) || (zipCodeNumber >= 41 && zipCodeNumber <= 51) || zipCode == 53 || (zipCodeNumber >= 60 && zipCodeNumber <= 61)) {
      if (weight <= 100) {
        pricePerQuintal = 38.70;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 37.35;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 31.15;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 29.40;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 28.50;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 28.30;
      } else if (weight > 2000) {
        pricePerQuintal = 28.15;
      };
    } else if (zipCodeNumber == 52 || zipCodeNumber == 54 || zipCodeNumber == 98 || (zipCodeNumber >= 62 && zipCodeNumber <= 69) || (zipCodeNumber >= 77 && zipCodeNumber <= 92)) {
      if (weight <= 100) {
        pricePerQuintal = 41.35;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 39.90;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 33.30;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 31.50;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 30.55;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 30.35;
      } else if (weight > 2000) {
        pricePerQuintal = 30.20;
      };
    } else if (zipCodeNumber == 99 || (zipCodeNumber >= 93 && zipCodeNumber <= 97) || (zipCodeNumber >= 70 && zipCodeNumber <= 76) || (zipCodeNumber >= 55 && zipCodeNumber <= 59)) {
      if (weight <= 100) {
        pricePerQuintal = 44.70;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 42.70;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 34.70;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 32.60;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 31.45;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 31.25;
      } else if (weight > 2000) {
        pricePerQuintal = 31.05;
      }
    };
  } else if (countrySelected == "Croazia") {
    shippingTime = 120;
    if ((zipCodeNumber >= 00 && zipCodeNumber <= 40) || (zipCodeNumber >= 42 && zipCodeNumber <= 44) || (zipCodeNumber >= 47 && zipCodeNumber <= 59)) {
      if (weight <= 100) {
        pricePerQuintal = 56.20;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 52.55;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 41.25;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 38.25;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 36.70;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 36.40;
      } else if (weight > 2000) {
        pricePerQuintal = 36.15;
      };
    };
  } else if (countrySelected == "Danimarca") {
    shippingTime = 120;
    if ((zipCodeNumber >= 00 && zipCodeNumber <= 36) || (zipCodeNumber >= 38 && zipCodeNumber <= 44) || (zipCodeNumber >= 46 && zipCodeNumber <= 48) || (zipCodeNumber >= 50 && zipCodeNumber <= 58) || (zipCodeNumber >= 60 && zipCodeNumber <= 66) || (zipCodeNumber >= 68 && zipCodeNumber <= 82) || (zipCodeNumber >= 84 && zipCodeNumber <= 97)) {
      if (weight <= 100) {
        pricePerQuintal = 59.90;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 55.05;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 41.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 37.80;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 35.90;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 35.50;
      } else if (weight > 2000) {
        pricePerQuintal = 35.20;
      };
    } else if ((zipCodeNumber >= 98 && zipCodeNumber <= 99) || zipCodeNumber == 45 || zipCodeNumber == 49 || zipCodeNumber == 59 || zipCodeNumber == 67 || zipCodeNumber == 83) {
      if (weight <= 100) {
        pricePerQuintal = 76.20;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 69.00;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 55.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 49.80;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 46.00;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 43.60;
      } else if (weight > 2000) {
        pricePerQuintal = 42.75;
      };
    } else if (zipCodeNumber == 45) {
      if (weight <= 100) {
        pricePerQuintal = 105.45;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 96.85;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 79.20;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 66.00;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 61.10;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 56.15;
      } else if (weight > 2000) {
        pricePerQuintal = 64.70;
      };
    };
  } else if (countrySelected == "Estonia") {
    shippingTime = 120;
    if ((zipCodeNumber >= 00 && zipCodeNumber <= 61) || (zipCodeNumber >= 63 && zipCodeNumber <= 90)) {
      if (weight <= 100) {
        pricePerQuintal = 51.15;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 49.30;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 42.45;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 40.30;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 39.20;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 39.00;
      } else if (weight > 2000) {
        pricePerQuintal = 38.85;
      };
    } else if ((zipCodeNumber >= 91 && zipCodeNumber <= 99) || zipCodeNumber == 62) {
      if (weight <= 100) {
        pricePerQuintal = 76.10;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 70.70;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 62.95;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 58.20;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 55.15;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 51.45;
      } else if (weight > 2000) {
        pricePerQuintal = 50.40;
      };
    };
  } else if (countrySelected == "Francia") {
    shippingTime = 72;
    if (zipCodeNumber == 01 || zipCodeNumber == 57) {
      if (weight <= 100) {
        pricePerQuintal = 33.70;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 31.15;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 24.00;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 21.50;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 20.35;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 20.10;
      } else if (weight > 2000) {
        pricePerQuintal = 19.90;
      };
    } else if (zipCodeNumber == 10 || zipCodeNumber == 13 || zipCodeNumber == 21 || zipCodeNumber == 25 || zipCodeNumber == 38 || zipCodeNumber == 69 || zipCodeNumber == 74) {
      if (weight <= 100) {
        pricePerQuintal = 38.75;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 35.70;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 26.55;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 23.90;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 22.55;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 22.25;
      } else if (weight > 2000) {
        pricePerQuintal = 22.05;
      };
    } else if (zipCodeNumber == 02 || zipCodeNumber == 15 || zipCodeNumber == 18 || zipCodeNumber == 26 || zipCodeNumber == 30 || zipCodeNumber == 49 || zipCodeNumber == 59 || zipCodeNumber == 68 || zipCodeNumber == 71 || zipCodeNumber == 84 || zipCodeNumber == 88 || (zipCodeNumber >= 07 && zipCodeNumber <= 08) ||(zipCodeNumber >= 75 && zipCodeNumber <= 79) ||(zipCodeNumber >= 90 && zipCodeNumber <= 95)) {
      if (weight <= 100) {
        pricePerQuintal = 41.15;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 37.90;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 27.95;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 25.15;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 23.75;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 23.45;
      } else if (weight > 2000) {
        pricePerQuintal = 23.20;
      }
    } else if (zipCodeNumber == 24 || zipCodeNumber == 39 || zipCodeNumber == 51 || zipCodeNumber == 60 || zipCodeNumber == 63 || zipCodeNumber == 67 || zipCodeNumber == 70 || zipCodeNumber == 72 || zipCodeNumber == 80 || zipCodeNumber == 83 || zipCodeNumber == 85 || zipCodeNumber == 89 || (zipCodeNumber >= 27 && zipCodeNumber <= 28) || (zipCodeNumber >= 34 && zipCodeNumber <= 35) || (zipCodeNumber >= 42 && zipCodeNumber <= 44) || (zipCodeNumber >= 54 && zipCodeNumber <= 55) || (zipCodeNumber >= 16 && zipCodeNumber <= 17)) {
      if (weight <= 100) {
        pricePerQuintal = 44.55;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 40.85;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 30.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.30;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 25.80;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 25.45;
      } else if (weight > 2000) {
        pricePerQuintal = 25.20;
      }
    } else if (zipCodeNumber == 03 || zipCodeNumber == 14 || zipCodeNumber == 19 || zipCodeNumber == 22 || zipCodeNumber == 33 || zipCodeNumber == 45 || zipCodeNumber == 58 || zipCodeNumber == 62 || zipCodeNumber == 73 || zipCodeNumber == 86 || zipCodeNumber == 45 || (zipCodeNumber >= 36 && zipCodeNumber <= 37)) {
      if (weight <= 100) {
        pricePerQuintal = 46.55;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 42.80;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 32.25;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 29.15;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 27.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 27.30;
      } else if (weight > 2000) {
        pricePerQuintal = 27.05;
      }
    } else if (zipCodeNumber == 23 || zipCodeNumber == 47 || zipCodeNumber == 48 || zipCodeNumber == 52 || zipCodeNumber == 82 || (zipCodeNumber >= 31 && zipCodeNumber <= 32)) {
      if (weight <= 100) {
        pricePerQuintal = 48.95;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 45.05;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 34.35;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 31.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 29.45;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 29.15;
      } else if (weight > 2000) {
        pricePerQuintal = 28.90;
      }
    } else if (zipCodeNumber == 29 || zipCodeNumber == 53 || zipCodeNumber == 56 || zipCodeNumber == 61 || (zipCodeNumber >= 40 && zipCodeNumber <= 41)) {
      if (weight <= 100) {
        pricePerQuintal = 52.20;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 48.00;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 36.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 33.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 31.35;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 30.95;
      } else if (weight > 2000) {
        pricePerQuintal = 30.60;
      }
    } else if (zipCodeNumber == 04 || zipCodeNumber == 64 || zipCodeNumber == 66 || zipCodeNumber == 81 || zipCodeNumber == 87) {
      if (weight <= 100) {
        pricePerQuintal = 53.80;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 49.60;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 38.75;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 35.30;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 33.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 33.30;
      } else if (weight > 2000) {
        pricePerQuintal = 33.00;
      }
    } else if (zipCodeNumber == 50 || (zipCodeNumber >= 05 && zipCodeNumber <= 06)) {
      if (weight <= 100) {
        pricePerQuintal = 57.40;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 52.60;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 41.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 37.60;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 35.75;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 35.45;
      } else if (weight > 2000) {
        pricePerQuintal = 35.10;
      }
    } else if (zipCodeNumber == 09 || zipCodeNumber == 46 || zipCodeNumber == 65) {
      if (weight <= 100) {
        pricePerQuintal = 58.80;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 54.25;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 43.70;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 39.75;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 37.85;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 37.55;
      } else if (weight > 2000) {
        pricePerQuintal = 37.15;
      }
    } else if (zipCodeNumber == 20) {
      if (weight <= 100) {
        pricePerQuintal = 59.05;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 55.05;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 46.75;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 42.70;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 41.05;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 41.25;
      } else if (weight > 2000) {
        pricePerQuintal = 41.15;
      }
    }
  } else if (countrySelected == "Germania") {
    shippingTime = 72;
    if (zipCodeNumber == 66 || zipCodeNumber == 75 || (zipCodeNumber >= 50 && zipCodeNumber <= 51) || (zipCodeNumber >= 70 && zipCodeNumber <= 73) || (zipCodeNumber >= 80 && zipCodeNumber <= 82) || (zipCodeNumber >= 85 && zipCodeNumber <= 89)) {
      if (weight <= 100) {
        pricePerQuintal = 25.70;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 24.40;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 18.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 16.80;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 16.00;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 15.80;
      } else if (weight > 2000) {
        pricePerQuintal = 15.60;
      };
    } else if (zipCodeNumber == 44 || zipCodeNumber == 74 || zipCodeNumber == 83 || zipCodeNumber == 84 || zipCodeNumber == 97 || (zipCodeNumber >= 40 && zipCodeNumber <= 42) || (zipCodeNumber >= 52 && zipCodeNumber <= 55) || (zipCodeNumber >= 60 && zipCodeNumber <= 65) || (zipCodeNumber >= 67 && zipCodeNumber <= 69) || (zipCodeNumber >= 76 && zipCodeNumber <= 79) || (zipCodeNumber >= 90 && zipCodeNumber <= 94)) {
      if (weight <= 100) {
        pricePerQuintal = 30.60;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 28.75;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 21.45;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 19.45;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 18.45;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 18.25;
      } else if (weight > 2000) {
        pricePerQuintal = 18.05;
      };
    } else if (zipCodeNumber == 01 || zipCodeNumber == 04 || zipCodeNumber == 12 || zipCodeNumber == 28 || zipCodeNumber == 30 || zipCodeNumber == 39 || (zipCodeNumber >= 06 && zipCodeNumber <= 10) || (zipCodeNumber >= 32 && zipCodeNumber <= 37) || (zipCodeNumber >= 45 && zipCodeNumber <= 49) || (zipCodeNumber >= 56 && zipCodeNumber <= 59) || (zipCodeNumber >= 95 && zipCodeNumber <= 96) || (zipCodeNumber >= 98 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 33.80;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 31.90;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 24.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 23.35;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 21.30;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 21.10;
      } else if (weight > 2000) {
        pricePerQuintal = 20.90;
      }
    } else if (zipCodeNumber == 29 || zipCodeNumber == 31 || zipCodeNumber == 38 || (zipCodeNumber >= 02 && zipCodeNumber <= 03) || (zipCodeNumber >= 13 && zipCodeNumber <= 17) || (zipCodeNumber >= 19 && zipCodeNumber <= 24)) {
      if (weight <= 100) {
        pricePerQuintal = 36.90;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 34.85;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 27.25;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 25.15;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 24.05;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 23.80;
      } else if (weight > 2000) {
        pricePerQuintal = 23.65;
      }
    } else if ((zipCodeNumber >= 25 && zipCodeNumber <= 27)) {
      if (weight <= 100) {
        pricePerQuintal = 44.70;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 41.90;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 34.30;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 31.45;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 29.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 28.60;
      } else if (weight > 2000) {
        pricePerQuintal = 28.20;
      }
    } else if (zipCodeNumber == 18) {
      if (weight <= 100) {
        pricePerQuintal = 38.95;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 36.85;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 29.20;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 25.95;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 25.70;
      } else if (weight > 2000) {
        pricePerQuintal = 25.55;
      };
    };
  } else if (countrySelected == "Gran Bretagna") {
    shippingTime = 120;
    if (zipCodeString == 'AL' || zipCodeString == 'BB' || zipCodeString == 'B1' || zipCodeString == 'B2' || zipCodeString == 'B3' || zipCodeString == 'B4' || zipCodeString == 'B5' || zipCodeString == 'B6' || zipCodeString == 'B7' || zipCodeString == 'B8' || zipCodeString == 'B9' || zipCodeString == 'B9' || zipCodeString == 'BL' || zipCodeString == 'BN' || zipCodeString == 'BR' || zipCodeString == 'CB' || zipCodeString == 'CM' || zipCodeString == 'CO' || zipCodeString == 'CR' || zipCodeString == 'CT' || zipCodeString == 'CV' || zipCodeString == 'DA' || zipCodeString == 'DE' || zipCodeString == 'DY' || zipCodeString == 'E1' || zipCodeString == 'E2' || zipCodeString == 'E3' || zipCodeString == 'E4' || zipCodeString == 'E5' || zipCodeString == 'E6' || zipCodeString == 'E7' || zipCodeString == 'E8' || zipCodeString == 'E9' || zipCodeString == 'EC' || zipCodeString == 'EN' || zipCodeString == 'GU' || zipCodeString == 'HA' || zipCodeString == 'HD' || zipCodeString == 'HP' || zipCodeString == 'HX' || zipCodeString == 'IG' || zipCodeString == 'KT' || zipCodeString == 'LE' || zipCodeString == 'LU' || zipCodeString == 'M2' || zipCodeString == 'M3' || zipCodeString == 'M4' || zipCodeString == 'M5' || zipCodeString == 'M6' || zipCodeString == 'M7' || zipCodeString == 'M8' || zipCodeString == 'M9' || zipCodeString == 'ME' || zipCodeString == 'MK' || zipCodeString == 'N1' || zipCodeString == 'N2' || zipCodeString == 'N3' || zipCodeString == 'N4' || zipCodeString == 'N5' || zipCodeString == 'N6' || zipCodeString == 'N7' || zipCodeString == 'N8' || zipCodeString == 'N9' || zipCodeString == 'NG' || zipCodeString == 'NN' || zipCodeString == 'NW' || zipCodeString == 'OL' || zipCodeString == 'OX' || zipCodeString == 'PE' || zipCodeString == 'RG' || zipCodeString == 'RG' || zipCodeString == 'RH' || zipCodeString == 'RM' || zipCodeString == 'SE' || zipCodeString == 'SG' || zipCodeString == 'SL' || zipCodeString == 'SM' || zipCodeString == 'SS' || zipCodeString == 'SW' || zipCodeString == 'TN' || zipCodeString == 'TW' || zipCodeString == 'UB' || zipCodeString == 'W1' || zipCodeString == 'W2' || zipCodeString == 'W3' || zipCodeString == 'W4' || zipCodeString == 'W5' || zipCodeString == 'W6' || zipCodeString == 'W7' || zipCodeString == 'W8' || zipCodeString == 'W9' || zipCodeString == 'WC' || zipCodeString == 'WD' || zipCodeString == 'WS' || zipCodeString == 'WV') {
      if (weight <= 100) {
        pricePerQuintal = 47.35;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 44.10;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 35.80;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 33.10;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 31.80;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 31.50;
      } else if (weight > 2000) {
        pricePerQuintal = 31.25;
      };
    } else if (zipCodeString == 'BD' || zipCodeString == 'CH' || zipCodeString == 'CW' || zipCodeString == 'DN' || zipCodeString == 'FY' || zipCodeString == 'GL' || zipCodeString == 'HG' || zipCodeString == 'HR' || zipCodeString == 'IP' || zipCodeString == 'L1' || zipCodeString == 'L2' || zipCodeString == 'L3' || zipCodeString == 'L4' || zipCodeString == 'L5' || zipCodeString == 'L6' || zipCodeString == 'L7' || zipCodeString == 'L8' || zipCodeString == 'L9' || zipCodeString == 'LA' || zipCodeString == 'LN' || zipCodeString == 'LS' || zipCodeString == 'M1' || zipCodeString == 'PO' || zipCodeString == 'PR' || zipCodeString == 'S1' || zipCodeString == 'S2' || zipCodeString == 'S3' || zipCodeString == 'S4' || zipCodeString == 'S5' || zipCodeString == 'S6' || zipCodeString == 'S7' || zipCodeString == 'S8' || zipCodeString == 'S9' || zipCodeString == 'SK' || zipCodeString == 'SO' || zipCodeString == 'ST' || zipCodeString == 'TF' || zipCodeString == 'WA' || zipCodeString == 'WF' || zipCodeString == 'WN' || zipCodeString == 'WR') {
      if (weight <= 100) {
        pricePerQuintal = 56.10;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 51.75;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 41.15;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 37.70;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 36.05;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 35.70;
      } else if (weight > 2000) {
        pricePerQuintal = 35.40;
      };
    } else if (zipCodeString == 'BA' || zipCodeString == 'BH' || zipCodeString == 'BS' || zipCodeString == 'CF' || zipCodeString == 'DL' || zipCodeString == 'DT' || zipCodeString == 'HU' || zipCodeString == 'LD' || zipCodeString == 'LL' || zipCodeString == 'NP' || zipCodeString == 'NR' || zipCodeString == 'SN' || zipCodeString == 'SP' || zipCodeString == 'SY' || zipCodeString == 'TS' || zipCodeString == 'YO') {
      if (weight <= 100) {
        pricePerQuintal = 65.05;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 59.20;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 48.90;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 44.10;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 41.90;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 41.50;
      } else if (weight > 2000) {
        pricePerQuintal = 41.05;
      };
    } else if (zipCodeString == 'BT' || zipCodeString == 'CA' || zipCodeString == 'DG' || zipCodeString == 'DH' || zipCodeString == 'EX' || zipCodeString == 'NE' || zipCodeString == 'PL' || zipCodeString == 'SA' || zipCodeString == 'SR' || zipCodeString == 'TA' || zipCodeString == 'TQ' || zipCodeString == 'TR') {
      if (weight <= 100) {
        pricePerQuintal = 71.80;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 66.35;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 56.10;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 50.35;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 48.15;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 47.45;
      } else if (weight > 2000) {
        pricePerQuintal = 46.65;
      };
    } else if (zipCodeString == 'AB' || zipCodeString == 'DD' || zipCodeString == 'EH' || zipCodeString == 'FK' || zipCodeString == 'G1' || zipCodeString == 'G2' || zipCodeString == 'G3' || zipCodeString == 'G4' || zipCodeString == 'G5' || zipCodeString == 'G6' || zipCodeString == 'G7' || zipCodeString == 'G8' || zipCodeString == 'G9' || zipCodeString == 'IV' || zipCodeString == 'KA' || zipCodeString == 'KW' || zipCodeString == 'KY' || zipCodeString == 'ML' || zipCodeString == 'PA' || zipCodeString == 'PH' || zipCodeString == 'TD') {
      if (weight <= 100) {
        pricePerQuintal = 78.55;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 73.05;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 61.15;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 54.60;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 52.15;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 51.15;
      } else if (weight > 2000) {
        pricePerQuintal = 50.10;
      };
    };
  } else if (countrySelected == "Grecia") {
    shippingTime = 96;
    if ((zipCodeNumber >= 12 && zipCodeNumber <= 13) || zipCodeNumber == 18 || zipCodeNumberGreece == 191 || zipCodeNumberGreece == 192 || zipCodeNumberGreece == 193 || zipCodeNumberGreece == 196 || zipCodeNumberGreece == 561 || zipCodeNumberGreece == 563 || zipCodeNumberGreece == 564 || zipCodeNumberGreece == 565) {
      if (weight <= 100) {
        pricePerQuintal = 50.00;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 47.00;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 38.00;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 35.00;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 33.00;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 32.00;
      } else if (weight > 2000) {
        pricePerQuintal = 32.00;
      };
    } else if ((zipCodeNumber >= 10 && zipCodeNumber <= 11) || (zipCodeNumber >= 14 && zipCodeNumber <= 17) || zipCodeNumber == 54 || zipCodeNumberGreece == 551 || zipCodeNumberGreece == 552 || zipCodeNumberGreece == 553 || zipCodeNumberGreece == 554 || zipCodeNumberGreece == 555 || zipCodeNumberGreece == 566 || zipCodeNumberGreece == 567 || zipCodeNumberGreece == 570) {
      if (weight <= 100) {
        pricePerQuintal = 53.00;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 49.00;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 40.00;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 37.00;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 35.00;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 35.00;
      } else if (weight > 2000) {
        pricePerQuintal = 35.00;
      };
    } else if (zipCodeNumberGreece == 190 || zipCodeNumberGreece == 194 || zipCodeNumberGreece == 195 || zipCodeNumberGreece == 572 || zipCodeNumberGreece == 575 || zipCodeNumber == 20 || zipCodeNumber == 32 || zipCodeNumber == 34 || zipCodeNumber == 59 || (zipCodeNumber >= 60 && zipCodeNumber <= 62)) {
      if (weight <= 100) {
        pricePerQuintal = 62.00;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 57.00;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 46.00;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 42.00;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 40.00;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 40.00;
      } else if (weight > 2000) {
        pricePerQuintal = 39.00;
      };
    } else if (zipCodeNumber == 30 || zipCodeNumber == 33 || zipCodeNumber == 58 || (zipCodeNumber >= 21 && zipCodeNumber <= 27) || (zipCodeNumber >= 35 && zipCodeNumber <= 38) || (zipCodeNumber >= 40 && zipCodeNumber <= 48) || (zipCodeNumber >= 50 && zipCodeNumber <= 53) || (zipCodeNumber >= 63 && zipCodeNumber <= 69)) {
      if (weight <= 100) {
        pricePerQuintal = 72.00;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 66.00;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 54.00;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 49.00;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 47.00;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 46.00;
      } else if (weight > 2000) {
        pricePerQuintal = 46.00;
      };
    };
  } else if (countrySelected == "Irlanda") {
    shippingTime = 168;
    if (weight <= 100) {
      pricePerQuintal = 64.25;
    } else if (weight > 100 && weight <= 200) {
      pricePerQuintal = 61.00;
    } else if (weight > 200 && weight <= 500) {
      pricePerQuintal = 50.60;
    } else if (weight > 500 && weight <= 1000) {
      pricePerQuintal = 47.80;
    } else if (weight > 1000 && weight <= 1500) {
      pricePerQuintal = 46.35;
    } else if (weight > 1500 && weight <= 2000) {
      pricePerQuintal = 46.05;
    } else if (weight > 2000) {
      pricePerQuintal = 45.85;
    };
  } else if (countrySelected == "Lettonia") {
    shippingTime = 120;
    if (zipCodeNumber == 37 || zipCodeNumber == 39 || (zipCodeNumber >= 10 && zipCodeNumber <= 32) || (zipCodeNumber >= 40 && zipCodeNumber <= 42) || (zipCodeNumber >= 49 && zipCodeNumber <= 51)) {
      if (weight <= 100) {
        pricePerQuintal = 42.80;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 41.50;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 35.05;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 33.35;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 32.40;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 32.25;
      } else if (weight > 2000) {
        pricePerQuintal = 32.10;
      };
    } else if (zipCodeNumber == 33 || zipCodeNumber == 36 || zipCodeNumber == 38 || zipCodeNumber == 44 || (zipCodeNumber >= 47 && zipCodeNumber <= 48) || (zipCodeNumber >= 52 && zipCodeNumber <= 53)) {
      if (weight <= 100) {
        pricePerQuintal = 46.85;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 44.80;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 37.85;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 36.65;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 34.50;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 34.30;
      } else if (weight > 2000) {
        pricePerQuintal = 34.15;
      };
    } else if (zipCodeNumber == 43 || (zipCodeNumber >= 34 && zipCodeNumber <= 35) || (zipCodeNumber >= 45 && zipCodeNumber <= 46) || (zipCodeNumber >= 54 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 52.25;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 49.45;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 41.65;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 38.85;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 37.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 37.35;
      } else if (weight > 2000) {
        pricePerQuintal = 37.15;
      };
    };
  } else if (countrySelected == "Liechtenstein") {
    if (weight <= 100) {
      pricePerQuintal = 38.45;
    } else if (weight > 100 && weight <= 200) {
      pricePerQuintal = 35.60;
    } else if (weight > 200 && weight <= 500) {
      pricePerQuintal = 26.65;
    } else if (weight > 500 && weight <= 1000) {
      pricePerQuintal = 23.75;
    } else if (weight > 1000 && weight <= 1500) {
      pricePerQuintal = 22.35;
    } else if (weight > 1500 && weight <= 2000) {
      pricePerQuintal = 22.05;
    } else if (weight > 2000) {
      pricePerQuintal = 21.90;
    };
  } else if (countrySelected == "Lituania") {
    shippingTime = 120;
    if (zipCodeNumber == 40 || (zipCodeNumber >= 01 && zipCodeNumber <= 38) || (zipCodeNumber >= 42 && zipCodeNumber <= 59) || (zipCodeNumber >= 61 && zipCodeNumber <= 71)) {
      if (weight <= 100) {
        pricePerQuintal = 41.40;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 39.95;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 33.35;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 31.50;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 30.55;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 30.35;
      } else if (weight > 2000) {
        pricePerQuintal = 30.20;
      };
    } else if (zipCodeNumber == 39 || zipCodeNumber == 41 || zipCodeNumber == 60 || (zipCodeNumber >= 72 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 47.35;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 44.85;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 37.35;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 35.15;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 33.85;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 33.65;
      } else if (weight > 2000) {
        pricePerQuintal = 33.40;
      };
    };
  } else if (countrySelected == "Lussemburgo") {
    shippingTime = 72;
    if (weight <= 100) {
      pricePerQuintal = 32.00;
    } else if (weight > 100 && weight <= 200) {
      pricePerQuintal = 30.00;
    } else if (weight > 200 && weight <= 500) {
      pricePerQuintal = 22.35;
    } else if (weight > 500 && weight <= 1000) {
      pricePerQuintal = 20.25;
    } else if (weight > 1000 && weight <= 1500) {
      pricePerQuintal = 19.20;
    } else if (weight > 1500 && weight <= 2000) {
      pricePerQuintal = 19.00;
    } else if (weight > 2000) {
      pricePerQuintal = 18.80;
    };
  } else if (countrySelected == "Macedonia") {
    if (zipCodeNumber == 10) {
      if (weight <= 100) {
        pricePerQuintal = 69.05;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 65.80;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 54.45;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 51.75;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 50.25;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 50.00;
      } else if (weight > 2000) {
        pricePerQuintal = 49.75;
      };
    } else {
      alert("La spedizione non è disponibile per il CAP selezionato");
    };
  } else if (countrySelected == "Norvegia") {
    shippingTime = 168;
    if (zipCodeNumber == 67 || (zipCodeNumber >= 00 && zipCodeNumber <= 24) || (zipCodeNumber >= 26 && zipCodeNumber <= 38) || (zipCodeNumber >= 40 && zipCodeNumber <= 63)) {
      if (weight <= 100) {
        pricePerQuintal = 86.05;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 80.70;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 66.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 62.55;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 60.55;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 60.15;
      } else if (weight > 2000) {
        pricePerQuintal = 59.85;
      };
    } else if (zipCodeNumber == 25 || zipCodeNumber == 39 || (zipCodeNumber >= 64 && zipCodeNumber <= 66) || (zipCodeNumber >= 68 && zipCodeNumber <= 79)) {
      if (weight <= 100) {
        pricePerQuintal = 113.70;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 106.50;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 90.05;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 84.65;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 82.05;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 81.55;
      } else if (weight > 2000) {
        pricePerQuintal = 81.05;
      };
    } else if ((zipCodeNumber >= 84 && zipCodeNumber <= 85) || (zipCodeNumber >= 90 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 218.35;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 184.15;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 161.85;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 124.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 106.70;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 94.40;
      } else if (weight > 2000) {
        pricePerQuintal = 94.25;
      };
    };
  } else if (countrySelected == "Olanda") {
    shippingTime = 72;
    if ((zipCodeNumber >= 01 && zipCodeNumber <= 16) || (zipCodeNumber >= 18 && zipCodeNumber <= 59) || (zipCodeNumber >= 61 && zipCodeNumber <= 82) || (zipCodeNumber >= 94 && zipCodeNumber <= 96)) {
      if (weight <= 100) {
        pricePerQuintal = 36.50;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 34.20;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 26.20;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 23.95;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 22.80;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 22.55;
      } else if (weight > 2000) {
        pricePerQuintal = 22.35;
      };
    } else if (zipCodeNumber == 60 || zipCodeNumber == 83 || zipCodeNumber == 87 || zipCodeNumber == 89 || zipCodeNumber == 90 || (zipCodeNumber >= 92 && zipCodeNumber <= 93) || (zipCodeNumber >= 97 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 42.25;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 38.95;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 30.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.30;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 25.90;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 26.65;
      } else if (weight > 2000) {
        pricePerQuintal = 25.40;
      };
    } else if (zipCodeNumber == 17 || zipCodeNumber == 88 || zipCodeNumber == 91) {
      if (weight <= 100) {
        pricePerQuintal = 55.20;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 47.40;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 37.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 33.35;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 31.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 30.20;
      } else if (weight > 2000) {
        pricePerQuintal = 29.55;
      };
    };
  } else if (countrySelected == "Polonia") {
    if ((zipCodeNumber >= 90 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 28.35;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 27.75;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 23.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 22.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 21.40;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 21.25;
      } else if (weight > 2000) {
        pricePerQuintal = 21.10;
      };
    } else if (zipCodeNumber == 09 || (zipCodeNumber >= 00 && zipCodeNumber <= 04) || (zipCodeNumber >= 40 && zipCodeNumber <= 41) || (zipCodeNumber >= 60 && zipCodeNumber <= 62) || (zipCodeNumber >= 86 && zipCodeNumber <= 87)) {
      if (weight <= 100) {
        pricePerQuintal = 34.80;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 33.30;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 26.55;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 24.75;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 23.80;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 23.60;
      } else if (weight > 2000) {
        pricePerQuintal = 23.40;
      };
    } else if (zipCodeNumber == 10 || zipCodeNumber == 29 || zipCodeNumber == 71 || zipCodeNumber == 80 || zipCodeNumber == 85 || (zipCodeNumber >= 05 && zipCodeNumber <= 08) || (zipCodeNumber >= 13 && zipCodeNumber <= 14) || (zipCodeNumber >= 20 && zipCodeNumber <= 21) || (zipCodeNumber >= 24 && zipCodeNumber <= 26) || (zipCodeNumber >= 30 && zipCodeNumber <= 32) || (zipCodeNumber >= 34 && zipCodeNumber <= 35) || (zipCodeNumber >= 42 && zipCodeNumber <= 45) || (zipCodeNumber >= 47 && zipCodeNumber <= 58) || (zipCodeNumber >= 63 && zipCodeNumber <= 64) || (zipCodeNumber >= 88 && zipCodeNumber <= 89)) {
      if (weight <= 100) {
        pricePerQuintal = 37.45;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 35.65;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 28.70;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 26.65;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 25.65;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 25.45;
      } else if (weight > 2000) {
        pricePerQuintal = 25.25;
      }
    } else if (zipCodeNumber == 18 || zipCodeNumber == 33 || zipCodeNumber == 46 || zipCodeNumber == 59 || zipCodeNumber == 77 || (zipCodeNumber >= 11 && zipCodeNumber <= 12) || (zipCodeNumber >= 15 && zipCodeNumber <= 16) || (zipCodeNumber >= 22 && zipCodeNumber <= 23) || (zipCodeNumber >= 36 && zipCodeNumber <= 39) || (zipCodeNumber >= 65 && zipCodeNumber <= 67) || (zipCodeNumber >= 69 && zipCodeNumber <= 70) || (zipCodeNumber >= 72 && zipCodeNumber <= 74) || (zipCodeNumber >= 81 && zipCodeNumber <= 84)) {
      if (weight <= 100) {
        pricePerQuintal = 40.60;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 38.55;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 31.00;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 28.85;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 27.70;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 27.50;
      } else if (weight > 2000) {
        pricePerQuintal = 27.30;
      }
    } else if (zipCodeNumber == 17 || zipCodeNumber == 19 || zipCodeNumber == 68 || (zipCodeNumber >= 27 && zipCodeNumber <= 28) || (zipCodeNumber >= 75 && zipCodeNumber <= 79)) {
      if (weight <= 100) {
        pricePerQuintal = 44.15;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 41.40;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 33.65;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 30.95;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 29.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 29.40;
      } else if (weight > 2000) {
        pricePerQuintal = 29.15;
      };
    };
  } else if (countrySelected == "Portogallo") {
    shippingTime = 120;
    if ((zipCodeNumber >= 00 && zipCodeNumber <= 23) || (zipCodeNumber >= 25 && zipCodeNumber <= 29) || (zipCodeNumber >= 37 && zipCodeNumber <= 50)) {
      if (weight <= 100) {
        pricePerQuintal = 42.95;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 41.10;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 33.65;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 31.65;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 30.65;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 30.40;
      } else if (weight > 2000) {
        pricePerQuintal = 30.25;
      };
    } else if (zipCodeNumber == 24 || zipCodeNumber == 51 || (zipCodeNumber >= 30 && zipCodeNumber <= 36) || (zipCodeNumber >= 53 && zipCodeNumber <= 59) || (zipCodeNumber >= 62 && zipCodeNumber <= 71) || (zipCodeNumber >= 74 && zipCodeNumber <= 75) || (zipCodeNumber >= 79 && zipCodeNumber <= 85) || (zipCodeNumber >= 87 && zipCodeNumber <= 88)) {
      if (weight <= 100) {
        pricePerQuintal = 50.15;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 47.25;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 37.90;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 35.25;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 33.90;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 33.60;
      } else if (weight > 2000) {
        pricePerQuintal = 33.40;
      };
    } else if (zipCodeNumber == 52 || zipCodeNumber == 86 || zipCodeNumber == 89 || (zipCodeNumber >= 60 && zipCodeNumber <= 61) || (zipCodeNumber >= 72 && zipCodeNumber <= 73) || (zipCodeNumber >= 76 && zipCodeNumber <= 78)) {
      if (weight <= 100) {
        pricePerQuintal = 52.75;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 49.35;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 40.10;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 37.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 35.55;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 35.30;
      } else if (weight > 2000) {
        pricePerQuintal = 35.05;
      };
    };
  } else if (countrySelected == "Repubblica Ceca") {
    shippingTime = 96;
    if ((zipCodeNumber >= 60 && zipCodeNumber <= 69)) {
      if (weight <= 100) {
        pricePerQuintal = 24.00;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 23.50;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 19.30;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 18.00;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 17.35;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 17.20;
      } else if (weight > 2000) {
        pricePerQuintal = 17.10;
      };
    } else if (zipCodeNumber == 45 || zipCodeNumber == 52 || zipCodeNumber == 59 || zipCodeNumber == 77 || (zipCodeNumber >= 80 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 25.60;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 24.90;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 20.45;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 19.00;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 18.25;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 18.05;
      } else if (weight > 2000) {
        pricePerQuintal = 17.95;
      };
    } else if ((zipCodeNumber >= 56 && zipCodeNumber <= 57) || (zipCodeNumber >= 70 && zipCodeNumber <= 71) || (zipCodeNumber >= 75 && zipCodeNumber <= 76)) {
      if (weight <= 100) {
        pricePerQuintal = 29.85;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 28.55;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 22.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 20.80;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 19.90;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 19.70;
      } else if (weight > 2000) {
        pricePerQuintal = 19.55;
      }
    } else if (zipCodeNumber == 11 || zipCodeNumber == 35 || zipCodeNumber == 37 || zipCodeNumber == 41 || zipCodeNumber == 43 || zipCodeNumber == 58 || (zipCodeNumber >= 15 && zipCodeNumber <= 16) || (zipCodeNumber >= 23 && zipCodeNumber <= 29) || (zipCodeNumber >= 50 && zipCodeNumber <= 55) || (zipCodeNumber >= 72 && zipCodeNumber <= 74) || (zipCodeNumber >= 78 && zipCodeNumber <= 79)) {
      if (weight <= 100) {
        pricePerQuintal = 31.90;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 30.35;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 23.70;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 21.85;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 20.85;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 20.65;
      } else if (weight > 2000) {
        pricePerQuintal = 20.50;
      }
    } else if (zipCodeNumber == 36 || zipCodeNumber == 42 || zipCodeNumber == 44 || (zipCodeNumber >= 00 && zipCodeNumber <= 10) || (zipCodeNumber >= 12 && zipCodeNumber <= 14) || (zipCodeNumber >= 17 && zipCodeNumber <= 22) || (zipCodeNumber >= 30 && zipCodeNumber <= 34) || (zipCodeNumber >= 38 && zipCodeNumber <= 40) || (zipCodeNumber >= 46 && zipCodeNumber <= 49)) {
      if (weight <= 100) {
        pricePerQuintal = 33.70;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 31.95;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 24.90;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 22.95;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 21.95;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 21.75;
      } else if (weight > 2000) {
        pricePerQuintal = 21.55;
      };
    };
  } else if (countrySelected == "Repubblica Serbia") {
    if ((zipCodeNumber >= 00 && zipCodeNumber <= 39)) {
      if (weight <= 100) {
        pricePerQuintal = 53.35;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 49.45;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 38.60;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 35.35;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 33.70;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 33.40;
      } else if (weight > 2000) {
        pricePerQuintal = 33.15;
      };
    } else {
      alert("La spedizione non è disponibile per il CAP selezionato");
    };
  } else if (countrySelected == "Romania") {
    shippingTime = 120;
    if (zipCodeNumber == 41 || (zipCodeNumber >= 30 && zipCodeNumber <= 31)  || (zipCodeNumber >= 34 && zipCodeNumber <= 39)) {
      if (weight <= 100) {
        pricePerQuintal = 54.25;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 50.25;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 38.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 35.35;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 33.70;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 33.40;
      } else if (weight > 2000) {
        pricePerQuintal = 33.15;
      };
    } else if (zipCodeNumber == 40 || zipCodeNumber == 45 || (zipCodeNumber >= 00 && zipCodeNumber <= 09) || (zipCodeNumber >= 32 && zipCodeNumber <= 33) || (zipCodeNumber >= 50 && zipCodeNumber <= 52)) {
      if (weight <= 100) {
        pricePerQuintal = 58.55;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 54.25;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 41.55;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 38.25;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 36.45;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 36.15;
      } else if (weight > 2000) {
        pricePerQuintal = 35.85;
      };
    } else if (zipCodeNumber == 85 || zipCodeNumber == 87 || (zipCodeNumber >= 10 && zipCodeNumber <= 11) || (zipCodeNumber >= 13 && zipCodeNumber <= 19) || (zipCodeNumber >= 22 && zipCodeNumber <= 23) || (zipCodeNumber >= 25 && zipCodeNumber <= 29) || (zipCodeNumber >= 43 && zipCodeNumber <= 44) || (zipCodeNumber >= 46 && zipCodeNumber <= 49) || (zipCodeNumber >= 53 && zipCodeNumber <= 60) || (zipCodeNumber >= 63 && zipCodeNumber <= 70) || (zipCodeNumber >= 74 && zipCodeNumber <= 79) || (zipCodeNumber >= 91 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 63.00;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 58.10;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 44.60;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 40.90;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 38.95;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 38.60;
      } else if (weight > 2000) {
        pricePerQuintal = 38.30;
      }
    } else if (zipCodeNumber == 12 || zipCodeNumber == 24 || zipCodeNumber == 42 || zipCodeNumber == 86 || (zipCodeNumber >= 20 && zipCodeNumber <= 21) || (zipCodeNumber >= 61 && zipCodeNumber <= 62) || (zipCodeNumber >= 71 && zipCodeNumber <= 73) || (zipCodeNumber >= 80 && zipCodeNumber <= 84) || (zipCodeNumber >= 88 && zipCodeNumber <= 90)) {
      if (weight <= 100) {
        pricePerQuintal = 68.65;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 62.80;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 48.75;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 44.45;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 42.15;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 41.80;
      } else if (weight > 2000) {
        pricePerQuintal = 41.45;
      }
    }
  } else if (countrySelected == "Slovacchia") {
    shippingTime = 96;
    if ((zipCodeNumber >= 80 && zipCodeNumber <= 95)) {
      if (weight <= 100) {
        pricePerQuintal = 32.90;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 31.00;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 23.70;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 21.65;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 20.65;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 20.40;
      } else if (weight > 2000) {
        pricePerQuintal = 20.20;
      };
    } else if ((zipCodeNumber >= 00 && zipCodeNumber <= 09) || (zipCodeNumber >= 96 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 43.30;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 40.30;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 30.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.75;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 26.35;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 26.05;
      } else if (weight > 2000) {
        pricePerQuintal = 25.85;
      };
    };
  } else if (countrySelected == "Slovenia") {
    shippingTime = 120;
    if (weight <= 100) {
      pricePerQuintal = 44.30;
    } else if (weight > 100 && weight <= 200) {
      pricePerQuintal = 41.50;
    } else if (weight > 200 && weight <= 500) {
      pricePerQuintal = 32.10;
    } else if (weight > 500 && weight <= 1000) {
      pricePerQuintal = 29.60;
    } else if (weight > 1000 && weight <= 1500) {
      pricePerQuintal = 28.30;
    } else if (weight > 1500 && weight <= 2000) {
      pricePerQuintal = 28.00;
    } else if (weight > 2000) {
      pricePerQuintal = 27.80;
    };
  } else if (countrySelected == "Spagna") {
    shippingTime = 96;
    if (zipCodeNumber == 08 || zipCodeNumber == 17 || zipCodeNumber == 25 || zipCodeNumber == 43) {
      if (weight <= 100) {
        pricePerQuintal = 33.55;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 31.40;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 23.85;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 21.65;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 20.50;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 20.30;
      } else if (weight > 2000) {
        pricePerQuintal = 20.10;
      };
    } else if (zipCodeNumber == 22 || zipCodeNumber == 26) {
      if (weight <= 100) {
        pricePerQuintal = 37.05;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 34.90;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 27.10;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 24.90;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 23.80;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 23.60;
      } else if (weight > 2000) {
        pricePerQuintal = 23.40;
      };
    } else if (zipCodeNumber == 01 || zipCodeNumber == 03 || zipCodeNumber == 09 || zipCodeNumber == 12 || zipCodeNumber == 20 || zipCodeNumber == 28  || zipCodeNumber == 39 || zipCodeNumber == 46 || zipCodeNumber == 48 || zipCodeNumber == 50 || (zipCodeNumber >= 30 && zipCodeNumber <= 31) || (zipCodeNumber >= 57 && zipCodeNumber <= 59)) {
      if (weight <= 100) {
        pricePerQuintal = 38.50;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 36.20;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 28.20;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 25.90;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 24.70;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 24.45;
      } else if (weight > 2000) {
        pricePerQuintal = 24.25;
      }
    } else if (zipCodeNumber == 02 || zipCodeNumber == 04 || zipCodeNumber == 19 || zipCodeNumber == 34 || zipCodeNumber == 40 || zipCodeNumber == 47 || (zipCodeNumber >= 13 && zipCodeNumber <= 14) || (zipCodeNumber >= 23 && zipCodeNumber <= 24) || (zipCodeNumber >= 44 && zipCodeNumber <= 45)) {
      if (weight <= 100) {
        pricePerQuintal = 41.90;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 39.50;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 31.45;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 29.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 27.85;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 27.65;
      } else if (weight > 2000) {
        pricePerQuintal = 27.45;
      }
    } else if (zipCodeNumber == 10 || zipCodeNumber == 16 || zipCodeNumber == 18 || zipCodeNumber == 27 || zipCodeNumber == 29 || zipCodeNumber == 33 || zipCodeNumber == 37 || (zipCodeNumber >= 05 && zipCodeNumber <= 06) || (zipCodeNumber >= 41 && zipCodeNumber <= 42)) {
      if (weight <= 100) {
        pricePerQuintal = 44.35;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 41.80;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 33.45;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 31.05;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 29.80;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 29.55;
      } else if (weight > 2000) {
        pricePerQuintal = 29.35;
      }
    } else if (zipCodeNumber == 11 || zipCodeNumber == 15 || zipCodeNumber == 21 || zipCodeNumber == 32 || zipCodeNumber == 36 || zipCodeNumber == 49) {
      if (weight <= 100) {
        pricePerQuintal = 47.15;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 44.60;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 35.85;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 33.50;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 32.25;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 32.00;
      } else if (weight > 2000) {
        pricePerQuintal = 31.80;
      };
    };
  } else if (countrySelected == "Svezia") {
    shippingTime = 144;
    if (zipCodeNumber == 72 || zipCodeNumber == 75 || (zipCodeNumber >= 00 && zipCodeNumber <= 37) || (zipCodeNumber >= 40 && zipCodeNumber <= 61) || (zipCodeNumber >= 63 && zipCodeNumber <= 64)) {
      if (weight <= 100) {
        pricePerQuintal = 70.10;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 65.80;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 54.20;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 50.90;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 49.25;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 48.85;
      } else if (weight > 2000) {
        pricePerQuintal = 48.60;
      };
    } else if (zipCodeNumber == 62 || (zipCodeNumber >= 38 && zipCodeNumber <= 39) || (zipCodeNumber >= 65 && zipCodeNumber <= 71) || (zipCodeNumber >= 73 && zipCodeNumber <= 74) || (zipCodeNumber >= 76 && zipCodeNumber <= 82) || (zipCodeNumber >= 86 && zipCodeNumber <= 87)) {
      if (weight <= 100) {
        pricePerQuintal = 85.25;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 79.10;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 65.60;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 60.95;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 58.75;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 58.30;
      } else if (weight > 2000) {
        pricePerQuintal = 57.95;
      };
    } else if ((zipCodeNumber >= 38 && zipCodeNumber <= 39) || (zipCodeNumber >= 65 && zipCodeNumber <= 71) || (zipCodeNumber >= 73 && zipCodeNumber <= 74)) {
      if (weight <= 100) {
        pricePerQuintal = 98.20;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 92.10;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 78.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 73.85;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 71.60;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 71.20;
      } else if (weight > 2000) {
        pricePerQuintal = 70.85;
      }
    } else if (zipCodeNumber == 83 || (zipCodeNumber >= 91 && zipCodeNumber <= 92) || (zipCodeNumber >= 95 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 108.25;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 101.25;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 87.50;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 82.00;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 79.50;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 79.15;
      } else if (weight > 2000) {
        pricePerQuintal = 78.65;
      };
    };
  } else if (countrySelected == "Svizzera") {
    shippingTime = 96;
    if (zipCodeNumber == 02 || (zipCodeNumber >= 04 && zipCodeNumber <= 18) || (zipCodeNumber >= 20 && zipCodeNumber <= 38) || (zipCodeNumber >= 40 && zipCodeNumber <= 64) || (zipCodeNumber >= 70 && zipCodeNumber <= 75) || (zipCodeNumber >= 80 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 50.65;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 45.15;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 31.85;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.85;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 26.05;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 25.60;
      } else if (weight > 2000) {
        pricePerQuintal = 25.20;
      };
    } else if (zipCodeNumber == 01 || zipCodeNumber == 03 || zipCodeNumber == 19 || zipCodeNumber == 39 || (zipCodeNumber >= 65 && zipCodeNumber <= 69) || (zipCodeNumber >= 76 && zipCodeNumber <= 79)) {
      if (weight <= 100) {
        pricePerQuintal = 72.25;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 63.05;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 49.10;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 42.25;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 39.25;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 38.80;
      } else if (weight > 2000) {
        pricePerQuintal = 38.15;
      };
    };
  } else if (countrySelected == "Ungheria") {
    if (zipCodeNumber == 32 || zipCodeNumber == 51 || (zipCodeNumber >= 00 && zipCodeNumber <= 26) || (zipCodeNumber >= 28 && zipCodeNumber <= 30) || (zipCodeNumber >= 80 && zipCodeNumber <= 81)) {
      if (weight <= 100) {
        pricePerQuintal = 32.75;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 31.50;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 25.10;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 23.40;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 22.50;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 22.35;
      } else if (weight > 2000) {
        pricePerQuintal = 22.15;
      };
    } else if (zipCodeNumber == 27 || zipCodeNumber == 31 || zipCodeNumber == 33 || zipCodeNumber == 35 || zipCodeNumber == 40 || zipCodeNumber == 50 || zipCodeNumber == 54 || (zipCodeNumber >= 60 && zipCodeNumber <= 63) || (zipCodeNumber >= 66 && zipCodeNumber <= 68) || (zipCodeNumber >= 84 && zipCodeNumber <= 85) || (zipCodeNumber >= 90 && zipCodeNumber <= 92)) {
      if (weight <= 100) {
        pricePerQuintal = 38.75;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 36.55;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 28.40;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 26.15;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 24.95;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 24.75;
      } else if (weight > 2000) {
        pricePerQuintal = 24.55;
      };
    } else if (zipCodeNumber == 34 || (zipCodeNumber >= 36 && zipCodeNumber <= 39) || (zipCodeNumber >= 41 && zipCodeNumber <= 49) || (zipCodeNumber >= 52 && zipCodeNumber <= 53) || (zipCodeNumber >= 55 && zipCodeNumber <= 59) || (zipCodeNumber >= 64 && zipCodeNumber <= 65) || (zipCodeNumber >= 69 && zipCodeNumber <= 79) || (zipCodeNumber >= 82 && zipCodeNumber <= 83) || (zipCodeNumber >= 86 && zipCodeNumber <= 89) || (zipCodeNumber >= 93 && zipCodeNumber <= 99)) {
      if (weight <= 100) {
        pricePerQuintal = 41.55;
      } else if (weight > 100 && weight <= 200) {
        pricePerQuintal = 39.10;
      } else if (weight > 200 && weight <= 500) {
        pricePerQuintal = 30.15;
      } else if (weight > 500 && weight <= 1000) {
        pricePerQuintal = 27.80;
      } else if (weight > 1000 && weight <= 1500) {
        pricePerQuintal = 26.50;
      } else if (weight > 1500 && weight <= 2000) {
        pricePerQuintal = 26.30;
      } else if (weight > 2000) {
        pricePerQuintal = 26.10;
      };
    };
  };

  packageQuintals = Math.ceil(weight / 100);
  totalPerQuintals = (packageQuintals * pricePerQuintal);
  ourCharge = (totalPerQuintals * 0.30);

  total = (totalPerQuintals + ourCharge + demCost + borderOperationCost + ex1Cost + hydraulicCost + insuranceCost);

  document.getElementById('resultWeight').innerHTML = "Il totale della spedizione è di " + Math.round(total) + " €";
  document.getElementById('details').innerHTML = "Dimensioni pacco: " + packageX + " x" + packageZ + " x" + packageY + " cm <br> Peso: " + weight + " kg";

  if (shippingTime != 0) {
    document.getElementById('resultTime').innerHTML = "Il tempo previsto della spedizione è di " + shippingTime + " ore";
  };
};







function changeCourrier(event) {
  var courrierSelected = document.getElementById('shipping-type').value;

  if (courrierSelected == "SDA NAZIONALE") {
    $(".sda-national").show();
    $(".sda-europe").hide();
    $(".fercam-international").hide();
  } else if (courrierSelected == "SDA - EUROPA") {
    $(".sda-europe").show();
    $(".sda-national").hide();
    $(".fercam-international").hide();
  } else if (courrierSelected == "FERCAM INTERNAZIONALE") {
    $(".fercam-international").show();
    $(".sda-national").hide();
    $(".sda-europe").hide();
  }
};

// JSON TO HTML GENERATOR

const products = [
  {
    ourCode: "ZY5030",
    storedIn: "Milano",
    category : "analysis",
    condition : "conditionNew",
    dataProduct : "JL603-C",
    image : "images/used/jl603-c.jpg",
    imageBig : "images/used/big-portrait/jc603-c-big.jpg",
    key : "jl603-c",
    price : 900,
    spec : "jl603-c-text",
    specTag : ["capacity", "readability", "panSize", "taringRange", "repeatability", "linearity", "sensivityTemp", "typicalStabilization", "adjustmentWeight", "levelIndicator", "usableHeigh", "externalDimensions1", "externalDimension2", "externalDimension3", "netWeight"],
    specData : ["610ct / 122g", "0.001ct / 0.001g", "80mm / 3.15\" dia.", "0-610 ct / 0-122 g", "0.001 ct / 0.001 g", "0.001 ct / 0.001 g", "	10 ppm/ °C", "3 s", "	100 g (included/incluso)", "Yes/Si", "160 mm", "194/236/254 mm", "194/286/254 mm", "380/225/332 mm", "2.5 kg (4.2 kg)"],
  },
  {
    ourCode : "BL292002",
    storedIn : "Milano",
    category : "analysis",
    condition : "conditionNew",
    dataProduct : "JS-2002-G",
    image : "images/used/js2002-g.jpg",
    imageBig : "images/used/big-portrait/js2002-g-big.jpg",
    key : "js2002-g",
    price : 750,
    spec : "js2002-g-text",
    specTag : ["capacity", "readability", "panSize", "legalForTrade", "repeatability", "linearity", "typicalStabilization", "externalDimensions1", "displayType"],
    specData : ["2.200 g", "0.01 g", "190 mm x 170 mm", "No", "0.007 g", "0.006 g", "1s", "290 mm x 84 mm x 184mm", "LCD"],
  },
  {
    ourCode : "BL8060",
    storedIn : "Milano",
    category : "analysis",
    condition : "conditionNew",
    dataProduct : "SP-6001",
    image : "images/used/spu6001.jpg",
    imageBig : "images/used/big-portrait/spu6001-big.jpg",
    key : "spu6001",
    price : 300,
    spec : "spu6001-text",
    specTag : ["capacity", "readability", "panSize", "repeatability", "linearity", "typicalStabilization", "externalDimensions1", "externalDimension3", "displayType", "netWeight"],
    specData : ["6.000 g", "0.1 g", "16.5 x 14.2 cm", "0.1 g", "0.2 g", "3s", " 19.2cm x 5.4cm x 21.0cm", "25.4 x 10.2 x 30.5 cm", "LCD", "1.8 kg"],
  },
  {
    ourCode : "BL7380",
    storedIn : "Milano",
    category : "analysis",
    condition : "conditionNew",
    dataProduct : "PAD7",
    image : "images/used/pad7.jpg",
    imageBig : "images/used/big-portrait/pad7-big.jpg",
    key : "pad7",
    price : 20,
    spec : "pad7-text",
  },
  {
    ourCode : "BL294002",
    storedIn : "Milano",
    category : "analysis",
    condition : "conditionNew",
    dataProduct : "JS-4002-G/M",
    image : "images/used/js2002-g.jpg",
    imageBig : "images/used/big-portrait/js2002-g-big.jpg",
    key : "js4002-g",
    price : 950,
    spec : "js4002-g-text",
    specTag : ["capacity", "readability", "panSize", "legalForTrade", "repeatability", "linearity", "typicalStabilization", "externalDimensions1", "displayType"],
    specData : ["4.200 g", "0.01 g", "190 mm x 170 mm", "No", "0.007 g", "0.006 g", "1s", "290 mm x 84 mm x 184mm", "LCD"],
  },
  {
    ourCode : "BL7308",
    storedIn : "Milano",
    category : "analysis",
    condition : "conditionNew",
    dataProduct : "PAJ812CM",
    image : "images/used/paj812cm.jpg",
    imageBig : "images/used/big-portrait/paj812cm-big.jpg",
    key : "paj812cm",
    price : 450,
    spec : "paj812cm-text",
    specTag : ["capacity", "readability", "panSize", "legalForTrade", "linearity", "typicalStabilization", "externalDimensions1", "displayType"],
    specData : ["810 g", "0.01 g", "180 mm x 168 mm", "EC Type Approved", "0.02 g", "3s", "96 mm x 320 mm x 196 mm", "LCD"],
  },
  {
    ourCode : "SK2507",
    storedIn : "Milano",
    category : "enamelling",
    condition : "conditionNew",
    dataProduct : "DS1000",
    image : "images/used/ds1000.jpg",
    imageBig : "images/used/big-portrait/ds1000-big.jpg",
    key : "ds1000",
    price : 270,
    spec : "ds1000-text",
    specTag : ["externalDimensions1", "netWeight"],
    specData : ["23.5 x 22.5 x 6.3 cm", "2.2 kg"],
  },
  {
    ourCode : "ZUS021",
    storedIn : "Valenza",
    category : "finishing",
    condition : "conditionUsed",
    dataProduct : "BR2510",
    image : "images/used/br2510.jpg",
    imageBig : "images/used/big-portrait/br2510-big.jpg",
    key : "br2510",
    price : 600,
    spec : "br2510-text",
    specTag : ["externalDimensions1", "powerSupply", "absorption", "netWeight"],
    specData : ["60 x 27 x 47 cm", "230v - 50/60Hz", "6Amp", "12 kg"],
  },
  // {
  //   category : "finishing",
  //   condition : "conditionNew",
  //   dataProduct : "FQ6000",
  //   image : "images/used/fq6000.jpg",
  //   imageBig : "images/used/big-portrait/fq6000-big.jpg",
  //   key : "fq6000",
  //   price : "100€",
  //   spec : "fq6000-text",
  // },
  // {
  //   category : "working-bench",
  //   condition : "conditionNew",
  //   dataProduct : "MR8210",
  //   image : "images/used/mr8200.jpg",
  //   imageBig : "images/used/big-portrait/mr8200-big.jpg",
  //   key : "mr8200",
  //   price : "1.200€",
  //   spec : "mr8200-text",
  // },
  // {
  //   category : "metal-forming",
  //   condition : "conditionNew",
  //   dataProduct : "ZY6806",
  //   image : "images/used/zy6806.jpg",
  //   imageBig : "images/used/big-portrait/zy6806-big.jpg",
  //   key : "zy6806",
  //   price : "1.200€",
  //   spec : "zy6806-text",
  // },
  // {
  //   category : "metal-forming",
  //   condition : "conditionNew",
  //   dataProduct : "PR5001",
  //   image : "images/used/pr5001.jpg",
  //   imageBig : "images/used/big-portrait/pr5001-big.jpg",
  //   key : "pr5001",
  //   price : "12.000€",
  //   spec : "pr5001-text",
  // },
  // {
  //   category : "metal-forming",
  //   condition : "conditionNew",
  //   dataProduct : "MR3003",
  //   image : "images/used/me3003.jpg",
  //   imageBig : "images/used/big-portrait/me3003-big.jpg",
  //   key : "me3003",
  //   price : "400€",
  //   spec : "me3003-text",
  // },
  // {
  //   category : "analysis",
  //   condition : "conditionNew",
  //   dataProduct : "METTLER BL1515",
  //   image : "images/used/bl1515.jpg",
  //   imageBig : "images/used/big-portrait/bl1515-big.jpg",
  //   key : "bl1515",
  //   price : "200€",
  //   spec : "bl1515-text",
  // },
  // {
  //   category : "analysis",
  //   condition : "conditionNew",
  //   dataProduct : "OHAUS BL7461",
  //   image : "images/used/bl7461.jpg",
  //   imageBig : "images/used/big-portrait/bl7461-big.jpg",
  //   key : "bl7461",
  //   price : "300€",
  //   spec : "bl7461-text",
  // },
  // {
  //   category : "analysis",
  //   condition : "conditionNew",
  //   dataProduct : "OHAUS BL8115",
  //   image : "images/used/bl8115.jpg",
  //   imageBig : "images/used/big-portrait/bl8115-big.jpg",
  //   key : "bl8115",
  //   price : "200€",
  //   spec : "bl8115-text",
  // },
  // {
  //   category : "analysis",
  //   condition : "conditionNew",
  //   dataProduct : "METTLER BL20802",
  //   image : "images/used/bl20802.jpg",
  //   imageBig : "images/used/big-portrait/bl20802-big.jpg",
  //   key : "bl20802",
  //   price : 300,
  //   spec : "bl20802-text",
  // },
  // {
  //   category : "casting",
  //   condition : "conditionNew",
  //   dataProduct : "DEAVB3",
  //   image : "images/used/deavb3.jpg",
  //   imageBig : "images/used/big-portrait/deavb3-big.jpg",
  //   key : "deavb3",
  //   price : 200,
  //   spec : "deavb3-text",
  // },
  // {
  //   category : "casting",
  //   condition : "conditionNew",
  //   dataProduct : "SZ6010",
  //   image : "images/used/sz6010.jpg",
  //   imageBig : "images/used/big-portrait/sz6010-big.jpg",
  //   key : "sz6010",
  //   price : "500€",
  //   spec : "sz6010-text",
  // },
];


// GLOBALS // da wrappe dentro un if che controlla se sono in second hand php
var optionSelected = localStorage.getItem("filterPreference");
var filtredProducts = [];
var nestedArray = [];


if (window.location.pathname == '/second-hand.php') {
  if (optionSelected == null || optionSelected == 0) {
    localStorage.setItem("filterPreference", 0);

    document.getElementById("generator").innerHTML =
    `${products.map(shTemplate).join('')}`

  } else if (optionSelected == 1) {
    var decreasingPricesProducts = products.sort(function(a, b) {
      return a.price - b.price;
    });
    localStorage.setItem("productsFiltred", JSON.stringify(decreasingPricesProducts));
    var rawProductsPriceFiltred = localStorage.getItem("productsFiltred");
    var parsedProductsPriceFiltred = ('rawProductsPriceFiltred', JSON.parse(rawProductsPriceFiltred));

    document.getElementById("generator").innerHTML =
    `${parsedProductsPriceFiltred.map(shTemplate).join('')}`
  } else if (optionSelected == 2) {
    var increasingPricesProducts = products.sort(function(a, b) {
      return b.price - a.price;
    });
    localStorage.setItem("productsFiltred", JSON.stringify(increasingPricesProducts));
    var rawProductsPriceFiltred = localStorage.getItem("productsFiltred");
    var parsedProductsPriceFiltred = ('rawProductsPriceFiltred', JSON.parse(rawProductsPriceFiltred));

    document.getElementById("generator").innerHTML =
    `${parsedProductsPriceFiltred.map(shTemplate).join('')}`
  }

  if (optionSelected == 0 || optionSelected == null) {
    $("#hidden").append("<select class=\"\" onchange=\"changeFunc(value);\"><option class=\"lang\" value=\"\" key=\"orderBy\" disabled selected hidden></option><option class=\"lang\" value=\"1\" key=\"decreasingPrice\"></option><option class=\"lang\" value=\"2\" key=\"increasingPrice\"></option></select>");
  } else if (optionSelected == 1) {
    $("#hidden").append("<select class=\"\" onchange=\"changeFunc(value);\"><option class=\"lang\" value=\"\" key=\"orderBy\" disabled hidden></option><option class=\"lang\" value=\"1\" key=\"decreasingPrice\" selected></option><option class=\"lang\" value=\"2\" key=\"increasingPrice\"></option></select>");
  } else if (optionSelected == 2) {
    $("#hidden").append("<select class=\"\" onchange=\"changeFunc(value);\"><option class=\"lang\" value=\"\" key=\"orderBy\"  disabled hidden></option><option class=\"lang\" value=\"1\" key=\"decreasingPrice\"></option><option class=\"lang\" value=\"2\" key=\"increasingPrice\" selected></option></select>");
  };


  function removeFilters() {
    localStorage.setItem("filterPreference", 0);
    location.reload();
  };

  function changeFunc($i) {
    optionSelected = ($i);
    location.reload();
    if (optionSelected == 1) {
      localStorage.setItem("filterPreference", 1);
    } else if (optionSelected == 2) {
      localStorage.setItem("filterPreference", 2);
    };
  };
};

function shTemplate(product) {
  return `
  <div class="shop" style="cursor: pointer;" onclick="window.location='/product-page.php';">
    <li class="port-item mix ${product.category}">
      <a onclick="void(0)" data-toggle="modal" data-target="#myModal" data-product="${product.dataProduct}">
        <div class="port-img-overlay">
          <img class="port-main-img" src="${product.image}" alt="img">
        </div>
      </a>
      <div class="port-overlay-cont">
        <div class="port-title-cont">
          <h3>
            <a href="/product-page.php" class="lang" key="${product.key}"></a>
          </h3>
          <span><h4 class="lang" key="vat" style="color: white;">${product.price} € <h4></span>
        </div>
      </div>
    </li>
  </div>
  `
};

function relatedProductsTemplate(relatedProduct) {
  return `
   <div class="relatedDiv">
    <div class="item mb-0 text-center ">
      <div>
        <div class="post-prev-img">
          <a href="#"><img src="${relatedProduct.image}" alt="img"></a>
        </div>
        <div class="post-prev-title mb-5">
          <h3><a href="#" class="font-norm a-inv">${relatedProduct.dataProduct}</a></h3>
        </div>
        <div class="shop-price-cont">
          <strong>${relatedProduct.price} €</strong>
        </div>
      </div>
    </div>
  </div>
  `
};

// PRODUCT PAGE FUNCTION GENERATOR

$(".shop").click(function() {

  var index = $(this).index();
  var productSelected = products[index];

  localStorage.removeItem("selectedProduct");
  localStorage.setItem("selectedProduct" ,JSON.stringify(productSelected));

  relatedCategorySetting();
});

if (window.location.pathname === "/product-page.php") {
  var rawProduct = localStorage.getItem('selectedProduct');
  var parsedProduct = ('rawProduct', JSON.parse(rawProduct));

  var rawRelatedProductCategoryArray = localStorage.getItem('relatedCategoryProductsSelected');
  var parsedRelatedProductArray = ('rawRelatedProductCategoryArray', JSON.parse(rawRelatedProductCategoryArray));

  nestedArray = parsedRelatedProductArray[0];

  var nestedArraySpecTag = parsedProduct.specTag;
  var nestedArraySpec = parsedProduct.specData;

  for (var i = nestedArray.length - 1; i >= 0; --i) {
    if (nestedArray[i].dataProduct == parsedProduct.dataProduct) {
        nestedArray.splice(i,1);
    }
  };

  if (nestedArray === undefined || nestedArray == 0) {
    $(".relatedContainer").append("<h5 class=\"lang\" key=\"noRelated\"></h3>");
  }

  if (typeof parsedProduct.specTag != "undefined") {
    $("#spec-div").append("<a class=\"lang\" href=\"#two\" data-toggle=\"tab\" key=\"spec\"></a>");
  }

  document.getElementById("relatedProducts").innerHTML =
    `${nestedArray.map(relatedProductsTemplate).join('')}`

  if (typeof nestedArraySpecTag !== "undefined") {
    for (i=0; i < Object.keys(nestedArraySpecTag).length; i++) {
      $(".productSpec").append("<tr><th class=\"lang\" scope=\"row\" key=" + nestedArraySpecTag[i] + "></th><td>" + nestedArraySpec[i] + "</td></tr>");
    };
  };

  $(".title-name").append("<h1 class=\"page-title lang\" key=" + parsedProduct.key + ">");
  $(".articleName").append(parsedProduct.dataProduct);
  $(".articleNameBanner").append("<h3 class=\"mt-0 mb-30 lang\" key =" + parsedProduct.key + "></h3>");
  $(".item-price").append(parsedProduct.price);
  $(".popup-gallery").append("<a href=" + parsedProduct.imageBig + "><img src=" + parsedProduct.image + " alt=\"immagine prodotto\"></a>");
  $(".tab-content").append("<div class=\"lang tab-pane fade in active\" key= " + parsedProduct.spec +" id=\"one\">");
  $(".sale-dot").append("<span class=\"lang sale-label label-danger bg-red\" key=" + parsedProduct.condition + "></span>");
  $(".storedIn").after(parsedProduct.storedIn);
  $(".ourCode").after(parsedProduct.ourCode);
  $(".shop-add-btn-cont").append("</a><a onclick=\"void(0)\" class=\"button medium moreInfo lang icon icon-\" data-toggle=\"modal\" data-target=\"#myModal\" key=\"infoButton\" data-product=" + parsedProduct.dataProduct + "></a>");
};

// <a onclick=\"void(0)\" class=\"button medium gray lang\" data-toggle=\"modal\" data-target=\"#myModal\" key=\"buyButton\" data-product=" + parsedProduct.dataProduct + ">

// RELATED PRODUCT SELECTOR


$(".relatedDiv").click(function() {

  var index = $('.relatedDiv').index($(this).closest('.relatedDiv'));
  var relatedProductSelected = nestedArray[index];

  localStorage.removeItem("selectedProduct");
  localStorage.setItem("selectedProduct" ,JSON.stringify(relatedProductSelected));
  relatedCategorySetting();
});

function findCategory(productCategory) {
  var rawProduct = localStorage.getItem('selectedProduct');
  var parsedProduct = ('rawProduct', JSON.parse(rawProduct));
  var categorySelected = parsedProduct.category;
  return productCategory.category === categorySelected;
}

function relatedCategorySetting() {

  var result = products.filter(findCategory);

  localStorage.removeItem('relatedCategoryProductsSelected');
  filtredProducts.push(result);
  localStorage.setItem('relatedCategoryProductsSelected' , JSON.stringify(filtredProducts))
  if (window.location.pathname == "/product-page.php") {
    location.reload();
  }
};

// MODAL

$(document).ready(function() {
  var langGet = localStorage.getItem('langSet');

  if (langGet == "it") {
    $(".multiLangForm").append("<div class=\"col-md-12 mb-23\"><input type=\"text\" value=\"\" data-msg-required=\"Inserire il nome\" maxlength=\"100\" class=\"controled\" name=\"name\" id=\"name\" placeholder=\"NOME\" required></div><div class=\"col-md-12 mb-23\"><input type=\"text\" value=\"\" data-msg-required=\"Inserire il cognome\" maxlength=\"100\" class=\"controled\" name=\"surname\" id=\"surname\" placeholder=\"COGNOME\" required></div><div class=\"col-md-12 mb-23\"><input type=\"text\" value=\"\" data-msg-required=\"Perfavore inserire la ragione sociale\" maxlength=\"100\" class=\"controled\" name=\"company\" id=\"company\" placeholder=\"AZIENDA / RAGIONE SOCIALE\" required></div><div class=\"col-md-12 mb-23\"><input type=\"text\" value=\"\" data-msg-required=\"Inserire il paese\" maxlength=\"100\" class=\"controled\" name=\"country\" id=\"country\" placeholder=\"PAESE\" required></div><div class=\"col-md-12 mb-23\"><input type=\"email\" value=\"\" data-msg-required=\"Perfavore inserire l\'indirizzo e-mail\" data-msg-email=\"Perfavore inserire un\'email valida\" maxlength=\"100\" class=\"controled\" name=\"email\" id=\"email\" placeholder=\"EMAIL\" required></div>");
    $(".sendButtonContainer").append("<input type=\"submit\" name=\"enter\" value=\"INVIA\" class=\"button medium gray mt-40\" data-loading-text=\"Loading...\">");
  } else if (langGet == "en") {
    $(".multiLangForm").append("<div class=\"col-md-12 mb-23\"><input type=\"text\" value=\"\" data-msg-required=\"Please enter your name\" maxlength=\"100\" class=\"controled\" name=\"name\" id=\"name\" placeholder=\"NAME\" required></div><div class=\"col-md-12 mb-23\"><input type=\"text\" value=\"\" data-msg-required=\"Please enter your surname\" maxlength=\"100\" class=\"controled\" name=\"surname\" id=\"surname\" placeholder=\"SURNAME\" required></div><div class=\"col-md-12 mb-23\"><input type=\"text\" value=\"\" data-msg-required=\"Please enter your company name\" maxlength=\"100\" class=\"controled\" name=\"company\" id=\"company\" placeholder=\"COMPANY\" required></div><div class=\"col-md-12 mb-23\"><input type=\"text\" value=\"\" data-msg-required=\"Please enter the country\" maxlength=\"100\" class=\"controled\" name=\"country\" id=\"country\" placeholder=\"COUNTRY\" required></div><div class=\"col-md-12 mb-23\"><input type=\"email\" value=\"\" data-msg-required=\"Please enter your email address\" data-msg-email=\"Please enter a valid email address\" maxlength=\"100\" class=\"controled\" name=\"email\" id=\"email\" placeholder=\"EMAIL\" required></div>");
    $(".sendButtonContainer").append("<input type=\"submit\" name=\"enter\" value=\"SEND MESSAGE\" class=\"button medium gray mt-40\" data-loading-text=\"Loading...\">");
  }
});


// GETTING IP AND POSITION

$.getJSON('https://api.ipify.org?format=json', function(data){
  var ip = data.ip;
  var access_key = '87d7be53d685e0cb0ce1b8de9f66e4d1';

  $.ajax({
      url: 'http://api.ipstack.com/' + ip + '?access_key=' + access_key,
      dataType: 'jsonp',
      success: function(json) {
        var clientCountry = json.country_name;

        if (clientCountry == 'Italy') {
          localStorage.setItem('langSet', 'it');
        } else {
          localStorage.setItem('langSet', 'en');
        }
      }
  });
});


$(".toggle").click(function() {
  $("#hidden").toggleClass("display");
  $(".filters-icon").toggleClass("filters-icon-selected");
  $(".shop").toggleClass("no-pointer");
});

// NEWS GENERATOR
if (window.location.pathname == "/news.php") {
  const news = [
    {
      titleKey : "news1",
      subKey : "news1-subtitle",
      link : "https://www.oroarezzo.it/it/",
      image : "https://www.oroarezzo.it/images/images/5_giorni.png"
    },
  ];


  document.getElementById("news-generator").innerHTML =
    `${news.map(newsTemplate).join('')}`
};


  function newsTemplate(news) {
    return `
      <div class="row mb-80">
        <div class="col-sm-4 col-md-3">
          <h2 class="section-title-3 mb-30 lang" key="${news.titleKey}"></h2>
          <div class="mb-50">
            <p class="lang" key="${news.subKey}"></p>
          </div>
        </div>
        <div class="col-sm-8 col-md-offset-1 ">
          <div class="row">
            <div class="col-md-6 plr-0 lightbox-item">
              <a href="${news.link}" target="_blank">
                <div class="port-img-overlay">
                  <img class="port-main-img" src="${news.image}" alt="img">
                </div>
                <div class="port-overlay-cont">
                  <div class="port-btn-cont">
                    <div aria-hidden="true" class="icon_link"></div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    `
  };
