<!DOCTYPE html>

        <?php include "sections/header.php";?>

        <?php include "sections/main-slider.php";?>

        <!-- SHOP DEPARTMENT -->
        <div id="shop-dep" class="page-section p-110-cont ">
          <div class="container">

            <div class="mb-50">
              <h2 class="section-title pr-0 lang" key="offers"><span class="bold lang" key="special"></span>
                  </h2>
            </div>
            <div class="row">

              <div class="col-md-8 col-sm-8 plr-5">
                <div class="row">

                  <div class="col-md-6 col-sm-6 shop-dep-item mb-10">
                    <a href="https://infinity.luigidaltrozzo.it/InfinityLDT/catalog/articolo/376-gomme-siliconiche/yauxtrdlce-gomma-siliconica-ldt-plus-verde-fosforescente.html">
                      <img src="images/shop/woman-dep.jpg" alt="img">
                      <div class="shop-dep-text-cont">
                        <h4 class="lang" key="rubber"></h4>
                      </div>
                    </a>
                  </div>

                  <div class="col-md-6 col-sm-6 shop-dep-item mb-10">
                    <a href="/second-hand.php">
                      <img src="images/used/bl1515.jpg" alt="img">
                      <div class="shop-dep-text-cont">
                        <h4 class="lang" key="sh"></h4>
                      </div>
                    </a>
                  </div>

                </div>
                <div class="row">

                  <div class="col-md-12 shop-dep-item mb-10">
                    <a href="https://infinity.luigidaltrozzo.it/InfinityLDT/catalog/articolo/453-incisori/kewshwjziu-laser-di-taglio-e-incisione-fibra-nano---scripta.html">
                      <img src="images/shop/sale-dep.jpg" alt="img">
                      <div class="shop-dep-text-cont yellow-bg">
                        <h4><span class="sale-bold lang" key="engraver"></h4>
                      </div>
                    </a>
                  </div>

                </div>
              </div>

              <div class="col-md-4 col-sm-4 shop-dep-item">
                <a href="https://infinity.luigidaltrozzo.it/InfinityLDT/cms/category/222-banchi-lavoro.html">
                  <img src="images/shop/acc-dep.jpg" alt="img">
                  <div class="shop-dep-text-cont">
                    <h4 class="lang" key="working-bench-index"></h4>
                  </div>
                </a>
              </div>

            </div>
          </div>
        </div>

        <!-- DIVIDER -->
        <hr class="mt-0 mb-0">

				<!-- FEATURES 1 -->
				<div id="about" class="page-section">
					<div class="container fes1-cont">
						<div class="row">

							<div class="col-md-4 fes1-img-cont wow fadeInUp mb-20">
								<img src="images/secondSectionImg.jpg" alt="img" >
							</div>

							<div class="col-md-8">

                <div class="row">
                  <div class="col-md-12">
                    <div class="fes1-main-title-cont wow fadeInDown">
                      <div class="title-fs-60">
                        <br>
                        <span class="bold"></span>
                      </div>
                      <div class="line-3-100"></div>
                    </div>
                  </div>
								</div>

                <div class="row">

                  <div class="col-md-6 col-sm-6">
                    <div class="fes1-box wow fadeIn" >
                      <div class="fes1-box-icon">
                        <div class="icon icon-basic-anticlockwise"></div>
                      </div>
                      <h3 class="lang" key="historicity"></h3>
                      <p class="lang" key="historicitySub"></p>
                    </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                    <div class="fes1-box wow fadeIn" data-wow-delay="200ms">
                      <div class="fes1-box-icon">
                        <div class="icon icon-basic-globe"></div>
                      </div>
                      <h3 class="lang" key="efficientDistribution"></h3>
                      <p class="lang" key="efficientDistributionSub"></p>
                    </div>
                  </div>

                </div>

                <div class="row">

                  <div class="col-md-6 col-sm-6">
                    <div class="fes1-box wow fadeIn" data-wow-delay="400ms">
                      <div class="fes1-box-icon">
                        <div class="icon icon-basic-pencil-ruler"></div>
                      </div>
                      <h3 class="lang" key="experience"></h3>
                      <p class="lang" key="experienceSub"></p>
                    </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                    <div class="fes1-box wow fadeIn"  data-wow-delay="600ms">
                      <div class="fes1-box-icon">
                        <div class="icon icon-ecommerce-diamond"></div>
                      </div>
                      <h3 class="lang" key="professionalism"></h3>
                      <p class="lang" key="professionalismSub"></p>
                    </div>
                  </div>

                </div>

							</div>

            </div>
					</div>
				</div>

				<!-- DIVIDER -->
				<hr class="mt-0 mb-0">

				<!-- FEATURES 2 -->
				<div class="page-section">
					<div class="container-fluid">
						<div class="row">

							<div class="col-md-6 wow fadeInLeft equal-height">
								<div class="fes2-main-text-cont">
									<div class="title-fs-45 lang" key="updated">
									</div>
									<div class="line-3-70"></div>
									<div class="fes2-text-cont lang" key="updatedSub"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="row">
									<div class="fes2-img equal-height"></div>
								</div>
							</div>

						</div>
					</div>
				</div>

				<!-- FEATURES 3 -->
				<div class="page-section">
					<div class="container-fluid">
						<div class="row">

							<div class="col-md-6 left-50 wow fadeInRight equal-height">
									<div class="fes2-main-text-cont">
										<div class="title-fs-45 lang" key="tradition">
										</div>
										<div class="line-3-100"></div>
										<div class="fes2-text-cont lang" key="traditionSub"></div>
									</div>
							</div>

              <div class="col-md-6 right-50">
								<div class="row">
									<div class="fes3-img equal-height" ></div>
								</div>
							</div>

						</div>
					</div>
				</div>

				<!-- DIVIDER -->
				<hr class="mt-0 mb-0">

				<!-- FEATURES 4 -->
				<div class="page-section fes4-cont">
					<div class="container">
            <div class="row">

              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes4-box wow fadeIn">
								  <h2 class="section-title lang" key="ourServices"></h2>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes4-box wow fadeIn" data-wow-delay="200ms">
								  <div class="fes4-title-cont" >
								  	<div class="fes4-box-icon">
								  		<div class="icon icon-music-shuffle-button"></div>
								  	</div>
								  	<h3><span class="bold lang" key="firstBox"></span></h3>
								  	<p class="lang" key="firstBoxSub"></p>
								  </div>
								  <div class="lang" key="firstBox2Sub">
								  </div>
								</div>
							</div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes4-box wow fadeIn" data-wow-delay="400ms">
								  <div class="fes4-title-cont" >
								  	<div class="fes4-box-icon">
								  		<div class="icon icon-basic-settings"></div>
								  	</div>
								  	<h3><span class="bold lang" key="secondBox"></span></h3>
								  	<p class="lang" key="secondBoxSub"></p>
								  </div>
								  <div class="lang" key="secondBox2Sub">
								  </div>
								</div>
							</div>

						</div>
						<div class="row">

              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes4-box wow fadeIn" data-wow-delay="600ms">
								  <div class="fes4-title-cont" >
								  	<div class="fes4-box-icon">
								  		<div class="icon icon-basic-pencil-ruler-pen"></div>
								  	</div>
								  	<h3><span class="bold lang" key="thirdBox"></span></h3>
								  	<p class="lang" key="thirdBoxSub"></p>
								  </div>
								  <div class="lang" key="thirdBox2Sub">
								  </div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes4-box wow fadeIn" data-wow-delay="800ms" >
								  <div class="fes4-title-cont" >
								  	<div class="fes4-box-icon">
								  		<div class="icon icon-music-headphones"></div>
								  	</div>
								  	<h3><span class="bold lang" key="fourthBox"></span></h3>
								  	<p class="lang" key="fourthBoxSub"></p>
								  </div>
								  <div class="lang" key="fourthBox2Sub">
								  </div>
								</div>
							</div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes4-box wow fadeIn" data-wow-delay="1000ms">
								  <div class="fes4-title-cont" >
								  	<div class="fes4-box-icon">
								  		<div class="icon icon-ecommerce-bag"></div>
								  	</div>
								  	<h3><span class="bold lang" key="fifthBox"></span></h3>
								  	<p class="lang" key="fifthBoxSub"></p>
								  </div>
								  <div class="lang" key="fifthBox2Sub">
								  </div>
								</div>
							</div>

            </div>
					</div>
				</div>

        <hr class="mt-0 mb-0">

        <!-- CLIENTS 1 & TESTIMONIALS 1 -->
        <div class="page-section p-110-cont">
					<div class="container">
            <div class="row">

              <div class="col-md-12">
                <div class="mb-50">
								  <h2 class="section-title lang" key="ourSuppliers"></h2>
								</div>
							</div>

						</div>
            <div class="row">
              <!-- CLIENTS 1 -->
                <div class="row mb-30" >
                  <div class="owl-clients-auto owl-carousel" >
                    <div class="item text-center"><img src="images/clients/1.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/2.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/3.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/4.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/5.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/6.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/7.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/8.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/9.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/10.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/11.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/12.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/13.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/14.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/15.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/16.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/17.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/18.png" alt="client"></div>
                  </div>
                </div>
                <div class="row mb-30" >
                  <div class="owl-clients-auto owl-carousel" >
                    <div class="item text-center"><img src="images/clients/19.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/20.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/21.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/22.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/23.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/24.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/25.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/26.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/27.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/28.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/29.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/30.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/31.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/32.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/33.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/34.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/35.png" alt="client"></div>
                    <div class="item text-center"><img src="images/clients/36.png" alt="client"></div>
                  </div>
                </div>
							</div>
						</div>
					</div>

				<!-- WORK PROCESS 1 -->
				<div class="page-section " >
				<div class="work-proc-1-bg" >
					<div class="container fes4-cont">
						<div class="row">

							<div class="col-md-4 ">
								<div class="mb-50">
								  <h2 class="section-title lang" key="ourStory"></h2>
								</div>
							</div>
							<div class="col-md-7 col-lg-offset-1">
                <div class="row storyRow">

                  <h7 class="lang" key="storyContent"><h7>

                </div>
							</div>
						</div>
					</div>
				</div>
				</div>

        <!-- COUNTERS 1 -->
        <div id="counter-1" class="page-section p-80-cont">
          <div class="container">

            <div  class="row text-center">

              <!-- Item -->
              <div class="col-xs-6 col-sm-3">
                <div class="count-number">
                  100
                </div>
                <div class="count-descr">
                  <span class="count-title lang" key="counterYears"></span>
                </div>
              </div>

              <!-- Item -->
              <div class="col-xs-6 col-sm-3">
                <div class="count-number">
                 34659
                </div>
                <div class="count-descr">
                  <span class="count-title lang" key="counterClient"></span>
                </div>
              </div>

              <!-- Item -->
              <div class="col-xs-6 col-sm-3">
                <div class="count-number">
                  5
                </div>
                <div class="count-descr">
                  <span class="count-title lang" key="counterBranches"></span>
                </div>
              </div>

              <!-- Item -->
              <div class="col-xs-6 col-sm-3">
                <div class="count-number">
                 4
                </div>
                <div class="count-descr">
                  <span class="count-title lang" key="counterGeneration"></span>
                </div>
              </div>

            </div>
          </div>
        </div>

        <!-- DIVIDER -->
        <hr class="mt-0 mb-0">

        <?php include "sections/contact-form.php";?>

        <!-- GOOGLE MAP -->
        <div class="page-section">
          <div class="container-fluid">
            <div class="row">
              <div data-address="Via Claudio Treves 26, Vimodrone" id="google-map"></div>
            </div>
          </div>
        </div>

        <?php include "sections/footer.php";?>
