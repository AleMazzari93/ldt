<!DOCTYPE html>
        <?php include "sections/header.php";?>

        <!-- PAGE TITLE -->
        <div class="page-title-cont page-title-small grey-light-bg">
          <div class="relative container align-left">
            <div class="row">

              <div class="col-md-8">
                <h1 class="page-title lang" key="terms"></h1>
              </div>

              <div class="col-md-4">
                <div class="breadcrumbs">
                  <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current lang" key="terms"></span>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div style="text-align: left; margin: 80px 40px 40px 40px">
        <p><strong>1. Premessa</strong></p>
<p>Il Cliente è invitato a stampare o a conservare secondo le modalità preferite le Condizioni generali.</p>
<p>In ogni momento e senza preavviso alcuno le condizoni di vendita potranno essere modificate a seguito di aggiornamenti normativi inerenti alla regolamentazione delle vendite on-line.</p>
<p><strong>2. Definizioni</strong></p>
<p>Ai fini del presente contratto, i termini indicati avranno i seguenti significati. I termini indicati al singolare avranno il medesimo significato anche al plurale e viceversa.</p>
<p>&#8220;Acquirente&#8221;: il sottoscrittore della Proposta di acquisto e delle presenti Condizioni generali di contratto, cui è destinata la vendita del Prodotto e/o che indichi, conformemente a quanto previsto dal successivo punto 10, un Terzo cui verrà fornito il Prodotto.</p>
<p>&#8220;Catalogo dei Prodotti&#8221;: l&#8217;insieme dei beni prodotti e/o commercializzati da LUIGI DAL TROZZO , con la specifica indicazione dei relativi costi e delle caratteristiche tecnico-funzionali.</p>
<p>&#8220;Condizioni generali di contratto&#8221;: le presenti pattuizioni contrattuali che regolano i rapporti tra LUIGIDALTROZZO e l&#8217;Acquirente.</p>
<p>&#8220;Corrispettivo&#8221;: le somme dovute dall&#8217;Acquirente in conseguenza della conclusione del contratto di vendita di uno o più Prodotti.</p>
<p>&#8220;Imballaggio&#8221;: scatola di protezione all&#8217;interno della quale il Vettore effettua la consegna a domicilio del Prodotto.</p>
<p>&#8220;Libretto di istruzioni&#8221;: informativa fornita dal Produttore unitamente alla consegna del Prodotto relativa alle modalità di uso del Prodotto, alle sue caratteristiche tecnico-funzionali ed alle modalità di pulizia e manutenzione dello stesso.</p>
<p>&#8220;Modulo di consegna&#8221;: prestampato che l&#8217;Acquirente dovrà sottoscrivere al momento della consegna del Prodotto all&#8217;indirizzo indicati sulla Proposta d&#8217;acquisto.</p>
<p>&#8220;Prodotto&#8221;: uno o più beni commercializzati da LUIGI DAL TROZZO , così come presentati all&#8217;interno del Catalogo dei prodotti.</p>
<p>&#8220;Produttore&#8221; : il fabbricante comunitario del bene o del servizio o un suo intermediario; l´importatore del bene o del servizio nel territorio dell´Unione Europea; qualsiasi altra persona fisica o giuridica che si presenta come produttore identificando il bene o servizio con il proprio nome, marchio o altro segno distintivo.</p>
<p>&#8220;Proposta di acquisto&#8221;: il modulo, predisposto da LUIGI DAL TROZZO e reperibile sul sito www.luigidaltrozzo.it , sottoscritto dall&#8217;Acquirente attraverso il quale egli indica espressamente: i) dati personali dell&#8217;Acquirente; ii) uno o più Prodotti di cui richiede l&#8217;acquisto, accettando espressamente le presenti Condizioni generali di contratto; iii) il luogo di consegna del Prodotto o dei Prodotti acquistati; iiii) una delle modalità di cui al successivo punto 5 attraverso cui effettuare il pagamento.</p>
<p>&#8220;Territorio&#8221;: il territorio della Repubblica Italiana, della Repubblica di San Marino e della Città del Vaticano.</p>
<p>&#8220;Terzo&#8221;: la persona fisica e/o giuridica che l&#8217;Acquirente indica quale soggetto cui sarà destinata la consegna di uno o più prodotti.</p>
<p>&#8220;Vettore&#8221;: la persona e/o società incaricata da LUIGI DAL TROZZO della consegna del Prodotto all&#8217;indirizzo indicato dall&#8217;Acquirente sulla Proposta d&#8217;acquisto.</p>
<p><strong>3. Ordini</strong></p>
<p>Effettuare gli acquisti da LUIGI DAL TROZZO , è molto semplice:</p>
<p>a) Registrarsi compilando il modulo sotto la sezione &#8220;registrati&#8221;.</p>
<p>b) Attendere l&#8217;e-mail di conferma di registrazione nella quale saranno forniti il nome utente e password;</p>
<p>c) Effettuare l&#8217;ordine</p>
<p><strong>4. Conclusione del contratto di vendita</strong></p>
<p>Le presenti Condizioni generali di contratto sono volte a regolare i rapporti tra l&#8217;Acquirente e LUIGI DAL TROZZO , con sede in Via Albricci 5  Partita IVA 08015150157 &#8211; www.luigidaltrozzo.it &#8211; info@luigidaltrozzo.it &#8211; tel. +39.02.2885871 &#8211; fax +39.02.2870812.</p>
<p>Il contratto di vendita tra Acquirente e LUIGI DAL TROZZO si intende concluso con la ricezione da parte di quest&#8217;ultima della Proposta di acquisto, sottoscritta dall&#8217;Acquirente e debitamente compilata in relazione a: i) dati personali e recapiti dell&#8217;Acquirente; ii) uno o più Prodotti di cui richiede l&#8217;acquisto, accettando espressamente le presenti Condizioni generali di contratto; iii) il luogo di consegna del Prodotto o dei Prodotti acquistati; iiii) una delle modalità di cui al successivo punto 5 attraverso cui effettuare il pagamento.</p>
<p>La Proposta di acquisto dovrà essere trasmessa a cura dell&#8217;Acquirente esclusivamente a mezzo internet. A seguito della ricezione della Proposta di acquisto, LUIGIDALTROZZO trasmetterà all&#8217;Acquirente comunicazione di conferma dell&#8217;ordine.</p>
<p>Qualora la Proposta di acquisto dovesse essere incompleta, LUIGI DAL TROZZO ne informerà l&#8217;Acquirente entro 15 giorni mediante comunicazione all&#8217;indirizzo e-mail indicato sulla Proposta di acquisto, invitandolo contemporaneamente a trasmettere nuovamente la Proposta di acquisto integralmente compilata. In mancanza di nuova ricezione della Proposta di acquisto debitamente integrata entro e non oltre i successivi 15 giorni, non potrà ritenersi concluso alcun contratto tra LUIGIDALTROZZO e l&#8217;Acquirente. In tale circostanza l&#8217;Acquirente potrà recedere liberamente, e senza responsabilità alcuna per LUIGIDALTROZZO, dal contratto con comunicazione scritta da inoltrarsi, entro i successivi 15 giorni, con le medesime modalità di trasmissione della Proposta di acquisto. È fatta ovviamente salva la possibilità per l&#8217;Acquirente di comunicare per iscritto, con le medesime modalità di trasmissione della Proposta di acquisto, la volontà di procedere ugualmente all&#8217;acquisto del Prodotto esonerando espressamente LUIGIDALTROZZO da qualunque responsabilità per l&#8217;eventuale ritardo nella consegna.</p>
<p>La consegna del Prodotto acquistato avverrà attraverso un Vettore incaricato da LUIGI DAL TROZZO, entro 30 giorni dalla ricezione della Proposta di acquisto sottoscritta e compilata così come sopra specificato, salvo diversi accordi fra le parti.</p>
<p><strong id="metodipagamento">5. Pagamento e Fatturazione</strong></p>
<p>Il Corrispettivo del Prodotto acquistato è esclusivamente quello indicato all&#8217;interno del Catalogo dei Prodotti pubblicato sul sito Internet www.luigidaltrozzo.it da LUIGIDALTROZZO e che costituisce parte integrante delle presenti Condizioni generali di contratto. Il Corrispettivo si intende Iva inclusa. Il pagamento del Corrispettivo potrà avvenire mediante una delle seguenti modalità:</p>
<p>&#8211; con carta di credito tramite circuito Paypal;</p>
<p>&#8211; bonifico bancario anticipato: la causale da riportare sul bonifico bancario dovrà indicare: il numero e la data dell&#8217;ordine, il nome e il cognome e l’eventuale ragione sociale dell&#8217;ordinante; il bonifico bancario dovrà essere effettuato a favore di:</p>
<p>Beneficiario: LUIGI DAL TROZZO &amp; C. S.A.S.</p>
<p>Banca: BANCA POPOLARE DI SONDRIO</p>
<p>Codice IBAN: IT06Z0569601602000006199X50</p>
<p>La fatturazione del Corrispettivo verrà effettuata in favore dell&#8217;Acquirente attraverso i dati indicati all&#8217;interno della Proposta d&#8217;Acquisto ed il relativo documento fiscale verrà inviato per e-mail.</p>
<p>Il Corrispettivo, qualora l&#8217;Acquirente abbia scelto la modalità di pagamento mediante carta di credito, si intenderà corrisposto ad LUIGIDALTROZZO solo con l&#8217;effettiva esecuzione del pagamento da parte della Banca o dell&#8217;Ente emittente la carta di credito.</p>
<p><strong id="consegna">6. Consegna</strong></p>
<p>Al momento della consegna della merce da parte del corriere, il Cliente è tenuto a controllare:</p>
<p>che il numero dei colli in consegna corrisponda a quanto indicato sulla lettera del vettore;</p>
<p>che l&#8217;imballo risulti integro, non danneggiato, né bagnato o, comunque, alterato, anche nei materiali di chiusura (nastro adesivo personalizzato). Eventuali danni o la mancata corrispondenza del numero dei colli o delle indicazioni, devono essere immediatamente contestati al corriere che effettua la consegna, apponendo la dicitura &#8220;ritiro con riserva&#8221; e la motivazione sull&#8217;apposito documento accompagnatorio e comunicati, entro otto giorni in una delle seguenti modalità:</p>
<p>a) via mail ecommerce@luigidaltrozzo.it</p>
<p>b) via telefono +39.02.2885871 Pur in presenza di imballo integro, la merce dovrà essere verificata entro otto giorni dal ricevimento. Eventuali anomalie occulte, dovranno essere segnalate nelle modalità sopra indicare.</p>
<p>Ogni segnalazione oltre i suddetti termini non sarà presa in considerazione. Per ogni dichiarazione, il cliente si assume la responsabilità piena di quanto dichiarato. Una volta firmato il documento del corriere, il Cliente non potrà opporre alcuna contestazione circa le caratteristiche esteriori di quanto consegnato. Se non ti trovi nel luogo di destinazione il giorno della consegna il corriere passerà il giorno successivo e lascerà una cartolina con il numero di telefono nel caso tu voglia metterti d&#8217;accordo per la consegna. Il vettore provvede a fare due tentativi di recapito dei prodotti all&#8217;indirizzo indicato dal Cliente.</p>
<p>Nel caso di mancato ritiro dei prodotti da parte del Cliente entro il termine sopra precisato, l&#8217;ordine verrà automaticamente annullato ed i prodotti restituiti ad LUIGIDALTROZZO . In tal caso i costi del trasporto non saranno rimborsati al Cliente. Ricordiamo che: Alcuni dei nostri prodotti sono stati da noi aperti per controllo qualità prodotto.</p>
<p>La consegna viene effettuata solo ed esclusivamente su territorio italiano.</p>
<p>Per giorno lavorativo si intende dal lunedì al venerdì esclusi i festivi.</p>
<p>Nessuna responsabilità può essere imputata a LUIGIDALTROZZO in caso di ritardo nella consegna.</p>
<p>Tempi di Consegna</p>
<p>La spedizione viene effettuata tramite corriere abilitato. La merce acquistata viene spedita entro 48/72 ore lavorative dalla ricezione dell&#8217; ordine, salvo accordi differenti oppure nel caso esposto più sotto.</p>
<p>&#8211; Qualora venga scelto il pagamento tramite Bonifico Bacario, la merce verrà spedita entro 24 ore lavorative dal momento in cui riceveremo l&#8217;ok del pagamento da parte della ns banca (gg di iter burocratici previsti 2/4 gg)<br />
&#8211; Nei casi in cui un prodotto sia terminato a magazzino, potrebbero verificarsi ritardi nella consegna della merce; ma in quest&#8217; ultimo caso il cliente sarà informato da un nostro operatore, per concordare la consegna o l&#8217;annullamento dell&#8217;ordine stesso</p>
<p>In via informativa, i tempi di consegna tramite Corriere convenzionato, sono :</p>
<p>&#8211; Province Nord/Centro e Capoluoghi Sud: consegna in 24/48 ore lavorative<br />
&#8211; Isole, Calabria, Basilicata: consegna in 48/72 ore lavorative.</p>
<p>Fatturazione</p>
<p>A consegna avvenuta verrà inoltrata una e-mail contenente la fattura di vendita. L’indirizzo elettronico di riferimento a cui sarà inviata la fattura suddetta è quello indicato in fase di registrazione dall’utente</p>
<p><strong id="dirittorecesso">7. Diritto di recesso D.L. n. 206/2005 e successivi aggiornamenti</strong></p>
<p>Oltre all&#8217;eventualità espressamente prevista al precedente punto 4, l&#8217;Acquirente ha diritto di recedere senza alcuna penalità e senza specificarne il motivo entro il termine di 14 giorni di calendario, inviando lettera raccomandata a.r., ovvero telegramma, telex, posta elettronica e fax, tutti da confermarsi comunque entro le 48 ore successive mediante raccomandata a.r.,<br />
Il termine per l&#8217;esercizio del diritto di recesso decorre dal giorno del ricevimento del Prodotto. Resta inteso che l&#8217;integrità del Prodotto da restituire è condizione essenziale per l&#8217;esercizio del diritto di recesso.</p>
<p>In caso di esercizio del diritto di recesso:</p>
<p>i) l&#8217;Acquirente si impegna a restituire il Prodotto entro 14 giorni di calendario, mettendolo a disposizione di LUIGI DAL TROZZO che provvederà al ritiro mediante Vettore. ii) LUIGI DAL TROZZO si impegna a rimborsare il corrispettivo eventualmente già versato dall&#8217;Acquirente entro 30 giorni dalla data in cui è venuta a conoscenza dell&#8217;esercizio del diritto di recesso.</p>
<p><strong>8. Difetti di conformità del Prodotto e Garanzia Legale.</strong></p>
<p>LUIGI DAL TROZZO è responsabile, ai sensi di legge, per i difetti di conformità del Prodotto che si manifestino entro due anni dalla consegna del Prodotto, ma l&#8217;Acquirente deve denunciare l&#8217;esistenza dei difetti entro e non oltre 15 giorni dalla data in cui ha scoperto il difetto.</p>
<p>In caso di difetti di conformità, l&#8217;Acquirente potrà richiedere ad LUIGI DAL TROZZO , a propria scelta e senza alcun onere, la riparazione o la sostituzione del Prodotto, salvo che il rimedio scelto dall&#8217;Acquirente risulti eccessivamente oneroso o impossibile rispetto all&#8217;altro. LUIGIDALTROZZO si impegna a provvedere al ritiro del Prodotto per la riparazione o la sostituzione dello stesso entro 60 giorni dalla richiesta.</p>
<p>I rimedi della risoluzione del contratto ovvero della riduzione del prezzo potranno essere esperiti dall&#8217;Acquirente solo qualora:</p>
<p>i) la riparazione o la sostituzione risultino impossibili o eccessivamente onerose;</p>
<p>ii) LUIGI DAL TROZZO non abbia provveduto al ritiro del Prodotto per la sua riparazione o sostituzione entro 60 giorni dalla richiesta.</p>
<p>Nell&#8217;eventualità di riduzione del prezzo, dovrà tenersi conto dell&#8217;uso già fatto del Prodotto.</p>
<p>In ogni caso un difetto di conformità di lieve entità non dà diritto alla risoluzione del contratto.</p>
<p><strong>9. Mancato o ritardato pagamento del Corrispettivo</strong></p>
<p>Nel caso in cui l&#8217;Acquirente, al di fuori delle ipotesi di recesso contrattualmente previste, ometta o ritardi il versamento del Corrispettivo pattuito a norma del precedente punto 5, il contratto si intenderà risolto e LUIGIDALTROZZO avrà diritto a richiedere all&#8217;Acquirente a titolo di penale una somma pari al 5% del Corrispettivo del Prodotto.</p>
<p><strong>10. Terzo destinatario della consegna del Prodotto</strong></p>
<p>Al momento della sottoscrizione della Proposta di acquisto, l&#8217;Acquirente può indicare il nominativo ed il recapito di un Terzo cui sarà destinata la consegna del Prodotto. In tal caso l&#8217;Acquirente si impegna a: i) pagare il Corrispettivo dovuto in base al contratto; ii) comunicare al Terzo le presenti Condizioni generali di Contratto e a garantirne il rispetto da parte del Terzo.</p>
<p>Nel caso di inadempimento del Terzo di qualunque obbligo su di esso gravante in base al contratto, LUIGI DAL TROZZO potrà rivalersi direttamente sull&#8217;Acquirente.</p>
<p>Nel caso in cui il Terzo si rifiuti di ricevere la consegna del Prodotto, il contratto si intenderà comunque validamente stipulato con l&#8217;Acquirente, con ogni conseguenza derivante dal contratto stesso.</p>
<p><strong>11. Foro competente.</strong></p>
<p>Per qualunque controversia scaturente dal presente contratto è esclusivamente competente il Foro di appartenenza di LUIGI DAL TROZZO .</p>
								<div class="clearfix"></div>
              </div>


        <?php include "sections/footer.php";?>
