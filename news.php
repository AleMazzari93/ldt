<!DOCTYPE html>
        <?php include "sections/header.php";?>

        <!-- PAGE TITLE SMALL -->
        <div class="page-title-cont page-title-small grey-light-bg">
          <div class="relative container align-left">
            <div class="row">

              <div class="col-md-8">
                <h1 class="page-title">NEWS</h1>
              </div>

              <div class="col-md-4">
                <div class="breadcrumbs">
                  <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current">NEWS</span>
                </div>
              </div>

            </div>
          </div>
        </div>


        <?php include "sections/news-body.php";?>
        <?php include "sections/footer.php";?>
