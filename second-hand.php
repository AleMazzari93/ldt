<!DOCTYPE html>
        <?php include "sections/header.php";?>

        <!-- PAGE TITLE SMALL -->
        <div class="page-title-cont page-title-small grey-light-bg">
          <div class="relative container align-left">
            <div class="row">

              <div class="col-md-8">
                <h1 class="page-title lang" key="sh"></h1>
              </div>

              <div class="col-md-4">
                <div class="breadcrumbs">
                  <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current lang" key="sh"></span>
                </div>
              </div>
            </div>
          </div>
        </div>

      <div class="grey-light-bg plr-30 plr-0-767 clearfix">
        <!-- COTENT CONTAINER -->
        <div class="white-bg plr-30 pt-50 pb-20">

          <div class="relative">
            <a class="toggle filters-icon glyphicon glyphicon-filter" key=""></a>
            <!-- <input class="search-icon" type="text" id="searchInput" onkeyup="function()" placeholder="Prova"></input> -->
            <!-- PORTFOLIO FILTER -->
            <div class="port-filter text-center text-left-767 main-filter-div">
              <a key="all" href="#" class="filter lang active" data-filter="*"></a>
              <a key="analysis" href="#" class="filter lang" data-filter=".analysis"></a>
              <a key="finishing" href="#" class="filter lang" data-filter=".finishing"></a>
              <a key="enamelling" href="#" class="filter lang" data-filter=".enamelling"></a>
              <a key="casting" href="#" class="filter lang" data-filter=".casting"></a>
              <a key="working-bench" href="#" class="filter lang" data-filter=".working-bench"></a>
              <a key="metal-forming" href="#" class="filter lang" data-filter=".metal-forming"></a>
              <div class="display hidden-container" id="hidden">
                <a class="icon icon_close close-hidden-cross toggle"></a>
                <h4 class="lang" key="filterOptions"></h4>
                <a class="lang" onclick="removeFilters()" key="removeFilters"></a>
              </div>
            </div>

            <!-- ITEMS GRID -->
            <ul class="port-grid port-grid-gut clearfix" id="items-grid">
              <?php include "sections/sh-product.php";?>
              <?php include "sections/modal.php";?>

              </div>
            </ul>
          </div>
        </div>
      </div>
      <?php include "sections/footer.php";?>
