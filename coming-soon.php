<html>
  <head>
    <title>Redirect</title>
    <meta http-equiv="refresh" content="5;URL=https://infinity.luigidaltrozzo.it">
  </head>
  <body>
    <p>
    Tra 5 secondi avverrà un redirect automatico alla nuova pagina.<br>
    Se non vuoi aspettare <a href="https://infinity.luigidaltrozzo.it">clicca qui</a>.
    </p>
  </body>
</html>
