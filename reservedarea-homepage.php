<!DOCTYPE html>
        <?php include "sections/header.php";?>

        <!-- PAGE TITLE -->
        <div class="page-title-cont page-title-small grey-light-bg">
          <div class="relative container align-left">
            <div class="row">

              <div class="col-md-8">
                <h1 class="page-title lang" key="">AREA RISERVATA</h1>
              </div>

              <div class="col-md-4">
                <div class="breadcrumbs">
                  <a href="index.php">Home</a><span class="slash-divider">/</span><span class="bread-current lang" key="">AREA RISERVATA</span>
                </div>
              </div>

            </div>
          </div>
        </div>

        <!-- MENU -->
        <div id="contact-link" class="page-section p-80-cont">
          <div class="container">
            <div class="row">

              <div class="col-md-12 branchesDiv">
                    <h3><a href="privacy-form-page.php" class="bold lang">MODULO PRIVACY</span></a></h3>
                </div>

              <div class="col-md-12 branchesDiv">
                    <h3><a href="shipping-cost.php" class="bold"></span>CALCOLATORE SPEDIZIONI</a></h3>
                </div>

            </div>
          </div>
        </div>

        <?php include "sections/footer.php";?>
